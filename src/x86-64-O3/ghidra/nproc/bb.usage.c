
#include "nproc.h"

long null_ARRAY_00612b6_0_4_;
long null_ARRAY_00612b6_40_8_;
long null_ARRAY_00612b6_4_4_;
long null_ARRAY_00612b6_48_8_;
long null_ARRAY_00612ba_0_8_;
long null_ARRAY_00612ba_8_8_;
long null_ARRAY_00612d8_0_8_;
long null_ARRAY_00612d8_16_8_;
long null_ARRAY_00612d8_24_8_;
long null_ARRAY_00612d8_32_8_;
long null_ARRAY_00612d8_40_8_;
long null_ARRAY_00612d8_48_8_;
long null_ARRAY_00612d8_8_8_;
long null_ARRAY_00612dc_0_4_;
long null_ARRAY_00612dc_16_8_;
long null_ARRAY_00612dc_24_4_;
long null_ARRAY_00612dc_32_8_;
long null_ARRAY_00612dc_40_4_;
long null_ARRAY_00612dc_4_4_;
long null_ARRAY_00612dc_44_4_;
long null_ARRAY_00612dc_48_4_;
long null_ARRAY_00612dc_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_0040fd00;
long DAT_0040fda1;
long DAT_0040fdd7;
long DAT_004101a0;
long DAT_004101a4;
long DAT_004101a8;
long DAT_004101ab;
long DAT_004101ad;
long DAT_004101b1;
long DAT_004101b5;
long DAT_0041076b;
long DAT_00410de8;
long DAT_00410ef1;
long DAT_00410ef7;
long DAT_00410f09;
long DAT_00410f0a;
long DAT_00410f28;
long DAT_00410f2c;
long DAT_00410fb6;
long DAT_00612760;
long DAT_00612770;
long DAT_00612780;
long DAT_00612b48;
long DAT_00612bb0;
long DAT_00612bb4;
long DAT_00612bb8;
long DAT_00612bbc;
long DAT_00612bc0;
long DAT_00612bd0;
long DAT_00612be0;
long DAT_00612be8;
long DAT_00612c00;
long DAT_00612c08;
long DAT_00612c50;
long DAT_00612c58;
long DAT_00612c60;
long DAT_00612df8;
long DAT_00612e00;
long DAT_00612e08;
long DAT_00612e18;
long fde_00411940;
long int7;
long null_ARRAY_00410080;
long null_ARRAY_00411240;
long null_ARRAY_00411490;
long null_ARRAY_00612b60;
long null_ARRAY_00612ba0;
long null_ARRAY_00612c20;
long null_ARRAY_00612c80;
long null_ARRAY_00612d80;
long null_ARRAY_00612dc0;
long PTR_DAT_00612b40;
long PTR_null_ARRAY_00612b98;
long register0x00000020;
void
FUN_00401cd0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401cfd;
  func_0x00401740 (DAT_00612be0, "Try \'%s --help\' for more information.\n",
		   DAT_00612c60);
  do
    {
      func_0x004018f0 ((ulong) uParm1);
    LAB_00401cfd:
      ;
      func_0x004015b0 ("Usage: %s [OPTION]...\n", DAT_00612c60);
      uVar3 = DAT_00612bc0;
      func_0x00401750
	("Print the number of processing units available to the current process,\nwhich may be less than the number of online processors\n\n",
	 DAT_00612bc0);
      func_0x00401750
	("      --all      print the number of installed processors\n      --ignore=N  if possible, exclude N processing units\n",
	 uVar3);
      func_0x00401750 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401750
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040fd00;
      local_80 = "test invocation";
      puVar6 = &DAT_0040fd00;
      local_78 = 0x40fd80;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401840 ("nproc", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x004015b0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004018a0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004017a0 (lVar2, &DAT_0040fda1, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "nproc";
		  goto LAB_00401ea1;
		}
	    }
	  func_0x004015b0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "nproc");
	LAB_00401eca:
	  ;
	  pcVar5 = "nproc";
	  uVar3 = 0x40fd39;
	}
      else
	{
	  func_0x004015b0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004018a0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004017a0 (lVar2, &DAT_0040fda1, 3);
	      if (iVar1 != 0)
		{
		LAB_00401ea1:
		  ;
		  func_0x004015b0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "nproc");
		}
	    }
	  func_0x004015b0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "nproc");
	  uVar3 = 0x410f27;
	  if (pcVar5 == "nproc")
	    goto LAB_00401eca;
	}
      func_0x004015b0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

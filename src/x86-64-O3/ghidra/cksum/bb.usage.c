
#include "cksum.h"

long null_ARRAY_0061276_0_8_;
long null_ARRAY_0061276_8_8_;
long null_ARRAY_0061294_0_8_;
long null_ARRAY_0061294_16_8_;
long null_ARRAY_0061294_24_8_;
long null_ARRAY_0061294_32_8_;
long null_ARRAY_0061294_40_8_;
long null_ARRAY_0061294_48_8_;
long null_ARRAY_0061294_8_8_;
long null_ARRAY_0061298_0_4_;
long null_ARRAY_0061298_16_8_;
long null_ARRAY_0061298_24_4_;
long null_ARRAY_0061298_32_8_;
long null_ARRAY_0061298_40_4_;
long null_ARRAY_0061298_4_4_;
long null_ARRAY_0061298_44_4_;
long null_ARRAY_0061298_48_4_;
long null_ARRAY_0061298_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_0040f82f;
long DAT_0040f830;
long DAT_0040f832;
long DAT_0040f835;
long DAT_0040f8bf;
long DAT_0040feec;
long DAT_0040ffa0;
long DAT_0040ffa4;
long DAT_0040ffa8;
long DAT_0040ffab;
long DAT_0040ffad;
long DAT_0040ffb1;
long DAT_0040ffb5;
long DAT_00410564;
long DAT_004109f5;
long DAT_00410af9;
long DAT_00410aff;
long DAT_00410b11;
long DAT_00410b12;
long DAT_00410b30;
long DAT_00410b34;
long DAT_00410bbe;
long DAT_00612330;
long DAT_00612340;
long DAT_00612350;
long DAT_00612708;
long DAT_00612770;
long DAT_00612774;
long DAT_00612778;
long DAT_0061277c;
long DAT_00612780;
long DAT_00612788;
long DAT_00612790;
long DAT_006127a0;
long DAT_006127a8;
long DAT_006127c0;
long DAT_006127c8;
long DAT_00612810;
long DAT_00612818;
long DAT_00612820;
long DAT_00612828;
long DAT_006129b8;
long DAT_006129c0;
long DAT_006129c8;
long DAT_006129d8;
long _DYNAMIC;
long fde_00411560;
long int7;
long null_ARRAY_0040fac0;
long null_ARRAY_0040fec0;
long null_ARRAY_0040ff00;
long null_ARRAY_00410e60;
long null_ARRAY_004110b0;
long null_ARRAY_00612760;
long null_ARRAY_006127e0;
long null_ARRAY_00612840;
long null_ARRAY_00612940;
long null_ARRAY_00612980;
long PTR_DAT_00612700;
long PTR_null_ARRAY_00612758;
long register0x00000020;
long stack0x00000008;
void
FUN_00401ef0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401f1d;
  func_0x00401710 (DAT_006127a0, "Try \'%s --help\' for more information.\n",
		   DAT_00612828);
  do
    {
      func_0x004018d0 ((ulong) uParm1);
    LAB_00401f1d:
      ;
      func_0x00401590 ("Usage: %s [FILE]...\n  or:  %s [OPTION]\n",
		       DAT_00612828, DAT_00612828);
      uVar3 = DAT_00612780;
      func_0x00401720 ("Print CRC checksum and byte counts of each FILE.\n\n",
		       DAT_00612780);
      func_0x00401720 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401720
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040f835;
      local_80 = "test invocation";
      puVar6 = &DAT_0040f835;
      local_78 = 0x40f89e;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401820 ("cksum", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401590 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401880 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401780 (lVar2, &DAT_0040f8bf, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "cksum";
		  goto LAB_004020b9;
		}
	    }
	  func_0x00401590 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "cksum");
	LAB_004020e2:
	  ;
	  pcVar5 = "cksum";
	  uVar3 = 0x40f857;
	}
      else
	{
	  func_0x00401590 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401880 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401780 (lVar2, &DAT_0040f8bf, 3);
	      if (iVar1 != 0)
		{
		LAB_004020b9:
		  ;
		  func_0x00401590
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "cksum");
		}
	    }
	  func_0x00401590 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "cksum");
	  uVar3 = 0x410b2f;
	  if (pcVar5 == "cksum")
	    goto LAB_004020e2;
	}
      func_0x00401590
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}


#include "b2sum.h"

long null_ARRAY_0061850_0_4_;
long null_ARRAY_0061850_40_8_;
long null_ARRAY_0061850_4_4_;
long null_ARRAY_0061850_48_8_;
long null_ARRAY_0061854_0_8_;
long null_ARRAY_0061854_8_8_;
long null_ARRAY_0061878_0_8_;
long null_ARRAY_0061878_16_8_;
long null_ARRAY_0061878_24_8_;
long null_ARRAY_0061878_32_8_;
long null_ARRAY_0061878_40_8_;
long null_ARRAY_0061878_48_8_;
long null_ARRAY_0061878_8_8_;
long null_ARRAY_006187c_0_4_;
long null_ARRAY_006187c_16_8_;
long null_ARRAY_006187c_24_4_;
long null_ARRAY_006187c_32_8_;
long null_ARRAY_006187c_40_4_;
long null_ARRAY_006187c_4_4_;
long null_ARRAY_006187c_44_4_;
long null_ARRAY_006187c_48_4_;
long null_ARRAY_006187c_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_00414680;
long DAT_00414683;
long DAT_00414686;
long DAT_00414720;
long DAT_004147f7;
long DAT_004147fc;
long DAT_004147ff;
long DAT_00414804;
long DAT_004154d8;
long DAT_004154dc;
long DAT_004154e0;
long DAT_004154e3;
long DAT_004154e5;
long DAT_004154e9;
long DAT_004154ed;
long DAT_00415a6b;
long DAT_004162a8;
long DAT_004163b1;
long DAT_004163b7;
long DAT_004163c9;
long DAT_004163ca;
long DAT_004163e8;
long DAT_004163ec;
long DAT_00416476;
long DAT_006180b8;
long DAT_006180c8;
long DAT_006180d8;
long DAT_006184e0;
long DAT_006184f8;
long DAT_00618550;
long DAT_00618554;
long DAT_00618558;
long DAT_0061855c;
long DAT_00618580;
long DAT_00618588;
long DAT_00618590;
long DAT_006185a0;
long DAT_006185a8;
long DAT_006185c0;
long DAT_006185c8;
long DAT_00618610;
long DAT_00618618;
long DAT_0061861c;
long DAT_0061861d;
long DAT_0061861e;
long DAT_0061861f;
long DAT_00618620;
long DAT_00618628;
long DAT_00618630;
long DAT_00618638;
long DAT_00618640;
long DAT_00618648;
long DAT_00618650;
long DAT_006187b8;
long DAT_006187f8;
long DAT_00618800;
long DAT_00618808;
long DAT_00618818;
long _DYNAMIC;
long fde_00416e70;
long int7;
long null_ARRAY_004152c0;
long null_ARRAY_00415460;
long null_ARRAY_00415470;
long null_ARRAY_00416700;
long null_ARRAY_00416950;
long null_ARRAY_00618500;
long null_ARRAY_00618540;
long null_ARRAY_006185e0;
long null_ARRAY_00618680;
long null_ARRAY_00618780;
long null_ARRAY_006187c0;
long PTR_FUN_006184e8;
long PTR_null_ARRAY_00618538;
long register0x00000020;
long stack0x00000008;
void
FUN_00403160 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040318d;
  func_0x00401940 (DAT_006185a0, "Try \'%s --help\' for more information.\n",
		   DAT_00618650);
  do
    {
      func_0x00401b30 ((ulong) uParm1);
    LAB_0040318d:
      ;
      func_0x00401770
	("Usage: %s [OPTION]... [FILE]...\nPrint or check %s (%d-bit) checksums.\n",
	 DAT_00618650, "BLAKE2", 0x200);
      uVar3 = DAT_00618580;
      func_0x00401950
	("\nWith no FILE, or when FILE is -, read standard input.\n",
	 DAT_00618580);
      func_0x00401950 ("\n  -b, --binary         read in binary mode\n",
		       uVar3);
      func_0x00401770
	("  -c, --check          read %s sums from the FILEs and check them\n",
	 "BLAKE2");
      func_0x00401950
	("  -l, --length         digest length in bits; must not exceed the maximum for\n                       the blake2 algorithm and must be a multiple of 8\n",
	 uVar3);
      func_0x00401950 ("      --tag            create a BSD-style checksum\n",
		       uVar3);
      func_0x00401950 ("  -t, --text           read in text mode (default)\n",
		       uVar3);
      func_0x00401950
	("\nThe following five options are useful only when verifying checksums:\n      --ignore-missing  don\'t fail or report status for missing files\n      --quiet          don\'t print OK for each successfully verified file\n      --status         don\'t output anything, status code shows success\n      --strict         exit non-zero for improperly formatted checksum lines\n  -w, --warn           warn about improperly formatted checksum lines\n\n",
	 uVar3);
      func_0x00401950 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401950
	("      --version  output version information and exit\n", uVar3);
      func_0x00401770
	("\nThe sums are computed as described in %s.  When checking, the input\nshould be a former output of this program.  The default mode is to print a\nline with checksum, a space, a character indicating input mode (\'*\' for binary,\n\' \' for text or where binary is insignificant), and name for each FILE.\n",
	 "RFC 7693");
      local_88 = &DAT_00414686;
      local_80 = "test invocation";
      puVar6 = &DAT_00414686;
      local_78 = 0x4146ff;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401a70 ("b2sum", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401ad0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004019c0 (lVar2, &DAT_00414720, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "b2sum";
		  goto LAB_00403391;
		}
	    }
	  func_0x00401770 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "b2sum");
	LAB_004033ba:
	  ;
	  pcVar5 = "b2sum";
	  uVar3 = 0x4146b8;
	}
      else
	{
	  func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401ad0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004019c0 (lVar2, &DAT_00414720, 3);
	      if (iVar1 != 0)
		{
		LAB_00403391:
		  ;
		  func_0x00401770
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "b2sum");
		}
	    }
	  func_0x00401770 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "b2sum");
	  uVar3 = 0x4163e7;
	  if (pcVar5 == "b2sum")
	    goto LAB_004033ba;
	}
      func_0x00401770
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

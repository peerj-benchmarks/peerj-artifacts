
#include "sleep.h"

long null_ARRAY_0061248_0_4_;
long null_ARRAY_0061248_40_8_;
long null_ARRAY_0061248_4_4_;
long null_ARRAY_0061248_48_8_;
long null_ARRAY_006124c_0_8_;
long null_ARRAY_006124c_8_8_;
long null_ARRAY_006126c_0_8_;
long null_ARRAY_006126c_16_8_;
long null_ARRAY_006126c_24_8_;
long null_ARRAY_006126c_32_8_;
long null_ARRAY_006126c_40_8_;
long null_ARRAY_006126c_48_8_;
long null_ARRAY_006126c_8_8_;
long null_ARRAY_0061270_0_4_;
long null_ARRAY_0061270_16_8_;
long null_ARRAY_0061270_24_4_;
long null_ARRAY_0061270_32_8_;
long null_ARRAY_0061270_40_4_;
long null_ARRAY_0061270_4_4_;
long null_ARRAY_0061270_44_4_;
long null_ARRAY_0061270_48_4_;
long null_ARRAY_0061270_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_0040f740;
long DAT_0040f7ca;
long DAT_0040fb65;
long DAT_0040fb7a;
long DAT_0040fc60;
long DAT_0040fc64;
long DAT_0040fc68;
long DAT_0040fc6b;
long DAT_0040fc6d;
long DAT_0040fc71;
long DAT_0040fc75;
long DAT_0041022b;
long DAT_004106bd;
long DAT_004107c1;
long DAT_004107c7;
long DAT_004107d9;
long DAT_004107da;
long DAT_004107f8;
long DAT_0041089e;
long DAT_00612088;
long DAT_00612098;
long DAT_006120a8;
long DAT_00612468;
long DAT_006124d0;
long DAT_006124d4;
long DAT_006124d8;
long DAT_006124dc;
long DAT_00612500;
long DAT_00612510;
long DAT_00612520;
long DAT_00612528;
long DAT_00612540;
long DAT_00612548;
long DAT_00612590;
long DAT_00612598;
long DAT_006125a0;
long DAT_006125a8;
long DAT_00612738;
long DAT_00612740;
long DAT_00612748;
long DAT_00612758;
long _DYNAMIC;
long fde_00411240;
long int7;
long null_ARRAY_0040fb20;
long null_ARRAY_0040fbc0;
long null_ARRAY_00410b40;
long null_ARRAY_00410d80;
long null_ARRAY_00612480;
long null_ARRAY_006124c0;
long null_ARRAY_00612560;
long null_ARRAY_006125c0;
long null_ARRAY_006126c0;
long null_ARRAY_00612700;
long PTR_DAT_00612460;
long PTR_null_ARRAY_006124b8;
long register0x00000020;
void
FUN_00401cb0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401cdd;
  func_0x00401660 (DAT_00612520, "Try \'%s --help\' for more information.\n",
		   DAT_006125a8);
  do
    {
      func_0x004017f0 ((ulong) uParm1);
    LAB_00401cdd:
      ;
      func_0x004014d0
	("Usage: %s NUMBER[SUFFIX]...\n  or:  %s OPTION\nPause for NUMBER seconds.  SUFFIX may be \'s\' for seconds (the default),\n\'m\' for minutes, \'h\' for hours or \'d\' for days.  Unlike most implementations\nthat require NUMBER be an integer, here NUMBER may be an arbitrary floating\npoint number.  Given two or more arguments, pause for the amount of time\nspecified by the sum of their values.\n\n",
	 DAT_006125a8, DAT_006125a8);
      uVar3 = DAT_00612500;
      func_0x00401670 ("      --help     display this help and exit\n",
		       DAT_00612500);
      func_0x00401670
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040f740;
      local_80 = "test invocation";
      puVar6 = &DAT_0040f740;
      local_78 = 0x40f7a9;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401740 ("sleep", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x004014d0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004017b0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004016c0 (lVar2, &DAT_0040f7ca, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "sleep";
		  goto LAB_00401e69;
		}
	    }
	  func_0x004014d0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "sleep");
	LAB_00401e92:
	  ;
	  pcVar5 = "sleep";
	  uVar3 = 0x40f762;
	}
      else
	{
	  func_0x004014d0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004017b0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004016c0 (lVar2, &DAT_0040f7ca, 3);
	      if (iVar1 != 0)
		{
		LAB_00401e69:
		  ;
		  func_0x004014d0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "sleep");
		}
	    }
	  func_0x004014d0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "sleep");
	  uVar3 = 0x4107f7;
	  if (pcVar5 == "sleep")
	    goto LAB_00401e92;
	}
      func_0x004014d0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

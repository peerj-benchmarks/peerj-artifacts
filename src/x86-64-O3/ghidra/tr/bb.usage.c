
#include "tr.h"

long null_ARRAY_0061644_0_4_;
long null_ARRAY_0061644_40_8_;
long null_ARRAY_0061644_4_4_;
long null_ARRAY_0061644_48_8_;
long null_ARRAY_0061648_0_8_;
long null_ARRAY_0061648_8_8_;
long null_ARRAY_00616dc_0_8_;
long null_ARRAY_00616dc_16_8_;
long null_ARRAY_00616dc_24_8_;
long null_ARRAY_00616dc_32_8_;
long null_ARRAY_00616dc_40_8_;
long null_ARRAY_00616dc_48_8_;
long null_ARRAY_00616dc_8_8_;
long null_ARRAY_00616e0_0_4_;
long null_ARRAY_00616e0_16_8_;
long null_ARRAY_00616e0_24_4_;
long null_ARRAY_00616e0_32_8_;
long null_ARRAY_00616e0_40_4_;
long null_ARRAY_00616e0_4_4_;
long null_ARRAY_00616e0_44_4_;
long null_ARRAY_00616e0_48_4_;
long null_ARRAY_00616e0_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000009;
long DAT_00000010;
long DAT_00411f94;
long DAT_00411f96;
long DAT_00411f99;
long DAT_00411f9c;
long DAT_00411f9f;
long DAT_00411fa2;
long DAT_00411fa3;
long DAT_00411fa5;
long DAT_00411fa8;
long DAT_0041200a;
long DAT_0041200c;
long DAT_00412091;
long DAT_004134a2;
long DAT_004136c0;
long DAT_004136c4;
long DAT_004136c8;
long DAT_004136cb;
long DAT_004136cd;
long DAT_004136d1;
long DAT_004136d5;
long DAT_00413c6b;
long DAT_004142e8;
long DAT_004143f1;
long DAT_004143f7;
long DAT_00414409;
long DAT_0041440a;
long DAT_00414428;
long DAT_0041442c;
long DAT_004144b6;
long DAT_00616000;
long DAT_00616010;
long DAT_00616020;
long DAT_00616428;
long DAT_00616490;
long DAT_00616494;
long DAT_00616498;
long DAT_0061649c;
long DAT_006164c0;
long DAT_006164c8;
long DAT_006164d0;
long DAT_006164e0;
long DAT_006164e8;
long DAT_00616500;
long DAT_00616508;
long DAT_00616c80;
long DAT_00616c81;
long DAT_00616c82;
long DAT_00616c83;
long DAT_00616c84;
long DAT_00616c88;
long DAT_00616c90;
long DAT_00616c98;
long DAT_00616e38;
long DAT_00616e40;
long DAT_00616e48;
long DAT_00616e58;
long fde_00414ea8;
long int7;
long null_ARRAY_004134c0;
long null_ARRAY_004135c0;
long null_ARRAY_00414740;
long null_ARRAY_00414990;
long null_ARRAY_00616440;
long null_ARRAY_00616480;
long null_ARRAY_00616520;
long null_ARRAY_00616580;
long null_ARRAY_00616680;
long null_ARRAY_00616780;
long null_ARRAY_00616880;
long null_ARRAY_00616cc0;
long null_ARRAY_00616dc0;
long null_ARRAY_00616e00;
long PTR_DAT_00616420;
long PTR_null_ARRAY_00616478;
long register0x00000020;
void
FUN_00404120 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040414d;
  func_0x00401990 (DAT_006164e0, "Try \'%s --help\' for more information.\n",
		   DAT_00616c98);
  do
    {
      func_0x00401b60 ((ulong) uParm1);
    LAB_0040414d:
      ;
      func_0x004017c0 ("Usage: %s [OPTION]... SET1 [SET2]\n", DAT_00616c98);
      uVar3 = DAT_006164c0;
      func_0x004019a0
	("Translate, squeeze, and/or delete characters from standard input,\nwriting to standard output.\n\n  -c, -C, --complement    use the complement of SET1\n  -d, --delete            delete characters in SET1, do not translate\n  -s, --squeeze-repeats   replace each sequence of a repeated character\n                            that is listed in the last specified SET,\n                            with a single occurrence of that character\n  -t, --truncate-set1     first truncate SET1 to length of SET2\n",
	 DAT_006164c0);
      func_0x004019a0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004019a0
	("      --version  output version information and exit\n", uVar3);
      func_0x004019a0
	("\nSETs are specified as strings of characters.  Most represent themselves.\nInterpreted sequences are:\n\n  \\NNN            character with octal value NNN (1 to 3 octal digits)\n  \\\\              backslash\n  \\a              audible BEL\n  \\b              backspace\n  \\f              form feed\n  \\n              new line\n  \\r              return\n  \\t              horizontal tab\n",
	 uVar3);
      func_0x004019a0
	("  \\v              vertical tab\n  CHAR1-CHAR2     all characters from CHAR1 to CHAR2 in ascending order\n  [CHAR*]         in SET2, copies of CHAR until length of SET1\n  [CHAR*REPEAT]   REPEAT copies of CHAR, REPEAT octal if starting with 0\n  [:alnum:]       all letters and digits\n  [:alpha:]       all letters\n  [:blank:]       all horizontal whitespace\n  [:cntrl:]       all control characters\n  [:digit:]       all digits\n",
	 uVar3);
      func_0x004019a0
	("  [:graph:]       all printable characters, not including space\n  [:lower:]       all lower case letters\n  [:print:]       all printable characters, including space\n  [:punct:]       all punctuation characters\n  [:space:]       all horizontal or vertical whitespace\n  [:upper:]       all upper case letters\n  [:xdigit:]      all hexadecimal digits\n  [=CHAR=]        all characters which are equivalent to CHAR\n",
	 uVar3);
      func_0x004019a0
	("\nTranslation occurs if -d is not given and both SET1 and SET2 appear.\n-t may be used only when translating.  SET2 is extended to length of\nSET1 by repeating its last character as necessary.  Excess characters\nof SET2 are ignored.  Only [:lower:] and [:upper:] are guaranteed to\nexpand in ascending order; used in SET2 while translating, they may\nonly be used in pairs to specify case conversion.  -s uses the last\nspecified SET, and occurs after translation or deletion.\n",
	 uVar3);
      local_88 = &DAT_0041200a;
      local_80 = "test invocation";
      puVar5 = &DAT_0041200a;
      local_78 = 0x412070;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401ab0 (&DAT_0041200c, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x004017c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401b10 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401a10 (lVar2, &DAT_00412091, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0041200c;
		  goto LAB_00404319;
		}
	    }
	  func_0x004017c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041200c);
	LAB_00404342:
	  ;
	  puVar5 = &DAT_0041200c;
	  uVar3 = 0x412029;
	}
      else
	{
	  func_0x004017c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401b10 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401a10 (lVar2, &DAT_00412091, 3);
	      if (iVar1 != 0)
		{
		LAB_00404319:
		  ;
		  func_0x004017c0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0041200c);
		}
	    }
	  func_0x004017c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041200c);
	  uVar3 = 0x414427;
	  if (puVar5 == &DAT_0041200c)
	    goto LAB_00404342;
	}
      func_0x004017c0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

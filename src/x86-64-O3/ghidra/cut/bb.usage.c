
#include "cut.h"

long DAT_00614c1_0_1_;
long DAT_00614c1_1_1_;
long null_ARRAY_00614b0_0_4_;
long null_ARRAY_00614b0_40_8_;
long null_ARRAY_00614b0_4_4_;
long null_ARRAY_00614b0_48_8_;
long null_ARRAY_00614b4_0_8_;
long null_ARRAY_00614b4_8_8_;
long null_ARRAY_00614d8_0_8_;
long null_ARRAY_00614d8_16_8_;
long null_ARRAY_00614d8_24_8_;
long null_ARRAY_00614d8_32_8_;
long null_ARRAY_00614d8_40_8_;
long null_ARRAY_00614d8_48_8_;
long null_ARRAY_00614d8_8_8_;
long null_ARRAY_00614dc_0_4_;
long null_ARRAY_00614dc_16_8_;
long null_ARRAY_00614dc_24_4_;
long null_ARRAY_00614dc_32_8_;
long null_ARRAY_00614dc_40_4_;
long null_ARRAY_00614dc_4_4_;
long null_ARRAY_00614dc_44_4_;
long null_ARRAY_00614dc_48_4_;
long null_ARRAY_00614dc_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long uVar8;
long DAT_00000008;
long DAT_00000010;
long DAT_00411357;
long DAT_00411359;
long DAT_004113df;
long DAT_00412088;
long DAT_0041208c;
long DAT_00412090;
long DAT_00412093;
long DAT_00412095;
long DAT_00412099;
long DAT_0041209d;
long DAT_0041262b;
long DAT_00412ab5;
long DAT_00412bb9;
long DAT_00412bbf;
long DAT_00412bd1;
long DAT_00412bd2;
long DAT_00412bf0;
long DAT_00412bf4;
long DAT_00412c7e;
long DAT_00614698;
long DAT_006146a8;
long DAT_006146b8;
long DAT_00614ae0;
long DAT_00614af0;
long DAT_00614b50;
long DAT_00614b54;
long DAT_00614b58;
long DAT_00614b5c;
long DAT_00614b80;
long DAT_00614b88;
long DAT_00614b90;
long DAT_00614ba0;
long DAT_00614ba8;
long DAT_00614bc0;
long DAT_00614bc8;
long DAT_00614c10;
long DAT_00614c12;
long DAT_00614c18;
long DAT_00614c20;
long DAT_00614c28;
long DAT_00614c29;
long DAT_00614c2a;
long DAT_00614c2b;
long DAT_00614c2c;
long DAT_00614c30;
long DAT_00614c38;
long DAT_00614c40;
long DAT_00614c48;
long DAT_00614c50;
long DAT_00614c58;
long DAT_00614c60;
long DAT_00614c68;
long DAT_00614df8;
long DAT_00614e00;
long DAT_00614e08;
long DAT_00614e10;
long DAT_00614e18;
long DAT_00614e28;
long fde_00413658;
long int7;
long null_ARRAY_00411d40;
long null_ARRAY_00412f20;
long null_ARRAY_00413170;
long null_ARRAY_00614b00;
long null_ARRAY_00614b40;
long null_ARRAY_00614be0;
long null_ARRAY_00614c80;
long null_ARRAY_00614d80;
long null_ARRAY_00614dc0;
long PTR_DAT_00614ae8;
long PTR_null_ARRAY_00614b38;
long register0x00000020;
void
FUN_00402940 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040296d;
  func_0x00401a80 (DAT_00614ba0, "Try \'%s --help\' for more information.\n",
		   DAT_00614c68);
  do
    {
      func_0x00401ca0 ((ulong) uParm1);
    LAB_0040296d:
      ;
      func_0x004018c0 ("Usage: %s OPTION... [FILE]...\n", DAT_00614c68);
      uVar3 = DAT_00614b80;
      func_0x00401a90
	("Print selected parts of lines from each FILE to standard output.\n",
	 DAT_00614b80);
      func_0x00401a90
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
      func_0x00401a90
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401a90
	("  -b, --bytes=LIST        select only these bytes\n  -c, --characters=LIST   select only these characters\n  -d, --delimiter=DELIM   use DELIM instead of TAB for field delimiter\n",
	 uVar3);
      func_0x00401a90
	("  -f, --fields=LIST       select only these fields;  also print any line\n                            that contains no delimiter character, unless\n                            the -s option is specified\n  -n                      (ignored)\n",
	 uVar3);
      func_0x00401a90
	("      --complement        complement the set of selected bytes, characters\n                            or fields\n",
	 uVar3);
      func_0x00401a90
	("  -s, --only-delimited    do not print lines not containing delimiters\n      --output-delimiter=STRING  use STRING as the output delimiter\n                            the default is to use the input delimiter\n",
	 uVar3);
      func_0x00401a90
	("  -z, --zero-terminated    line delimiter is NUL, not newline\n",
	 uVar3);
      func_0x00401a90 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401a90
	("      --version  output version information and exit\n", uVar3);
      func_0x00401a90
	("\nUse one, and only one of -b, -c or -f.  Each LIST is made up of one\nrange, or many ranges separated by commas.  Selected input is written\nin the same order that it is read, and is written exactly once.\n",
	 uVar3);
      func_0x00401a90
	("Each range is one of:\n\n  N     N\'th byte, character or field, counted from 1\n  N-    from N\'th byte, character or field, to end of line\n  N-M   from N\'th to M\'th (included) byte, character or field\n  -M    from first to M\'th (included) byte, character or field\n",
	 uVar3);
      local_88 = &DAT_00411357;
      local_80 = "test invocation";
      puVar5 = &DAT_00411357;
      local_78 = 0x4113be;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401bb0 (&DAT_00411359, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x004018c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401c20 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401b00 (lVar2, &DAT_004113df, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_00411359;
		  goto LAB_00402b79;
		}
	    }
	  func_0x004018c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00411359);
	LAB_00402ba2:
	  ;
	  puVar5 = &DAT_00411359;
	  uVar3 = 0x411377;
	}
      else
	{
	  func_0x004018c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401c20 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401b00 (lVar2, &DAT_004113df, 3);
	      if (iVar1 != 0)
		{
		LAB_00402b79:
		  ;
		  func_0x004018c0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_00411359);
		}
	    }
	  func_0x004018c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00411359);
	  uVar3 = 0x412bef;
	  if (puVar5 == &DAT_00411359)
	    goto LAB_00402ba2;
	}
      func_0x004018c0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}


#include "split.h"

long null_ARRAY_006184c_0_4_;
long null_ARRAY_006184c_40_8_;
long null_ARRAY_006184c_4_4_;
long null_ARRAY_006184c_48_8_;
long null_ARRAY_0061850_0_8_;
long null_ARRAY_0061850_8_8_;
long null_ARRAY_0061880_0_8_;
long null_ARRAY_0061880_24_4_;
long null_ARRAY_0061880_48_8_;
long null_ARRAY_0061880_56_8_;
long null_ARRAY_0061880_8_8_;
long null_ARRAY_00618b4_0_8_;
long null_ARRAY_00618b4_16_8_;
long null_ARRAY_00618b4_24_8_;
long null_ARRAY_00618b4_32_8_;
long null_ARRAY_00618b4_40_8_;
long null_ARRAY_00618b4_48_8_;
long null_ARRAY_00618b4_8_8_;
long null_ARRAY_00618b8_0_4_;
long null_ARRAY_00618b8_16_8_;
long null_ARRAY_00618b8_24_4_;
long null_ARRAY_00618b8_32_8_;
long null_ARRAY_00618b8_40_4_;
long null_ARRAY_00618b8_4_4_;
long null_ARRAY_00618b8_44_4_;
long null_ARRAY_00618b8_48_4_;
long null_ARRAY_00618b8_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_00413ddb;
long DAT_00413e5a;
long DAT_00413e90;
long DAT_00413eac;
long DAT_00413eaf;
long DAT_00413f39;
long DAT_00413fba;
long DAT_00413fbd;
long DAT_00414005;
long DAT_00414006;
long DAT_004150e8;
long DAT_004150ec;
long DAT_004150f0;
long DAT_004150f3;
long DAT_004150f5;
long DAT_004150f9;
long DAT_004150fd;
long DAT_004156ab;
long DAT_00415d28;
long DAT_00415e31;
long DAT_00415e37;
long DAT_00415e49;
long DAT_00415e4a;
long DAT_00415e68;
long DAT_00415e78;
long DAT_00415e7c;
long DAT_00618000;
long DAT_00618010;
long DAT_00618020;
long DAT_00618480;
long DAT_00618484;
long DAT_00618490;
long DAT_006184a0;
long DAT_00618510;
long DAT_00618514;
long DAT_00618518;
long DAT_0061851c;
long DAT_00618540;
long DAT_00618544;
long DAT_00618700;
long DAT_00618710;
long DAT_00618720;
long DAT_00618728;
long DAT_00618740;
long DAT_00618748;
long DAT_006187c0;
long DAT_006187c8;
long DAT_006187d0;
long DAT_006187d8;
long DAT_006187e0;
long DAT_006187e1;
long DAT_006187e2;
long DAT_00618890;
long DAT_00618898;
long DAT_006188a0;
long DAT_006188a8;
long DAT_006188b0;
long DAT_006188b8;
long DAT_006188c0;
long DAT_00618a00;
long DAT_00618a08;
long DAT_00618a10;
long DAT_00618a18;
long DAT_00618a20;
long DAT_00618a28;
long DAT_00618a30;
long DAT_00618a38;
long DAT_00618bb8;
long DAT_006193c8;
long DAT_006193d0;
long DAT_006193d8;
long DAT_006193e8;
long fde_00416968;
long int7;
long null_ARRAY_00414e43;
long null_ARRAY_004161a0;
long null_ARRAY_004163f0;
long null_ARRAY_006184c0;
long null_ARRAY_00618500;
long null_ARRAY_00618760;
long null_ARRAY_00618800;
long null_ARRAY_00618900;
long null_ARRAY_00618980;
long null_ARRAY_00618a40;
long null_ARRAY_00618b40;
long null_ARRAY_00618b80;
long null_ARRAY_00618bc0;
long PTR_DAT_00618498;
long PTR_null_ARRAY_006184f8;
long PTR_s_abcdefghijklmnopqrstuvwxyz_00618488;
long register0x00000020;
long stack0x00000008;
void
FUN_004051e0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040520d;
  func_0x00401ca0 (DAT_00618720, "Try \'%s --help\' for more information.\n",
		   DAT_00618a38);
  do
    {
      func_0x00401e80 ((ulong) uParm1);
    LAB_0040520d:
      ;
      func_0x00401a30 ("Usage: %s [OPTION]... [FILE [PREFIX]]\n",
		       DAT_00618a38);
      uVar3 = DAT_00618700;
      func_0x00401cb0
	("Output pieces of FILE to PREFIXaa, PREFIXab, ...;\ndefault size is 1000 lines, and default PREFIX is \'x\'.\n",
	 DAT_00618700);
      func_0x00401cb0
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
      func_0x00401cb0
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401ca0 (uVar3,
		       "  -a, --suffix-length=N   generate suffixes of length N (default %d)\n      --additional-suffix=SUFFIX  append an additional SUFFIX to file names\n  -b, --bytes=SIZE        put SIZE bytes per output file\n  -C, --line-bytes=SIZE   put at most SIZE bytes of records per output file\n  -d                      use numeric suffixes starting at 0, not alphabetic\n      --numeric-suffixes[=FROM]  same as -d, but allow setting the start value\n  -x                      use hex suffixes starting at 0, not alphabetic\n      --hex-suffixes[=FROM]  same as -x, but allow setting the start value\n  -e, --elide-empty-files  do not generate empty output files with \'-n\'\n      --filter=COMMAND    write to shell COMMAND; file name is $FILE\n  -l, --lines=NUMBER      put NUMBER lines/records per output file\n  -n, --number=CHUNKS     generate CHUNKS output files; see explanation below\n  -t, --separator=SEP     use SEP instead of newline as the record separator;\n                            \'\\0\' (zero) specifies the NUL character\n  -u, --unbuffered        immediately copy input to output with \'-n r/...\'\n",
		       2);
      func_0x00401cb0
	("      --verbose           print a diagnostic just before each\n                            output file is opened\n",
	 uVar3);
      func_0x00401cb0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401cb0
	("      --version  output version information and exit\n", uVar3);
      func_0x00401cb0
	("\nThe SIZE argument is an integer and optional unit (example: 10K is 10*1024).\nUnits are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers of 1000).\n",
	 uVar3);
      func_0x00401cb0
	("\nCHUNKS may be:\n  N       split into N files based on size of input\n  K/N     output Kth of N to stdout\n  l/N     split into N files without splitting lines/records\n  l/K/N   output Kth of N to stdout without splitting lines/records\n  r/N     like \'l\' but use round robin distribution\n  r/K/N   likewise but only output Kth of N to stdout\n",
	 uVar3);
      local_88 = &DAT_00413eaf;
      local_80 = "test invocation";
      puVar6 = &DAT_00413eaf;
      local_78 = 0x413f18;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401dd0 ("split", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401a30 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401e30 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401d10 (lVar2, &DAT_00413f39, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "split";
		  goto LAB_004053f9;
		}
	    }
	  func_0x00401a30 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "split");
	LAB_00405422:
	  ;
	  pcVar5 = "split";
	  uVar3 = 0x413ed1;
	}
      else
	{
	  func_0x00401a30 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401e30 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401d10 (lVar2, &DAT_00413f39, 3);
	      if (iVar1 != 0)
		{
		LAB_004053f9:
		  ;
		  func_0x00401a30
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "split");
		}
	    }
	  func_0x00401a30 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "split");
	  uVar3 = 0x415e67;
	  if (pcVar5 == "split")
	    goto LAB_00405422;
	}
      func_0x00401a30
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

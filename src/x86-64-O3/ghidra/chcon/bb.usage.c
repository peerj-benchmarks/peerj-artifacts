
#include "chcon.h"

long null_ARRAY_0061a48_0_4_;
long null_ARRAY_0061a48_40_8_;
long null_ARRAY_0061a48_4_4_;
long null_ARRAY_0061a48_48_8_;
long null_ARRAY_0061a4c_0_8_;
long null_ARRAY_0061a4c_8_8_;
long null_ARRAY_0061a70_0_8_;
long null_ARRAY_0061a70_16_8_;
long null_ARRAY_0061a70_24_8_;
long null_ARRAY_0061a70_32_8_;
long null_ARRAY_0061a70_40_8_;
long null_ARRAY_0061a70_48_8_;
long null_ARRAY_0061a70_8_8_;
long null_ARRAY_0061a74_0_4_;
long null_ARRAY_0061a74_16_8_;
long null_ARRAY_0061a74_24_4_;
long null_ARRAY_0061a74_32_8_;
long null_ARRAY_0061a74_40_4_;
long null_ARRAY_0061a74_4_4_;
long null_ARRAY_0061a74_44_4_;
long null_ARRAY_0061a74_48_4_;
long null_ARRAY_0061a74_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_00415880;
long DAT_0041590a;
long DAT_00416798;
long DAT_0041679c;
long DAT_004167a0;
long DAT_004167a3;
long DAT_004167a5;
long DAT_004167a9;
long DAT_004167ad;
long DAT_00416d2b;
long DAT_004171da;
long DAT_004171eb;
long DAT_004171ec;
long DAT_00417339;
long DAT_0041733a;
long DAT_00417358;
long DAT_00417392;
long DAT_004174d5;
long DAT_004174df;
long DAT_00417561;
long DAT_0061a000;
long DAT_0061a010;
long DAT_0061a020;
long DAT_0061a468;
long DAT_0061a4d0;
long DAT_0061a4d4;
long DAT_0061a4d8;
long DAT_0061a4dc;
long DAT_0061a500;
long DAT_0061a510;
long DAT_0061a520;
long DAT_0061a528;
long DAT_0061a540;
long DAT_0061a548;
long DAT_0061a5a0;
long DAT_0061a5a8;
long DAT_0061a5b0;
long DAT_0061a5b8;
long DAT_0061a5c0;
long DAT_0061a5c8;
long DAT_0061a5d0;
long DAT_0061a5d1;
long DAT_0061a5d2;
long DAT_0061a5d8;
long DAT_0061a5e0;
long DAT_0061a5e8;
long DAT_0061a778;
long DAT_0061a780;
long DAT_0061a784;
long DAT_0061a788;
long DAT_0061a790;
long DAT_0061a798;
long DAT_0061a7a8;
long fde_004180c0;
long int7;
long null_ARRAY_00416580;
long null_ARRAY_00417200;
long null_ARRAY_004173a0;
long null_ARRAY_00417800;
long null_ARRAY_00417a40;
long null_ARRAY_0061a480;
long null_ARRAY_0061a4c0;
long null_ARRAY_0061a560;
long null_ARRAY_0061a590;
long null_ARRAY_0061a600;
long null_ARRAY_0061a700;
long null_ARRAY_0061a740;
long PTR_DAT_0061a460;
long PTR_null_ARRAY_0061a4b8;
long register0x00000020;
void
FUN_004028b0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004028dd;
  func_0x00401ad0 (DAT_0061a520, "Try \'%s --help\' for more information.\n",
		   DAT_0061a5e8);
  do
    {
      func_0x00401cb0 ((ulong) uParm1);
    LAB_004028dd:
      ;
      func_0x004018c0
	("Usage: %s [OPTION]... CONTEXT FILE...\n  or:  %s [OPTION]... [-u USER] [-r ROLE] [-l RANGE] [-t TYPE] FILE...\n  or:  %s [OPTION]... --reference=RFILE FILE...\n",
	 DAT_0061a5e8, DAT_0061a5e8, DAT_0061a5e8);
      uVar3 = DAT_0061a500;
      func_0x00401ae0
	("Change the SELinux security context of each FILE to CONTEXT.\nWith --reference, change the security context of each FILE to that of RFILE.\n",
	 DAT_0061a500);
      func_0x00401ae0
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401ae0
	("      --dereference      affect the referent of each symbolic link (this is\n                         the default), rather than the symbolic link itself\n  -h, --no-dereference   affect symbolic links instead of any referenced file\n",
	 uVar3);
      func_0x00401ae0
	("  -u, --user=USER        set user USER in the target security context\n  -r, --role=ROLE        set role ROLE in the target security context\n  -t, --type=TYPE        set type TYPE in the target security context\n  -l, --range=RANGE      set range RANGE in the target security context\n",
	 uVar3);
      func_0x00401ae0
	("      --no-preserve-root  do not treat \'/\' specially (the default)\n      --preserve-root    fail to operate recursively on \'/\'\n",
	 uVar3);
      func_0x00401ae0
	("      --reference=RFILE  use RFILE\'s security context rather than specifying\n                         a CONTEXT value\n",
	 uVar3);
      func_0x00401ae0
	("  -R, --recursive        operate on files and directories recursively\n",
	 uVar3);
      func_0x00401ae0
	("  -v, --verbose          output a diagnostic for every file processed\n",
	 uVar3);
      func_0x00401ae0
	("\nThe following options modify how a hierarchy is traversed when the -R\noption is also specified.  If more than one is specified, only the final\none takes effect.\n\n  -H                     if a command line argument is a symbolic link\n                         to a directory, traverse it\n  -L                     traverse every symbolic link to a directory\n                         encountered\n  -P                     do not traverse any symbolic links (default)\n\n",
	 uVar3);
      func_0x00401ae0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401ae0
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_00415880;
      local_80 = "test invocation";
      puVar6 = &DAT_00415880;
      local_78 = 0x4158e9;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401bf0 ("chcon", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x004018c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401c50 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401b60 (lVar2, &DAT_0041590a, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "chcon";
		  goto LAB_00402ae1;
		}
	    }
	  func_0x004018c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "chcon");
	LAB_00402b0a:
	  ;
	  pcVar5 = "chcon";
	  uVar3 = 0x4158a2;
	}
      else
	{
	  func_0x004018c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401c50 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401b60 (lVar2, &DAT_0041590a, 3);
	      if (iVar1 != 0)
		{
		LAB_00402ae1:
		  ;
		  func_0x004018c0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "chcon");
		}
	    }
	  func_0x004018c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "chcon");
	  uVar3 = 0x417357;
	  if (pcVar5 == "chcon")
	    goto LAB_00402b0a;
	}
      func_0x004018c0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}


#include "paste.h"

long null_ARRAY_00612ac_0_8_;
long null_ARRAY_00612ac_8_8_;
long null_ARRAY_00612cc_0_8_;
long null_ARRAY_00612cc_16_8_;
long null_ARRAY_00612cc_24_8_;
long null_ARRAY_00612cc_32_8_;
long null_ARRAY_00612cc_40_8_;
long null_ARRAY_00612cc_48_8_;
long null_ARRAY_00612cc_8_8_;
long null_ARRAY_00612d0_0_4_;
long null_ARRAY_00612d0_16_8_;
long null_ARRAY_00612d0_24_4_;
long null_ARRAY_00612d0_32_8_;
long null_ARRAY_00612d0_40_4_;
long null_ARRAY_00612d0_4_4_;
long null_ARRAY_00612d0_44_4_;
long null_ARRAY_00612d0_48_4_;
long null_ARRAY_00612d0_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_0040fd25;
long DAT_0040fdaf;
long DAT_0040fdb7;
long DAT_0040fdd7;
long DAT_004102d0;
long DAT_004102d4;
long DAT_004102d8;
long DAT_004102db;
long DAT_004102dd;
long DAT_004102e1;
long DAT_004102e5;
long DAT_0041086b;
long DAT_00410cf5;
long DAT_00410df9;
long DAT_00410dff;
long DAT_00410e11;
long DAT_00410e12;
long DAT_00410e30;
long DAT_00410e34;
long DAT_00410ebe;
long DAT_00612688;
long DAT_00612698;
long DAT_006126a8;
long DAT_00612a60;
long DAT_00612a70;
long DAT_00612ad0;
long DAT_00612ad4;
long DAT_00612ad8;
long DAT_00612adc;
long DAT_00612b00;
long DAT_00612b08;
long DAT_00612b10;
long DAT_00612b20;
long DAT_00612b28;
long DAT_00612b40;
long DAT_00612b48;
long DAT_00612b90;
long DAT_00612b98;
long DAT_00612ba0;
long DAT_00612ba1;
long DAT_00612ba8;
long DAT_00612bb0;
long DAT_00612bb8;
long DAT_00612d38;
long DAT_00612d40;
long DAT_00612d48;
long DAT_00612d58;
long fde_00411868;
long int7;
long null_ARRAY_004101c0;
long null_ARRAY_00411160;
long null_ARRAY_004113b0;
long null_ARRAY_00612ac0;
long null_ARRAY_00612b60;
long null_ARRAY_00612bc0;
long null_ARRAY_00612cc0;
long null_ARRAY_00612d00;
long PTR_DAT_00612a68;
long PTR_null_ARRAY_00612ab8;
long register0x00000020;
void
FUN_004024f0 (uint uParm1)
{
  undefined8 uVar1;
  int iVar2;
  long lVar3;
  undefined1 *puVar4;
  undefined **ppuVar5;
  char *pcVar6;
  undefined *puVar7;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040251d;
  func_0x00401720 (DAT_00612b20, "Try \'%s --help\' for more information.\n",
		   DAT_00612bb8);
  do
    {
      func_0x004018e0 ((ulong) uParm1);
    LAB_0040251d:
      ;
      func_0x004015a0 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00612bb8);
      uVar1 = DAT_00612b00;
      func_0x00401730
	("Write lines consisting of the sequentially corresponding lines from\neach FILE, separated by TABs, to standard output.\n",
	 DAT_00612b00);
      func_0x00401730
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar1);
      func_0x00401730
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar1);
      func_0x00401730
	("  -d, --delimiters=LIST   reuse characters from LIST instead of TABs\n  -s, --serial            paste one file at a time instead of in parallel\n",
	 uVar1);
      func_0x00401730
	("  -z, --zero-terminated    line delimiter is NUL, not newline\n",
	 uVar1);
      func_0x00401730 ("      --help     display this help and exit\n",
		       uVar1);
      func_0x00401730
	("      --version  output version information and exit\n", uVar1);
      local_88 = &DAT_0040fd25;
      local_80 = "test invocation";
      puVar7 = &DAT_0040fd25;
      local_78 = 0x40fd8e;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar5 = &local_88;
      do
	{
	  iVar2 = func_0x00401820 ("paste", puVar7);
	  if (iVar2 == 0)
	    break;
	  ppuVar5 = ppuVar5 + 2;
	  puVar7 = *ppuVar5;
	}
      while (puVar7 != (undefined *) 0x0);
      pcVar6 = ppuVar5[1];
      if (pcVar6 == (char *) 0x0)
	{
	  func_0x004015a0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar3 = func_0x00401880 (5, 0);
	  if (lVar3 != 0)
	    {
	      iVar2 = func_0x00401790 (lVar3, &DAT_0040fdaf, 3);
	      if (iVar2 != 0)
		{
		  pcVar6 = "paste";
		  goto LAB_004026e9;
		}
	    }
	  func_0x004015a0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "paste");
	LAB_00402712:
	  ;
	  pcVar6 = "paste";
	  puVar4 = (undefined1 *) 0x40fd47;
	}
      else
	{
	  func_0x004015a0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar3 = func_0x00401880 (5, 0);
	  if (lVar3 != 0)
	    {
	      iVar2 = func_0x00401790 (lVar3, &DAT_0040fdaf, 3);
	      if (iVar2 != 0)
		{
		LAB_004026e9:
		  ;
		  func_0x004015a0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "paste");
		}
	    }
	  func_0x004015a0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "paste");
	  puVar4 = &DAT_0040fdb7;
	  if (pcVar6 == "paste")
	    goto LAB_00402712;
	}
      func_0x004015a0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar6,
	 puVar4);
    }
  while (true);
}

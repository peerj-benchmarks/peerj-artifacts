
#include "basename.h"

long null_ARRAY_006123e_0_4_;
long null_ARRAY_006123e_40_8_;
long null_ARRAY_006123e_4_4_;
long null_ARRAY_006123e_48_8_;
long null_ARRAY_0061242_0_8_;
long null_ARRAY_0061242_8_8_;
long null_ARRAY_0061260_0_8_;
long null_ARRAY_0061260_16_8_;
long null_ARRAY_0061260_24_8_;
long null_ARRAY_0061260_32_8_;
long null_ARRAY_0061260_40_8_;
long null_ARRAY_0061260_48_8_;
long null_ARRAY_0061260_8_8_;
long null_ARRAY_0061264_0_4_;
long null_ARRAY_0061264_16_8_;
long null_ARRAY_0061264_24_4_;
long null_ARRAY_0061264_32_8_;
long null_ARRAY_0061264_40_4_;
long null_ARRAY_0061264_4_4_;
long null_ARRAY_0061264_44_4_;
long null_ARRAY_0061264_48_4_;
long null_ARRAY_0061264_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_0040f480;
long DAT_0040f50d;
long DAT_0040fa98;
long DAT_0040fa9c;
long DAT_0040faa0;
long DAT_0040faa3;
long DAT_0040faa5;
long DAT_0040faa9;
long DAT_0040faad;
long DAT_0041002b;
long DAT_004104b5;
long DAT_004105b9;
long DAT_004105bf;
long DAT_004105d1;
long DAT_004105d2;
long DAT_004105f0;
long DAT_004105f4;
long DAT_0041067e;
long DAT_00612000;
long DAT_00612010;
long DAT_00612020;
long DAT_006123c8;
long DAT_00612430;
long DAT_00612434;
long DAT_00612438;
long DAT_0061243c;
long DAT_00612440;
long DAT_00612450;
long DAT_00612460;
long DAT_00612468;
long DAT_00612480;
long DAT_00612488;
long DAT_006124d0;
long DAT_006124d8;
long DAT_006124e0;
long DAT_00612678;
long DAT_00612680;
long DAT_00612688;
long DAT_00612698;
long _DYNAMIC;
long fde_00411030;
long int7;
long null_ARRAY_0040f980;
long null_ARRAY_00410920;
long null_ARRAY_00410b70;
long null_ARRAY_006123e0;
long null_ARRAY_00612420;
long null_ARRAY_006124a0;
long null_ARRAY_00612500;
long null_ARRAY_00612600;
long null_ARRAY_00612640;
long PTR_DAT_006123c0;
long PTR_null_ARRAY_00612418;
long register0x00000020;
long stack0x00000008;
void
FUN_00401c50 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401c7d;
  func_0x00401620 (DAT_00612460, "Try \'%s --help\' for more information.\n",
		   DAT_006124e0);
  do
    {
      func_0x004017b0 ((ulong) uParm1);
    LAB_00401c7d:
      ;
      func_0x004014a0
	("Usage: %s NAME [SUFFIX]\n  or:  %s OPTION... NAME...\n",
	 DAT_006124e0, DAT_006124e0);
      uVar3 = DAT_00612440;
      func_0x00401630
	("Print NAME with any leading directory components removed.\nIf specified, also remove a trailing SUFFIX.\n",
	 DAT_00612440);
      func_0x00401630
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401630
	("  -a, --multiple       support multiple arguments and treat each as a NAME\n  -s, --suffix=SUFFIX  remove a trailing SUFFIX; implies -a\n  -z, --zero           end each output line with NUL, not newline\n",
	 uVar3);
      func_0x00401630 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401630
	("      --version  output version information and exit\n", uVar3);
      func_0x004014a0
	("\nExamples:\n  %s /usr/bin/sort          -> \"sort\"\n  %s include/stdio.h .h     -> \"stdio\"\n  %s -s .h include/stdio.h  -> \"stdio\"\n  %s -a any/str1 any/str2   -> \"str1\" followed by \"str2\"\n",
	 DAT_006124e0, DAT_006124e0, DAT_006124e0, DAT_006124e0);
      local_88 = &DAT_0040f480;
      local_80 = "test invocation";
      puVar6 = &DAT_0040f480;
      local_78 = 0x40f4ec;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401700 ("basename", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x004014a0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401760 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401680 (lVar2, &DAT_0040f50d, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "basename";
		  goto LAB_00401e49;
		}
	    }
	  func_0x004014a0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "basename");
	LAB_00401e72:
	  ;
	  pcVar5 = "basename";
	  uVar3 = 0x40f4a5;
	}
      else
	{
	  func_0x004014a0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401760 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401680 (lVar2, &DAT_0040f50d, 3);
	      if (iVar1 != 0)
		{
		LAB_00401e49:
		  ;
		  func_0x004014a0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "basename");
		}
	    }
	  func_0x004014a0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "basename");
	  uVar3 = 0x4105ef;
	  if (pcVar5 == "basename")
	    goto LAB_00401e72;
	}
      func_0x004014a0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

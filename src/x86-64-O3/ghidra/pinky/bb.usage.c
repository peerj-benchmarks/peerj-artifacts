
#include "pinky.h"

long null_ARRAY_0061352_0_8_;
long null_ARRAY_0061352_8_8_;
long null_ARRAY_0061368_0_4_;
long null_ARRAY_0061380_0_8_;
long null_ARRAY_0061380_16_8_;
long null_ARRAY_0061380_24_8_;
long null_ARRAY_0061380_32_8_;
long null_ARRAY_0061380_40_8_;
long null_ARRAY_0061380_48_8_;
long null_ARRAY_0061380_8_8_;
long null_ARRAY_0061384_0_4_;
long null_ARRAY_0061384_16_8_;
long null_ARRAY_0061384_24_4_;
long null_ARRAY_0061384_32_8_;
long null_ARRAY_0061384_40_4_;
long null_ARRAY_0061384_4_4_;
long null_ARRAY_0061384_44_4_;
long null_ARRAY_0061384_48_4_;
long null_ARRAY_0061384_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_004104a2;
long DAT_0041050b;
long DAT_0041051f;
long DAT_00410537;
long DAT_004105d0;
long DAT_0041062c;
long DAT_00410631;
long DAT_0041063d;
long DAT_00410648;
long DAT_0041064d;
long DAT_00410bb8;
long DAT_00410c00;
long DAT_00410c04;
long DAT_00410c08;
long DAT_00410c0b;
long DAT_00410c0d;
long DAT_00410c11;
long DAT_00410c15;
long DAT_004111ab;
long DAT_00411635;
long DAT_00411739;
long DAT_0041173f;
long DAT_00411751;
long DAT_00411752;
long DAT_00411770;
long DAT_004117f6;
long DAT_006130a8;
long DAT_006130b8;
long DAT_006130c8;
long DAT_006134c0;
long DAT_006134c1;
long DAT_006134c2;
long DAT_006134c3;
long DAT_006134c4;
long DAT_006134c5;
long DAT_006134c6;
long DAT_006134c7;
long DAT_006134d0;
long DAT_00613530;
long DAT_00613534;
long DAT_00613538;
long DAT_0061353c;
long DAT_00613580;
long DAT_00613590;
long DAT_006135a0;
long DAT_006135a8;
long DAT_006135c0;
long DAT_006135c8;
long DAT_00613668;
long DAT_00613670;
long DAT_00613678;
long DAT_006136b0;
long DAT_006136b8;
long DAT_006136c0;
long DAT_006136c8;
long DAT_006138b8;
long DAT_006138c0;
long DAT_006138c8;
long DAT_006138d8;
long _DYNAMIC;
long fde_004121a0;
long int7;
long null_ARRAY_00410b40;
long null_ARRAY_00411a80;
long null_ARRAY_00411cd0;
long null_ARRAY_00613520;
long null_ARRAY_006135e0;
long null_ARRAY_00613620;
long null_ARRAY_00613650;
long null_ARRAY_00613680;
long null_ARRAY_00613700;
long null_ARRAY_00613800;
long null_ARRAY_00613840;
long null_ARRAY_00613880;
long PTR_DAT_006134c8;
long PTR_null_ARRAY_00613518;
long PTR_null_ARRAY_00613540;
long register0x00000020;
void
FUN_00402930 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040295d;
  func_0x004018c0 (DAT_006135a0, "Try \'%s --help\' for more information.\n",
		   DAT_006136c8);
  do
    {
      func_0x00401ae0 ((ulong) uParm1);
    LAB_0040295d:
      ;
      func_0x00401740 ("Usage: %s [OPTION]... [USER]...\n", DAT_006136c8);
      uVar3 = DAT_00613580;
      func_0x004018e0
	("\n  -l              produce long format output for the specified USERs\n  -b              omit the user\'s home directory and shell in long format\n  -h              omit the user\'s project file in long format\n  -p              omit the user\'s plan file in long format\n  -s              do short format output, this is the default\n",
	 DAT_00613580);
      func_0x004018e0
	("  -f              omit the line of column headings in short format\n  -w              omit the user\'s full name in short format\n  -i              omit the user\'s full name and remote host in short format\n  -q              omit the user\'s full name, remote host and idle time\n                  in short format\n",
	 uVar3);
      func_0x004018e0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004018e0
	("      --version  output version information and exit\n", uVar3);
      func_0x00401740
	("\nA lightweight \'finger\' program;  print user information.\nThe utmp file will be %s.\n",
	 "/dev/null/utmp");
      local_88 = &DAT_00410537;
      local_80 = "test invocation";
      puVar6 = &DAT_00410537;
      local_78 = 0x4105af;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004019e0 ("pinky", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401740 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401a50 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401930 (lVar2, &DAT_004105d0, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "pinky";
		  goto LAB_00402b11;
		}
	    }
	  func_0x00401740 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "pinky");
	LAB_00402b3a:
	  ;
	  pcVar5 = "pinky";
	  uVar3 = 0x410568;
	}
      else
	{
	  func_0x00401740 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401a50 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401930 (lVar2, &DAT_004105d0, 3);
	      if (iVar1 != 0)
		{
		LAB_00402b11:
		  ;
		  func_0x00401740
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "pinky");
		}
	    }
	  func_0x00401740 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "pinky");
	  uVar3 = 0x41176f;
	  if (pcVar5 == "pinky")
	    goto LAB_00402b3a;
	}
      func_0x00401740
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

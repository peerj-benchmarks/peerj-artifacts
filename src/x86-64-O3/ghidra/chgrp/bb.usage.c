
#include "chgrp.h"

long null_ARRAY_0061b4c_0_4_;
long null_ARRAY_0061b4c_40_8_;
long null_ARRAY_0061b4c_4_4_;
long null_ARRAY_0061b4c_48_8_;
long null_ARRAY_0061b50_0_8_;
long null_ARRAY_0061b50_8_8_;
long null_ARRAY_0061b70_0_8_;
long null_ARRAY_0061b70_16_8_;
long null_ARRAY_0061b70_24_8_;
long null_ARRAY_0061b70_32_8_;
long null_ARRAY_0061b70_40_8_;
long null_ARRAY_0061b70_48_8_;
long null_ARRAY_0061b70_8_8_;
long null_ARRAY_0061b74_0_4_;
long null_ARRAY_0061b74_16_8_;
long null_ARRAY_0061b74_24_4_;
long null_ARRAY_0061b74_32_8_;
long null_ARRAY_0061b74_40_4_;
long null_ARRAY_0061b74_4_4_;
long null_ARRAY_0061b74_44_4_;
long null_ARRAY_0061b74_48_4_;
long null_ARRAY_0061b74_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_004168c0;
long DAT_0041694a;
long DAT_00417860;
long DAT_00417864;
long DAT_00417868;
long DAT_0041786b;
long DAT_0041786d;
long DAT_00417871;
long DAT_00417875;
long DAT_00417e2b;
long DAT_004184ce;
long DAT_004184dd;
long DAT_004184df;
long DAT_004184e0;
long DAT_00418639;
long DAT_0041863a;
long DAT_00418658;
long DAT_004186c9;
long DAT_00418815;
long DAT_0041881f;
long DAT_0041886a;
long DAT_0061b000;
long DAT_0061b010;
long DAT_0061b020;
long DAT_0061b4a8;
long DAT_0061b510;
long DAT_0061b514;
long DAT_0061b518;
long DAT_0061b51c;
long DAT_0061b540;
long DAT_0061b550;
long DAT_0061b560;
long DAT_0061b568;
long DAT_0061b580;
long DAT_0061b588;
long DAT_0061b5e0;
long DAT_0061b5e8;
long DAT_0061b5f0;
long DAT_0061b5f8;
long DAT_0061b778;
long DAT_0061b77c;
long DAT_0061b780;
long DAT_0061b788;
long DAT_0061b790;
long DAT_0061b798;
long DAT_0061b7a8;
long fde_004193e0;
long int7;
long null_ARRAY_00417240;
long null_ARRAY_00418500;
long null_ARRAY_004186e0;
long null_ARRAY_00418b00;
long null_ARRAY_00418d40;
long null_ARRAY_0061b4c0;
long null_ARRAY_0061b500;
long null_ARRAY_0061b5a0;
long null_ARRAY_0061b5d0;
long null_ARRAY_0061b600;
long null_ARRAY_0061b700;
long null_ARRAY_0061b740;
long PTR_DAT_0061b4a0;
long PTR_null_ARRAY_0061b4f8;
long register0x00000020;
void
FUN_00402710 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040273d;
  func_0x00401e40 (DAT_0061b560, "Try \'%s --help\' for more information.\n",
		   DAT_0061b5f8);
  do
    {
      func_0x00402050 ((ulong) uParm1);
    LAB_0040273d:
      ;
      func_0x00401bd0
	("Usage: %s [OPTION]... GROUP FILE...\n  or:  %s [OPTION]... --reference=RFILE FILE...\n",
	 DAT_0061b5f8, DAT_0061b5f8);
      uVar3 = DAT_0061b540;
      func_0x00401e50
	("Change the group of each FILE to GROUP.\nWith --reference, change the group of each FILE to that of RFILE.\n\n",
	 DAT_0061b540);
      func_0x00401e50
	("  -c, --changes          like verbose but report only when a change is made\n  -f, --silent, --quiet  suppress most error messages\n  -v, --verbose          output a diagnostic for every file processed\n",
	 uVar3);
      func_0x00401e50
	("      --dereference      affect the referent of each symbolic link (this is\n                         the default), rather than the symbolic link itself\n  -h, --no-dereference   affect symbolic links instead of any referenced file\n",
	 uVar3);
      func_0x00401e50
	("                         (useful only on systems that can change the\n                         ownership of a symlink)\n",
	 uVar3);
      func_0x00401e50
	("      --no-preserve-root  do not treat \'/\' specially (the default)\n      --preserve-root    fail to operate recursively on \'/\'\n",
	 uVar3);
      func_0x00401e50
	("      --reference=RFILE  use RFILE\'s group rather than specifying a\n                         GROUP value\n",
	 uVar3);
      func_0x00401e50
	("  -R, --recursive        operate on files and directories recursively\n",
	 uVar3);
      func_0x00401e50
	("\nThe following options modify how a hierarchy is traversed when the -R\noption is also specified.  If more than one is specified, only the final\none takes effect.\n\n  -H                     if a command line argument is a symbolic link\n                         to a directory, traverse it\n  -L                     traverse every symbolic link to a directory\n                         encountered\n  -P                     do not traverse any symbolic links (default)\n\n",
	 uVar3);
      func_0x00401e50 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401e50
	("      --version  output version information and exit\n", uVar3);
      func_0x00401bd0
	("\nExamples:\n  %s staff /u      Change the group of /u to \"staff\".\n  %s -hR staff /u  Change the group of /u and subfiles to \"staff\".\n",
	 DAT_0061b5f8, DAT_0061b5f8);
      local_88 = &DAT_004168c0;
      local_80 = "test invocation";
      puVar6 = &DAT_004168c0;
      local_78 = 0x416929;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401f80 ("chgrp", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401bd0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401ff0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401ee0 (lVar2, &DAT_0041694a, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "chgrp";
		  goto LAB_00402949;
		}
	    }
	  func_0x00401bd0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "chgrp");
	LAB_00402972:
	  ;
	  pcVar5 = "chgrp";
	  uVar3 = 0x4168e2;
	}
      else
	{
	  func_0x00401bd0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401ff0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401ee0 (lVar2, &DAT_0041694a, 3);
	      if (iVar1 != 0)
		{
		LAB_00402949:
		  ;
		  func_0x00401bd0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "chgrp");
		}
	    }
	  func_0x00401bd0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "chgrp");
	  uVar3 = 0x418657;
	  if (pcVar5 == "chgrp")
	    goto LAB_00402972;
	}
      func_0x00401bd0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

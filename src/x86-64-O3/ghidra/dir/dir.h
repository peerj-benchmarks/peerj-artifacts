typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined7;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_004023d0(void);
void thunk_FUN_00636078(void);
void FUN_00402bb0(void);
ulong FUN_00402be0(ulong uParm1,undefined1 *puParm2);
void entry(void);
void FUN_00404410(undefined8 *puParm1);
void FUN_00404440(void);
void FUN_004044c0(void);
void FUN_00404540(void);
ulong FUN_00404580(ulong *puParm1,ulong uParm2);
ulong FUN_00404590(long *plParm1,long *plParm2);
void FUN_004045b0(int iParm1);
void FUN_004045d0(void);
ulong FUN_004045e0(byte **ppbParm1,byte **ppbParm2,char cParm3,long *plParm4);
ulong FUN_004048f0(uint uParm1);
undefined8 FUN_00404950(undefined8 uParm1,long lParm2);
undefined8 FUN_004049f0(undefined8 uParm1);
void FUN_00404a50(void);
void FUN_00404c70(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00404c80(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00404c90(undefined8 uParm1,ulong uParm2);
void FUN_00404e20(void);
void FUN_00405310(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00405320(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00405330(void);
undefined8 FUN_00405350(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_004053b0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00405410(char cParm1,ulong uParm2,int iParm3);
undefined8 FUN_004054f0(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00405550(undefined8 *puParm1,undefined8 *puParm2);
void FUN_004055b0(char cParm1);
byte * FUN_00405790(byte **ppbParm1,byte *pbParm2,undefined8 uParm3,int iParm4,byte **ppbParm5,char *pcParm6);
long FUN_00405fa0(long *plParm1);
ulong FUN_00406160(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004061d0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406240(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_004062c0(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00406300(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_004063b0(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00406420(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00406460(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004064d0(undefined8 uParm1);
ulong FUN_00406540(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406590(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004065d0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406620(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406670(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004066c0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406700(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406770(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004067e0(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00406860(undefined8 *puParm1,undefined8 *puParm2);
void FUN_004068d0(undefined8 *puParm1,undefined8 *puParm2);
undefined8 FUN_00406940(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004069f0(char cParm1,ulong uParm2,int iParm3,undefined8 uParm4);
ulong FUN_00406b00(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406b50(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406ba0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406c20(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406ca0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406d20(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406db0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406e20(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406e90(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406f10(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00406fa0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00407030(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004070c0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00407180(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00407240(long lParm1,undefined uParm2);
ulong FUN_004074e0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004075b0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00407670(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00407740(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00407800(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_004078c0(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00407980(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00407a50(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00407b20(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_00407c30(undefined8 *puParm1,undefined8 *puParm2);
void FUN_00407d40(undefined8 *puParm1);
long FUN_00407e60(char *param_1,undefined8 param_2,ulong param_3,long param_4,char param_5,long param_6,byte *param_7);
long FUN_004082e0(long *plParm1,ulong uParm2);
undefined8 FUN_00408cd0(char *pcParm1,int iParm2,undefined4 uParm3,char *pcParm4);
long FUN_00409d30(long *plParm1,byte bParm2,undefined8 uParm3,ulong uParm4);
void FUN_0040a700(long lParm1);
void FUN_0040b4b0(void);
void FUN_0040bd90(long lParm1,long lParm2,char cParm3);
void FUN_0040c680(uint uParm1);
long FUN_0040ca00(undefined8 uParm1,ulong uParm2);
void FUN_0040cb20(void);
long FUN_0040cb30(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_0040cca0(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_0040ccf0(long *plParm1,long lParm2,long lParm3);
long FUN_0040cdc0(undefined8 uParm1,undefined8 uParm2,long *plParm3,long lParm4,long lParm5,code *pcParm6);
char * FUN_0040cfb0(char *pcParm1,uint uParm2);
void FUN_0040d5d0(void);
ulong FUN_0040d660(char *pcParm1);
char * thunk_FUN_0040d6cc(char *pcParm1);
char * FUN_0040d6cc(char *pcParm1);
void FUN_0040d710(long lParm1);
undefined8 FUN_0040d740(void);
void FUN_0040d750(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_0040d7e0(long lParm1,undefined8 uParm2,undefined8 *puParm3);
void FUN_0040d820(long lParm1,undefined *puParm2);
void FUN_0040d9f0(void);
long FUN_0040da10(long lParm1,char *pcParm2,undefined8 *puParm3);
ulong FUN_0040db10(byte *pbParm1,char *pcParm2);
void FUN_0040dfe0(undefined8 *puParm1);
ulong FUN_0040e020(ulong uParm1);
ulong FUN_0040e0c0(ulong uParm1,ulong uParm2);
undefined FUN_0040e0d0(long lParm1,long lParm2);;
undefined8 FUN_0040e0e0(long *plParm1,long **pplParm2,char cParm3);
long FUN_0040e250(long *plParm1,long lParm2,long **pplParm3,char cParm4);
undefined8 FUN_0040e370(long lParm1);
long FUN_0040e380(long *plParm1,long lParm2);
long * FUN_0040e3f0(ulong uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_0040e6e0(long **pplParm1);
ulong FUN_0040e7c0(long *plParm1,ulong uParm2);
long FUN_0040ea30(long *plParm1,long lParm2);
long FUN_0040ed10(long *plParm1,long lParm2);
ulong FUN_0040f000(undefined8 *puParm1,ulong uParm2);
ulong FUN_0040f030(undefined8 *puParm1,undefined8 *puParm2);
void FUN_0040f070(undefined8 *puParm1);
char * FUN_0040f090(ulong uParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5);
undefined8 FUN_0040fe80(char *pcParm1,uint *puParm2,long *plParm3);
int * FUN_00410060(int iParm1);
int * FUN_00410100(int iParm1);
char * FUN_004101a0(long lParm1,long lParm2);
char * FUN_00410240(ulong uParm1,long lParm2);
undefined *FUN_00410290(long lParm1,undefined *puParm2,long lParm3,undefined8 *puParm4,int iParm5,ulong uParm6);
ulong FUN_00410690(byte *pbParm1,long lParm2,ulong uParm3);
void FUN_00410910(undefined8 uParm1,ulong uParm2);
void FUN_00410940(undefined8 *puParm1,ulong uParm2,undefined8 *puParm3,code *pcParm4);
void FUN_00410b70(long lParm1,long lParm2,undefined8 uParm3);
byte * FUN_00410b80(byte *param_1,long param_2,byte *param_3,undefined8 *param_4,byte param_5,undefined8 param_6,undefined8 param_7,uint param_8);
void FUN_00412630(void);
void FUN_00412650(long lParm1);
undefined * FUN_004126f0(char *pcParm1,int iParm2);
void FUN_004127c0(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined8 FUN_00413650(undefined1 *puParm1);
ulong FUN_00413690(undefined1 *puParm1);
void FUN_004136a0(undefined1 *puParm1,undefined4 uParm2);
ulong FUN_004136b0(undefined1 *puParm1,byte bParm2,undefined8 uParm3);
undefined8 FUN_004136f0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,undefined1 *puParm5);
undefined1 * FUN_00413770(uint uParm1,uint uParm2,undefined8 uParm3);
undefined1 * FUN_00413970(uint uParm1,undefined8 uParm2);
undefined1 * FUN_00413b50(undefined8 uParm1);
undefined1 * FUN_00413d30(uint uParm1,uint uParm2,undefined8 uParm3);
undefined1 * FUN_00413f80(uint uParm1,undefined8 uParm2);
undefined1 * FUN_00414120(undefined8 uParm1);
long FUN_004142a0(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,undefined8 uParm6);
long FUN_00414500(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5);
long FUN_00414750(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
long FUN_00414c70(undefined8 param_1,long param_2,undefined8 param_3,undefined8 param_4,long param_5,long param_6,long param_7,long param_8,long param_9,long param_10,long param_11,long param_12,long param_13,long param_14);
void FUN_00415120(long lParm1);
long FUN_00415140(long lParm1,long lParm2);
void FUN_00415180(long lParm1,ulong *puParm2);
void FUN_004151e0(undefined8 uParm1,long lParm2);
void FUN_00415220(undefined8 uParm1);
void FUN_00415260(void);
ulong FUN_00415290(undefined8 param_1,ulong param_2,ulong param_3,ulong param_4,undefined8 param_5,undefined8 param_6,uint param_7);
long FUN_00415370(void);
long FUN_004153a0(void);
ulong FUN_00415450(byte *pbParm1,byte **ppbParm2,uint uParm3,ulong *puParm4,long lParm5);
void FUN_00415ab0(uint uParm1,int iParm2,undefined uParm3,long lParm4,undefined8 uParm5);
ulong FUN_00415b30(byte *pbParm1,byte **ppbParm2,uint uParm3,ulong *puParm4,long lParm5);
undefined8 FUN_00416190(ulong uParm1,ulong uParm2);
void FUN_004161e0(undefined8 uParm1);
int * FUN_00416220(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
int * FUN_00416370(int *piParm1);
char * FUN_00416470(char *pcParm1);
undefined8 FUN_00416580(int iParm1,long lParm2,ulong uParm3,ulong uParm4,byte bParm5,uint uParm6);
ulong FUN_00416bc0(uint *puParm1,uint *puParm2,uint *puParm3,byte bParm4,uint uParm5);
undefined8 FUN_00417ba0(int iParm1,long lParm2,ulong uParm3,ulong uParm4,byte bParm5,uint uParm6);
ulong FUN_00418190(byte *pbParm1,byte *pbParm2,byte *pbParm3,byte bParm4,uint uParm5);
ulong FUN_00419150(undefined8 uParm1,long lParm2,ulong uParm3);
long FUN_004193d0(long lParm1,ulong uParm2);
void FUN_00419850(long lParm1,int *piParm2);
ulong FUN_00419b30(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_0041a290(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,int param_7);
void FUN_0041a990(void);
void FUN_0041a9b0(void);
undefined8 FUN_0041aa70(long lParm1,long lParm2);
void FUN_0041aaf0(long lParm1);
ulong FUN_0041ab10(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_0041ab80(ulong *puParm1,char cParm2,ulong uParm3);
ulong FUN_0041ac70(void);
ulong FUN_0041aca0(long *plParm1,undefined8 *puParm2,long lParm3,code *pcParm4,long lParm5);
void FUN_0041ad40(undefined8 *puParm1,long lParm2);
ulong FUN_0041ae50(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
undefined8 FUN_0041af70(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_0041afe0(long lParm1,long lParm2);
ulong FUN_0041b020(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,long param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_0041b140(long lParm1,long lParm2);
char * FUN_0041b190(char *pcParm1,char *pcParm2,ulong uParm3);
undefined8 * FUN_0041b420(long lParm1);
undefined8 FUN_0041b590(undefined8 *puParm1,char *pcParm2);
undefined8 * FUN_0041b770(long lParm1);
undefined8 FUN_0041b810(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_0041b930(long lParm1,uint *puParm2);
void FUN_0041baf0(long lParm1);
void FUN_0041bb10(void);
ulong FUN_0041bbc0(char *pcParm1);
undefined4 * FUN_0041bc30(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_0041be90(void);
uint * FUN_0041c100(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0041c6b0(int iParm1,ulong uParm2,undefined4 *puParm3,long lParm4,uint uParm5);
void FUN_0041d400(uint param_1);
ulong FUN_0041d5e0(void);
void FUN_0041d800(undefined8 uParm1,uint uParm2);
undefined8 *FUN_0041d970(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
ulong FUN_00424340(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00424410(int *piParm1,long lParm2);
void FUN_00424490(ulong uParm1);
ulong FUN_004244e0(ulong uParm1,char cParm2);
ulong FUN_00424540(undefined8 uParm1);
undefined8 FUN_004245b0(void);
ulong FUN_004245c0(char *pcParm1,ulong uParm2);
char * FUN_004245f0(void);
double FUN_00424940(int *piParm1);
void FUN_00424980(uint *param_1);
ulong FUN_00424a00(long param_1,long param_2,char param_3,char param_4,char param_5,char param_6,char param_7);
undefined8 FUN_00424a80(long param_1,undefined8 param_2,char param_3,char param_4,char param_5,char param_6,char param_7);
ulong FUN_00424ae0(uint uParm1,byte *pbParm2,undefined8 uParm3,undefined8 uParm4,undefined8 uParm5);
void FUN_00424f00(undefined8 uParm1);
ulong FUN_00424f90(ulong param_1,undefined8 param_2,ulong param_3);
undefined8 FUN_004250d0(ulong uParm1);
ulong FUN_00425140(long lParm1);
undefined8 FUN_004251d0(void);
void FUN_004251e0(void);
undefined8 FUN_004251f0(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
void FUN_004252b0(code *pcParm1,long *plParm2,undefined8 uParm3);
long FUN_00425360(undefined8 *puParm1,code *pcParm2,long *plParm3);
long FUN_00425cb0(undefined8 *puParm1);
undefined8 FUN_00426770(uint *puParm1,ulong *puParm2);
undefined8 FUN_00426990(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_00427640(char *pcParm1,char *pcParm2,ulong uParm3);
ulong FUN_00427770(void);
ulong FUN_004277a0(void);
undefined8 * FUN_004277e0(ulong uParm1);
void FUN_00427860(ulong uParm1);
void FUN_004278f0(void);
undefined8 _DT_FINI(void);
undefined FUN_00636000();
undefined FUN_00636008();
undefined FUN_00636010();
undefined FUN_00636018();
undefined FUN_00636020();
undefined FUN_00636028();
undefined FUN_00636030();
undefined FUN_00636038();
undefined FUN_00636040();
undefined FUN_00636048();
undefined FUN_00636050();
undefined FUN_00636058();
undefined FUN_00636060();
undefined FUN_00636068();
undefined FUN_00636070();
undefined FUN_00636078();
undefined FUN_00636080();
undefined FUN_00636088();
undefined FUN_00636090();
undefined FUN_00636098();
undefined FUN_006360a0();
undefined FUN_006360a8();
undefined FUN_006360b0();
undefined FUN_006360b8();
undefined FUN_006360c0();
undefined FUN_006360c8();
undefined FUN_006360d0();
undefined FUN_006360d8();
undefined FUN_006360e0();
undefined FUN_006360e8();
undefined FUN_006360f0();
undefined FUN_006360f8();
undefined FUN_00636100();
undefined FUN_00636108();
undefined FUN_00636110();
undefined FUN_00636118();
undefined FUN_00636120();
undefined FUN_00636128();
undefined FUN_00636130();
undefined FUN_00636138();
undefined FUN_00636140();
undefined FUN_00636148();
undefined FUN_00636150();
undefined FUN_00636158();
undefined FUN_00636160();
undefined FUN_00636168();
undefined FUN_00636170();
undefined FUN_00636178();
undefined FUN_00636180();
undefined FUN_00636188();
undefined FUN_00636190();
undefined FUN_00636198();
undefined FUN_006361a0();
undefined FUN_006361a8();
undefined FUN_006361b0();
undefined FUN_006361b8();
undefined FUN_006361c0();
undefined FUN_006361c8();
undefined FUN_006361d0();
undefined FUN_006361d8();
undefined FUN_006361e0();
undefined FUN_006361e8();
undefined FUN_006361f0();
undefined FUN_006361f8();
undefined FUN_00636200();
undefined FUN_00636208();
undefined FUN_00636210();
undefined FUN_00636218();
undefined FUN_00636220();
undefined FUN_00636228();
undefined FUN_00636230();
undefined FUN_00636238();
undefined FUN_00636240();
undefined FUN_00636248();
undefined FUN_00636250();
undefined FUN_00636258();
undefined FUN_00636260();
undefined FUN_00636268();
undefined FUN_00636270();
undefined FUN_00636278();
undefined FUN_00636280();
undefined FUN_00636288();
undefined FUN_00636290();
undefined FUN_00636298();
undefined FUN_006362a0();
undefined FUN_006362a8();
undefined FUN_006362b0();
undefined FUN_006362b8();
undefined FUN_006362c0();
undefined FUN_006362c8();
undefined FUN_006362d0();
undefined FUN_006362d8();
undefined FUN_006362e0();
undefined FUN_006362e8();
undefined FUN_006362f0();
undefined FUN_006362f8();
undefined FUN_00636300();
undefined FUN_00636308();
undefined FUN_00636310();
undefined FUN_00636318();
undefined FUN_00636320();
undefined FUN_00636328();
undefined FUN_00636330();
undefined FUN_00636338();
undefined FUN_00636340();
undefined FUN_00636348();
undefined FUN_00636350();
undefined FUN_00636358();
undefined FUN_00636360();
undefined FUN_00636368();
undefined FUN_00636370();
undefined FUN_00636378();
undefined FUN_00636380();
undefined FUN_00636388();
undefined FUN_00636390();
undefined FUN_00636398();
undefined FUN_006363a0();
undefined FUN_006363a8();
undefined FUN_006363b0();
undefined FUN_006363b8();
undefined FUN_006363c0();
undefined FUN_006363c8();
undefined FUN_006363d0();
undefined FUN_006363d8();
undefined FUN_006363e0();
undefined FUN_006363e8();


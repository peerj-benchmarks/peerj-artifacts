
#include "cat.h"

long null_ARRAY_0061348_0_8_;
long null_ARRAY_0061348_8_8_;
long null_ARRAY_0061368_0_8_;
long null_ARRAY_0061368_16_8_;
long null_ARRAY_0061368_24_8_;
long null_ARRAY_0061368_32_8_;
long null_ARRAY_0061368_40_8_;
long null_ARRAY_0061368_48_8_;
long null_ARRAY_0061368_8_8_;
long null_ARRAY_006136c_0_4_;
long null_ARRAY_006136c_16_8_;
long null_ARRAY_006136c_24_4_;
long null_ARRAY_006136c_32_8_;
long null_ARRAY_006136c_40_4_;
long null_ARRAY_006136c_4_4_;
long null_ARRAY_006136c_44_4_;
long null_ARRAY_006136c_48_4_;
long null_ARRAY_006136c_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_0041030c;
long DAT_0041030e;
long DAT_00410394;
long DAT_00410ad0;
long DAT_00410ad4;
long DAT_00410ad8;
long DAT_00410adb;
long DAT_00410add;
long DAT_00410ae1;
long DAT_00410ae5;
long DAT_0041106b;
long DAT_004114f5;
long DAT_004115f9;
long DAT_004115ff;
long DAT_00411611;
long DAT_00411612;
long DAT_00411630;
long DAT_00411634;
long DAT_004116be;
long DAT_00613000;
long DAT_00613010;
long DAT_00613020;
long DAT_00613400;
long DAT_00613408;
long DAT_00613430;
long DAT_00613490;
long DAT_00613494;
long DAT_00613498;
long DAT_0061349c;
long DAT_006134c0;
long DAT_006134d0;
long DAT_006134e0;
long DAT_006134e8;
long DAT_00613500;
long DAT_00613508;
long DAT_00613550;
long DAT_00613554;
long DAT_00613558;
long DAT_00613560;
long DAT_00613568;
long DAT_00613570;
long DAT_006136f8;
long DAT_00613700;
long DAT_00613708;
long DAT_00613718;
long _DYNAMIC;
long fde_00412070;
long int7;
long null_ARRAY_00410940;
long null_ARRAY_00411960;
long null_ARRAY_00411bb0;
long null_ARRAY_00613410;
long null_ARRAY_00613480;
long null_ARRAY_00613520;
long null_ARRAY_00613580;
long null_ARRAY_00613680;
long null_ARRAY_006136c0;
long PTR_DAT_00613428;
long PTR_null_ARRAY_00613478;
long register0x00000020;
void
FUN_004027e0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040280d;
  func_0x004017c0 (DAT_006134e0, "Try \'%s --help\' for more information.\n",
		   DAT_00613570);
  do
    {
      func_0x00401960 ((ulong) uParm1);
    LAB_0040280d:
      ;
      func_0x00401610 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00613570);
      uVar3 = DAT_006134c0;
      func_0x004017d0 ("Concatenate FILE(s) to standard output.\n",
		       DAT_006134c0);
      func_0x004017d0
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
      func_0x004017d0
	("\n  -A, --show-all           equivalent to -vET\n  -b, --number-nonblank    number nonempty output lines, overrides -n\n  -e                       equivalent to -vE\n  -E, --show-ends          display $ at end of each line\n  -n, --number             number all output lines\n  -s, --squeeze-blank      suppress repeated empty output lines\n",
	 uVar3);
      func_0x004017d0
	("  -t                       equivalent to -vT\n  -T, --show-tabs          display TAB characters as ^I\n  -u                       (ignored)\n  -v, --show-nonprinting   use ^ and M- notation, except for LFD and TAB\n",
	 uVar3);
      func_0x004017d0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004017d0
	("      --version  output version information and exit\n", uVar3);
      func_0x00401610
	("\nExamples:\n  %s f - g  Output f\'s contents, then standard input, then g\'s contents.\n  %s        Copy standard input to standard output.\n",
	 DAT_00613570, DAT_00613570);
      local_88 = &DAT_0041030c;
      local_80 = "test invocation";
      puVar5 = &DAT_0041030c;
      local_78 = 0x410373;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004018c0 (&DAT_0041030e, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401610 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401920 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401840 (lVar2, &DAT_00410394, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0041030e;
		  goto LAB_004029e1;
		}
	    }
	  func_0x00401610 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041030e);
	LAB_00402a0a:
	  ;
	  puVar5 = &DAT_0041030e;
	  uVar3 = 0x41032c;
	}
      else
	{
	  func_0x00401610 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401920 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401840 (lVar2, &DAT_00410394, 3);
	      if (iVar1 != 0)
		{
		LAB_004029e1:
		  ;
		  func_0x00401610
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0041030e);
		}
	    }
	  func_0x00401610 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041030e);
	  uVar3 = 0x41162f;
	  if (puVar5 == &DAT_0041030e)
	    goto LAB_00402a0a;
	}
      func_0x00401610
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

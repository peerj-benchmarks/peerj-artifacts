
#include "[.h"

long null_ARRAY_00613a0_0_4_;
long null_ARRAY_00613a0_40_8_;
long null_ARRAY_00613a0_4_4_;
long null_ARRAY_00613a0_48_8_;
long null_ARRAY_00613a4_0_8_;
long null_ARRAY_00613a4_8_8_;
long null_ARRAY_00613c4_0_8_;
long null_ARRAY_00613c4_16_8_;
long null_ARRAY_00613c4_24_8_;
long null_ARRAY_00613c4_32_8_;
long null_ARRAY_00613c4_40_8_;
long null_ARRAY_00613c4_48_8_;
long null_ARRAY_00613c4_8_8_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00410100;
long DAT_00410101;
long DAT_00410103;
long DAT_00410106;
long DAT_0041010a;
long DAT_0041010e;
long DAT_00410112;
long DAT_00410116;
long DAT_0041011a;
long DAT_0041011e;
long DAT_00410122;
long DAT_00410126;
long DAT_004101b8;
long DAT_004101f8;
long DAT_004101fb;
long DAT_004101fe;
long DAT_004102ad;
long DAT_004102e4;
long DAT_00411258;
long DAT_0041125c;
long DAT_0041125e;
long DAT_00411262;
long DAT_00411265;
long DAT_00411267;
long DAT_0041126b;
long DAT_0041126f;
long DAT_0041180b;
long DAT_0041180d;
long DAT_00411c95;
long DAT_00411ca6;
long DAT_00411d2e;
long DAT_006135f8;
long DAT_00613608;
long DAT_00613618;
long DAT_006139e8;
long DAT_00613a50;
long DAT_00613a80;
long DAT_00613a90;
long DAT_00613aa0;
long DAT_00613aa8;
long DAT_00613ac0;
long DAT_00613ac8;
long DAT_00613b10;
long DAT_00613b18;
long DAT_00613b1c;
long DAT_00613b20;
long DAT_00613b28;
long DAT_00613b30;
long DAT_00613c78;
long DAT_00613c80;
long DAT_00613c88;
long _DYNAMIC;
long fde_00412710;
long null_ARRAY_00411fc0;
long null_ARRAY_00412210;
long null_ARRAY_00613a00;
long null_ARRAY_00613a40;
long null_ARRAY_00613ae0;
long null_ARRAY_00613b40;
long null_ARRAY_00613c40;
long PTR_DAT_006139e0;
long PTR_null_ARRAY_00613a38;
long register0x00000020;
long stack0x00000008;
void
FUN_00403390 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  long *plVar4;
  long lVar5;
  long local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004033bd;
  func_0x004016f0 (DAT_00613aa0, "Try \'%s --help\' for more information.\n",
		   DAT_00613b30);
  do
    {
      func_0x00401880 ((ulong) uParm1);
    LAB_004033bd:
      ;
      uVar3 = DAT_00613a80;
      func_0x00401700
	("Usage: test EXPRESSION\n  or:  test\n  or:  [ EXPRESSION ]\n  or:  [ ]\n  or:  [ OPTION\n",
	 DAT_00613a80);
      func_0x00401700 ("Exit with the status determined by EXPRESSION.\n\n",
		       uVar3);
      func_0x00401700 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401700
	("      --version  output version information and exit\n", uVar3);
      func_0x00401700
	("\nAn omitted EXPRESSION defaults to false.  Otherwise,\nEXPRESSION is true or false and sets exit status.  It is one of:\n",
	 uVar3);
      func_0x00401700
	("\n  ( EXPRESSION )               EXPRESSION is true\n  ! EXPRESSION                 EXPRESSION is false\n  EXPRESSION1 -a EXPRESSION2   both EXPRESSION1 and EXPRESSION2 are true\n  EXPRESSION1 -o EXPRESSION2   either EXPRESSION1 or EXPRESSION2 is true\n",
	 uVar3);
      func_0x00401700
	("\n  -n STRING            the length of STRING is nonzero\n  STRING               equivalent to -n STRING\n  -z STRING            the length of STRING is zero\n  STRING1 = STRING2    the strings are equal\n  STRING1 != STRING2   the strings are not equal\n",
	 uVar3);
      func_0x00401700
	("\n  INTEGER1 -eq INTEGER2   INTEGER1 is equal to INTEGER2\n  INTEGER1 -ge INTEGER2   INTEGER1 is greater than or equal to INTEGER2\n  INTEGER1 -gt INTEGER2   INTEGER1 is greater than INTEGER2\n  INTEGER1 -le INTEGER2   INTEGER1 is less than or equal to INTEGER2\n  INTEGER1 -lt INTEGER2   INTEGER1 is less than INTEGER2\n  INTEGER1 -ne INTEGER2   INTEGER1 is not equal to INTEGER2\n",
	 uVar3);
      func_0x00401700
	("\n  FILE1 -ef FILE2   FILE1 and FILE2 have the same device and inode numbers\n  FILE1 -nt FILE2   FILE1 is newer (modification date) than FILE2\n  FILE1 -ot FILE2   FILE1 is older than FILE2\n",
	 uVar3);
      func_0x00401700
	("\n  -b FILE     FILE exists and is block special\n  -c FILE     FILE exists and is character special\n  -d FILE     FILE exists and is a directory\n  -e FILE     FILE exists\n",
	 uVar3);
      func_0x00401700
	("  -f FILE     FILE exists and is a regular file\n  -g FILE     FILE exists and is set-group-ID\n  -G FILE     FILE exists and is owned by the effective group ID\n  -h FILE     FILE exists and is a symbolic link (same as -L)\n  -k FILE     FILE exists and has its sticky bit set\n",
	 uVar3);
      func_0x00401700
	("  -L FILE     FILE exists and is a symbolic link (same as -h)\n  -O FILE     FILE exists and is owned by the effective user ID\n  -p FILE     FILE exists and is a named pipe\n  -r FILE     FILE exists and read permission is granted\n  -s FILE     FILE exists and has a size greater than zero\n",
	 uVar3);
      func_0x00401700
	("  -S FILE     FILE exists and is a socket\n  -t FD       file descriptor FD is opened on a terminal\n  -u FILE     FILE exists and its set-user-ID bit is set\n  -w FILE     FILE exists and write permission is granted\n  -x FILE     FILE exists and execute (or search) permission is granted\n",
	 uVar3);
      func_0x00401700
	("\nExcept for -h and -L, all FILE-related tests dereference symbolic links.\nBeware that parentheses need to be escaped (e.g., by backslashes) for shells.\nINTEGER may also be -l STRING, which evaluates to the length of STRING.\n",
	 uVar3);
      func_0x00401700
	("\nNOTE: Binary -a and -o are inherently ambiguous.  Use \'test EXPR1 && test\nEXPR2\' or \'test EXPR1 || test EXPR2\' instead.\n",
	 uVar3);
      func_0x00401700
	("\nNOTE: [ honors the --help and --version options, but test does not.\ntest treats each of those as it treats any other nonempty STRING.\n",
	 uVar3);
      func_0x00401530
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
	 "test and/or [");
      local_88 = 0x410229;
      local_80 = "test invocation";
      lVar5 = 0x410229;
      local_78 = 0x41028c;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      plVar4 = &local_88;
      do
	{
	  iVar1 = func_0x004017d0 (0x410229, lVar5);
	  if (iVar1 == 0)
	    break;
	  plVar4 = plVar4 + 2;
	  lVar5 = *plVar4;
	}
      while (lVar5 != 0);
      lVar5 = plVar4[1];
      if (lVar5 == 0)
	{
	  func_0x00401530 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar5 = func_0x00401830 (5, 0);
	  if (lVar5 != 0)
	    {
	      iVar1 = func_0x00401760 (lVar5, &DAT_004102ad, 3);
	      if (iVar1 != 0)
		{
		  lVar5 = 0x410229;
		  goto LAB_004035f9;
		}
	    }
	  func_0x00401530 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   0x410229);
	LAB_00403622:
	  ;
	  lVar5 = 0x410229;
	  uVar3 = 0x410245;
	}
      else
	{
	  func_0x00401530 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401830 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401760 (lVar2, &DAT_004102ad, 3);
	      if (iVar1 != 0)
		{
		LAB_004035f9:
		  ;
		  func_0x00401530
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     0x410229);
		}
	    }
	  func_0x00401530 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   0x410229);
	  uVar3 = 0x411851;
	  if (lVar5 == 0x410229)
	    goto LAB_00403622;
	}
      func_0x00401530
	("or available locally via: info \'(coreutils) %s%s\'\n", lVar5,
	 uVar3);
    }
  while (true);
}

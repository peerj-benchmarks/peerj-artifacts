
#include "ln.h"

long null_ARRAY_0061b86_0_4_;
long null_ARRAY_0061b86_40_8_;
long null_ARRAY_0061b86_4_4_;
long null_ARRAY_0061b86_48_8_;
long null_ARRAY_0061b8a_0_8_;
long null_ARRAY_0061b8a_8_8_;
long null_ARRAY_0061bb0_0_8_;
long null_ARRAY_0061bb0_16_8_;
long null_ARRAY_0061bb0_24_8_;
long null_ARRAY_0061bb0_32_8_;
long null_ARRAY_0061bb0_40_8_;
long null_ARRAY_0061bb0_48_8_;
long null_ARRAY_0061bb0_8_8_;
long null_ARRAY_0061bb4_0_4_;
long null_ARRAY_0061bb4_16_8_;
long null_ARRAY_0061bb4_24_4_;
long null_ARRAY_0061bb4_32_8_;
long null_ARRAY_0061bb4_40_4_;
long null_ARRAY_0061bb4_4_4_;
long null_ARRAY_0061bb4_44_4_;
long null_ARRAY_0061bb4_48_4_;
long null_ARRAY_0061bb4_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_0041734d;
long DAT_0041734f;
long DAT_00417352;
long DAT_004173d7;
long DAT_00417709;
long DAT_0041770a;
long DAT_0041770b;
long DAT_00417740;
long DAT_00417743;
long DAT_004178e0;
long DAT_004178e6;
long DAT_004178e8;
long DAT_004178ec;
long DAT_004178f0;
long DAT_004178f3;
long DAT_004178f5;
long DAT_004178f9;
long DAT_004178fd;
long DAT_00417f32;
long DAT_004183b5;
long DAT_004184b9;
long DAT_004184bf;
long DAT_004184d1;
long DAT_004184d2;
long DAT_004184f0;
long DAT_00418549;
long DAT_0041854b;
long DAT_0041854e;
long DAT_004185ef;
long DAT_00418605;
long DAT_0061b378;
long DAT_0061b388;
long DAT_0061b398;
long DAT_0061b840;
long DAT_0061b841;
long DAT_0061b850;
long DAT_0061b8b0;
long DAT_0061b8b4;
long DAT_0061b8b8;
long DAT_0061b8bc;
long DAT_0061b900;
long DAT_0061b908;
long DAT_0061b910;
long DAT_0061b920;
long DAT_0061b928;
long DAT_0061b940;
long DAT_0061b948;
long DAT_0061b990;
long DAT_0061b998;
long DAT_0061b999;
long DAT_0061b99a;
long DAT_0061b99b;
long DAT_0061b99c;
long DAT_0061b99d;
long DAT_0061b9a0;
long DAT_0061b9a8;
long DAT_0061b9b0;
long DAT_0061b9b8;
long DAT_0061b9c0;
long DAT_0061b9c8;
long DAT_0061bb38;
long DAT_0061bb78;
long DAT_0061bb7c;
long DAT_0061bb80;
long DAT_0061bbd8;
long DAT_0061bbe0;
long DAT_0061bbf0;
long _DYNAMIC;
long fde_00419240;
long int7;
long null_ARRAY_00417500;
long null_ARRAY_00417700;
long null_ARRAY_004177c0;
long null_ARRAY_00417800;
long null_ARRAY_00417870;
long null_ARRAY_004188a0;
long null_ARRAY_00418a20;
long null_ARRAY_00418b10;
long null_ARRAY_0061b860;
long null_ARRAY_0061b8a0;
long null_ARRAY_0061b960;
long null_ARRAY_0061ba00;
long null_ARRAY_0061bb00;
long null_ARRAY_0061bb40;
long PTR_DAT_0061b848;
long PTR_FUN_0061b8c0;
long PTR_null_ARRAY_0061b898;
long PTR_null_ARRAY_0061b8c8;
long register0x00000020;
void
FUN_00403250 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040327d;
  func_0x00401fc0 (DAT_0061b920, "Try \'%s --help\' for more information.\n",
		   DAT_0061b9c8);
  do
    {
      func_0x00402220 ((ulong) uParm1);
    LAB_0040327d:
      ;
      func_0x00401d50
	("Usage: %s [OPTION]... [-T] TARGET LINK_NAME   (1st form)\n  or:  %s [OPTION]... TARGET                  (2nd form)\n  or:  %s [OPTION]... TARGET... DIRECTORY     (3rd form)\n  or:  %s [OPTION]... -t DIRECTORY TARGET...  (4th form)\n",
	 DAT_0061b9c8, DAT_0061b9c8, DAT_0061b9c8, DAT_0061b9c8);
      uVar3 = DAT_0061b900;
      func_0x00401fd0
	("In the 1st form, create a link to TARGET with the name LINK_NAME.\nIn the 2nd form, create a link to TARGET in the current directory.\nIn the 3rd and 4th forms, create links to each TARGET in DIRECTORY.\nCreate hard links by default, symbolic links with --symbolic.\nBy default, each destination (name of new link) should not already exist.\nWhen creating hard links, each TARGET must exist.  Symbolic links\ncan hold arbitrary text; if later resolved, a relative link is\ninterpreted in relation to its parent directory.\n",
	 DAT_0061b900);
      func_0x00401fd0
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401fd0
	("      --backup[=CONTROL]      make a backup of each existing destination file\n  -b                          like --backup but does not accept an argument\n  -d, -F, --directory         allow the superuser to attempt to hard link\n                                directories (note: will probably fail due to\n                                system restrictions, even for the superuser)\n  -f, --force                 remove existing destination files\n",
	 uVar3);
      func_0x00401fd0
	("  -i, --interactive           prompt whether to remove destinations\n  -L, --logical               dereference TARGETs that are symbolic links\n  -n, --no-dereference        treat LINK_NAME as a normal file if\n                                it is a symbolic link to a directory\n  -P, --physical              make hard links directly to symbolic links\n  -r, --relative              create symbolic links relative to link location\n  -s, --symbolic              make symbolic links instead of hard links\n",
	 uVar3);
      func_0x00401fd0
	("  -S, --suffix=SUFFIX         override the usual backup suffix\n  -t, --target-directory=DIRECTORY  specify the DIRECTORY in which to create\n                                the links\n  -T, --no-target-directory   treat LINK_NAME as a normal file always\n  -v, --verbose               print name of each linked file\n",
	 uVar3);
      func_0x00401fd0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401fd0
	("      --version  output version information and exit\n", uVar3);
      func_0x00401fd0
	("\nThe backup suffix is \'~\', unless set with --suffix or SIMPLE_BACKUP_SUFFIX.\nThe version control method may be selected via the --backup option or through\nthe VERSION_CONTROL environment variable.  Here are the values:\n\n",
	 uVar3);
      func_0x00401fd0
	("  none, off       never make backups (even if --backup is given)\n  numbered, t     make numbered backups\n  existing, nil   numbered if numbered backups exist, simple otherwise\n  simple, never   always make simple backups\n",
	 uVar3);
      func_0x00401d50
	("\nUsing -s ignores -L and -P.  Otherwise, the last option specified controls\nbehavior when a TARGET is a symbolic link, defaulting to %s.\n",
	 &DAT_00417352);
      local_88 = &DAT_0041734d;
      local_80 = "test invocation";
      puVar5 = &DAT_0041734d;
      local_78 = 0x4173b6;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00402150 (&DAT_0041734f, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401d50 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004021b0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00402060 (lVar2, &DAT_004173d7, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0041734f;
		  goto LAB_00403479;
		}
	    }
	  func_0x00401d50 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041734f);
	LAB_004034a2:
	  ;
	  puVar5 = &DAT_0041734f;
	  uVar3 = 0x41736f;
	}
      else
	{
	  func_0x00401d50 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004021b0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00402060 (lVar2, &DAT_004173d7, 3);
	      if (iVar1 != 0)
		{
		LAB_00403479:
		  ;
		  func_0x00401d50
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0041734f);
		}
	    }
	  func_0x00401d50 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0041734f);
	  uVar3 = 0x4184ef;
	  if (puVar5 == &DAT_0041734f)
	    goto LAB_004034a2;
	}
      func_0x00401d50
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

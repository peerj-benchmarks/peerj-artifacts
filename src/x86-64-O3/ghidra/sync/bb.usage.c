
#include "sync.h"

long null_ARRAY_0061244_0_8_;
long null_ARRAY_0061244_8_8_;
long null_ARRAY_0061264_0_8_;
long null_ARRAY_0061264_16_8_;
long null_ARRAY_0061264_24_8_;
long null_ARRAY_0061264_32_8_;
long null_ARRAY_0061264_40_8_;
long null_ARRAY_0061264_48_8_;
long null_ARRAY_0061264_8_8_;
long null_ARRAY_0061268_0_4_;
long null_ARRAY_0061268_16_8_;
long null_ARRAY_0061268_24_4_;
long null_ARRAY_0061268_32_8_;
long null_ARRAY_0061268_40_4_;
long null_ARRAY_0061268_4_4_;
long null_ARRAY_0061268_44_4_;
long null_ARRAY_0061268_48_4_;
long null_ARRAY_0061268_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_0040f600;
long DAT_0040f602;
long DAT_0040f6a7;
long DAT_0040f6ca;
long DAT_0040fb38;
long DAT_0040fb3c;
long DAT_0040fb40;
long DAT_0040fb43;
long DAT_0040fb45;
long DAT_0040fb49;
long DAT_0040fb4d;
long DAT_004100eb;
long DAT_00410575;
long DAT_00410679;
long DAT_0041067f;
long DAT_00410691;
long DAT_00410692;
long DAT_004106b0;
long DAT_004106b4;
long DAT_0041073e;
long DAT_00612000;
long DAT_00612010;
long DAT_00612020;
long DAT_006123e8;
long DAT_00612450;
long DAT_00612454;
long DAT_00612458;
long DAT_0061245c;
long DAT_00612480;
long DAT_00612490;
long DAT_006124a0;
long DAT_006124a8;
long DAT_006124c0;
long DAT_006124c8;
long DAT_00612510;
long DAT_00612518;
long DAT_00612520;
long DAT_00612678;
long DAT_006126b8;
long DAT_006126c0;
long DAT_006126c8;
long DAT_006126d8;
long _DYNAMIC;
long fde_004110c8;
long int7;
long null_ARRAY_0040fa40;
long null_ARRAY_004109e0;
long null_ARRAY_00410c30;
long null_ARRAY_00612440;
long null_ARRAY_006124e0;
long null_ARRAY_00612540;
long null_ARRAY_00612640;
long null_ARRAY_00612680;
long PTR_DAT_006123e0;
long PTR_null_ARRAY_00612438;
long register0x00000020;
void
FUN_00401da0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401dcd;
  func_0x00401680 (DAT_006124a0, "Try \'%s --help\' for more information.\n",
		   DAT_00612520);
  do
    {
      func_0x00401830 ((ulong) uParm1);
    LAB_00401dcd:
      ;
      func_0x00401500 ("Usage: %s [OPTION] [FILE]...\n", DAT_00612520);
      uVar3 = DAT_00612480;
      func_0x00401690
	("Synchronize cached writes to persistent storage\n\nIf one or more files are specified, sync only them,\nor their containing file systems.\n\n",
	 DAT_00612480);
      func_0x00401690
	("  -d, --data             sync only file data, no unneeded metadata\n",
	 uVar3);
      func_0x00401690
	("  -f, --file-system      sync the file systems that contain the files\n",
	 uVar3);
      func_0x00401690 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401690
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040f600;
      local_80 = "test invocation";
      puVar5 = &DAT_0040f600;
      local_78 = 0x40f686;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401770 (&DAT_0040f602, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x00401500 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004017d0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004016e0 (lVar2, &DAT_0040f6a7, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040f602;
		  goto LAB_00401f79;
		}
	    }
	  func_0x00401500 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040f602);
	LAB_00401fa2:
	  ;
	  puVar5 = &DAT_0040f602;
	  uVar3 = 0x40f63f;
	}
      else
	{
	  func_0x00401500 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004017d0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004016e0 (lVar2, &DAT_0040f6a7, 3);
	      if (iVar1 != 0)
		{
		LAB_00401f79:
		  ;
		  func_0x00401500
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040f602);
		}
	    }
	  func_0x00401500 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040f602);
	  uVar3 = 0x4106af;
	  if (puVar5 == &DAT_0040f602)
	    goto LAB_00401fa2;
	}
      func_0x00401500
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

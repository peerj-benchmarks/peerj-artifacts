
#include "chmod.h"

long null_ARRAY_0061946_0_4_;
long null_ARRAY_0061946_40_8_;
long null_ARRAY_0061946_4_4_;
long null_ARRAY_0061946_48_8_;
long null_ARRAY_006194a_0_8_;
long null_ARRAY_006194a_8_8_;
long null_ARRAY_006196c_0_8_;
long null_ARRAY_006196c_16_8_;
long null_ARRAY_006196c_24_8_;
long null_ARRAY_006196c_32_8_;
long null_ARRAY_006196c_40_8_;
long null_ARRAY_006196c_48_8_;
long null_ARRAY_006196c_8_8_;
long null_ARRAY_0061970_0_4_;
long null_ARRAY_0061970_16_8_;
long null_ARRAY_0061970_24_4_;
long null_ARRAY_0061970_32_8_;
long null_ARRAY_0061970_40_4_;
long null_ARRAY_0061970_4_4_;
long null_ARRAY_0061970_44_4_;
long null_ARRAY_0061970_48_4_;
long null_ARRAY_0061970_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_00414e80;
long DAT_00414f0a;
long DAT_00415b38;
long DAT_00415b3e;
long DAT_00415b40;
long DAT_00415b44;
long DAT_00415b48;
long DAT_00415b4b;
long DAT_00415b4d;
long DAT_00415b51;
long DAT_00415b55;
long DAT_004160eb;
long DAT_0041659a;
long DAT_004165ab;
long DAT_004165ac;
long DAT_004166e1;
long DAT_004166e7;
long DAT_004166f9;
long DAT_004166fa;
long DAT_00416718;
long DAT_00416752;
long DAT_00416816;
long DAT_00619000;
long DAT_00619010;
long DAT_00619020;
long DAT_00619440;
long DAT_00619450;
long DAT_006194b0;
long DAT_006194b4;
long DAT_006194b8;
long DAT_006194bc;
long DAT_006194c0;
long DAT_006194d0;
long DAT_006194e0;
long DAT_006194e8;
long DAT_00619500;
long DAT_00619508;
long DAT_00619560;
long DAT_00619568;
long DAT_00619569;
long DAT_0061956a;
long DAT_0061956c;
long DAT_00619570;
long DAT_00619578;
long DAT_00619580;
long DAT_00619588;
long DAT_00619738;
long DAT_00619740;
long DAT_00619744;
long DAT_00619748;
long DAT_00619750;
long DAT_00619760;
long fde_00417318;
long int7;
long null_ARRAY_00415880;
long null_ARRAY_004165c0;
long null_ARRAY_00416760;
long null_ARRAY_00416aa0;
long null_ARRAY_00416ce0;
long null_ARRAY_00619460;
long null_ARRAY_006194a0;
long null_ARRAY_00619520;
long null_ARRAY_00619550;
long null_ARRAY_006195c0;
long null_ARRAY_006196c0;
long null_ARRAY_00619700;
long PTR_DAT_00619448;
long PTR_null_ARRAY_00619498;
long register0x00000020;
void
FUN_004029a0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004029cd;
  func_0x00401a00 (DAT_006194e0, "Try \'%s --help\' for more information.\n",
		   DAT_00619588);
  do
    {
      func_0x00401be0 ((ulong) uParm1);
    LAB_004029cd:
      ;
      func_0x00401810
	("Usage: %s [OPTION]... MODE[,MODE]... FILE...\n  or:  %s [OPTION]... OCTAL-MODE FILE...\n  or:  %s [OPTION]... --reference=RFILE FILE...\n",
	 DAT_00619588, DAT_00619588, DAT_00619588);
      uVar3 = DAT_006194c0;
      func_0x00401a10
	("Change the mode of each FILE to MODE.\nWith --reference, change the mode of each FILE to that of RFILE.\n\n",
	 DAT_006194c0);
      func_0x00401a10
	("  -c, --changes          like verbose but report only when a change is made\n  -f, --silent, --quiet  suppress most error messages\n  -v, --verbose          output a diagnostic for every file processed\n",
	 uVar3);
      func_0x00401a10
	("      --no-preserve-root  do not treat \'/\' specially (the default)\n      --preserve-root    fail to operate recursively on \'/\'\n",
	 uVar3);
      func_0x00401a10
	("      --reference=RFILE  use RFILE\'s mode instead of MODE values\n",
	 uVar3);
      func_0x00401a10
	("  -R, --recursive        change files and directories recursively\n",
	 uVar3);
      func_0x00401a10 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401a10
	("      --version  output version information and exit\n", uVar3);
      func_0x00401a10
	("\nEach MODE is of the form \'[ugoa]*([-+=]([rwxXst]*|[ugo]))+|[-+=][0-7]+\'.\n",
	 uVar3);
      local_88 = &DAT_00414e80;
      local_80 = "test invocation";
      puVar6 = &DAT_00414e80;
      local_78 = 0x414ee9;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401b20 ("chmod", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401810 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401b90 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401a90 (lVar2, &DAT_00414f0a, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "chmod";
		  goto LAB_00402ba9;
		}
	    }
	  func_0x00401810 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "chmod");
	LAB_00402bd2:
	  ;
	  pcVar5 = "chmod";
	  uVar3 = 0x414ea2;
	}
      else
	{
	  func_0x00401810 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401b90 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401a90 (lVar2, &DAT_00414f0a, 3);
	      if (iVar1 != 0)
		{
		LAB_00402ba9:
		  ;
		  func_0x00401810
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "chmod");
		}
	    }
	  func_0x00401810 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "chmod");
	  uVar3 = 0x416717;
	  if (pcVar5 == "chmod")
	    goto LAB_00402bd2;
	}
      func_0x00401810
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

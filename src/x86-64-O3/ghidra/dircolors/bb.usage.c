
#include "dircolors.h"

long null_ARRAY_006187a_0_4_;
long null_ARRAY_006187a_40_8_;
long null_ARRAY_006187a_4_4_;
long null_ARRAY_006187a_48_8_;
long null_ARRAY_006187e_0_8_;
long null_ARRAY_006187e_8_8_;
long null_ARRAY_0061890_16_8_;
long null_ARRAY_0061890_24_8_;
long null_ARRAY_0061890_32_8_;
long null_ARRAY_0061890_48_8_;
long null_ARRAY_0061890_8_8_;
long null_ARRAY_00618a8_0_8_;
long null_ARRAY_00618a8_16_8_;
long null_ARRAY_00618a8_24_8_;
long null_ARRAY_00618a8_32_8_;
long null_ARRAY_00618a8_40_8_;
long null_ARRAY_00618a8_48_8_;
long null_ARRAY_00618a8_8_8_;
long null_ARRAY_00618ac_0_4_;
long null_ARRAY_00618ac_16_8_;
long null_ARRAY_00618ac_24_4_;
long null_ARRAY_00618ac_32_8_;
long null_ARRAY_00618ac_40_4_;
long null_ARRAY_00618ac_4_4_;
long null_ARRAY_00618ac_44_4_;
long null_ARRAY_00618ac_48_4_;
long null_ARRAY_00618ac_8_4_;
long local_2f_1_1_;
long local_5_4_4_;
long local_5b_1_1_;
long local_6_4_4_;
long local_8_0_4_;
long local_8_4_4_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000009;
long DAT_00000010;
long DAT_00000fe0;
long DAT_00413a17;
long DAT_00413a33;
long DAT_00413adf;
long DAT_00413b26;
long DAT_00413b41;
long DAT_00413b42;
long DAT_00413c0e;
long DAT_0041550d;
long DAT_00415570;
long DAT_00415574;
long DAT_00415578;
long DAT_0041557b;
long DAT_0041557d;
long DAT_00415581;
long DAT_00415585;
long DAT_00415b2b;
long DAT_00415fb5;
long DAT_00416731;
long DAT_00416732;
long DAT_00416750;
long DAT_004167d6;
long DAT_00618340;
long DAT_00618350;
long DAT_00618360;
long DAT_00618788;
long DAT_006187f0;
long DAT_006187f4;
long DAT_006187f8;
long DAT_006187fc;
long DAT_00618840;
long DAT_00618848;
long DAT_00618850;
long DAT_00618860;
long DAT_00618868;
long DAT_00618880;
long DAT_00618888;
long DAT_00618958;
long DAT_00618960;
long DAT_00618968;
long DAT_00618ab8;
long DAT_00618af8;
long DAT_00618b00;
long DAT_00618b08;
long DAT_00618b18;
long fde_004171e0;
long int7;
long null_ARRAY_00414140;
long null_ARRAY_00414240;
long null_ARRAY_00414380;
long null_ARRAY_00416a60;
long null_ARRAY_00416cb0;
long null_ARRAY_006187a0;
long null_ARRAY_006187e0;
long null_ARRAY_006188a0;
long null_ARRAY_00618900;
long null_ARRAY_00618980;
long null_ARRAY_00618a80;
long null_ARRAY_00618ac0;
long PTR_DAT_00618780;
long PTR_FUN_00618800;
long PTR_null_ARRAY_006187d8;
long register0x00000020;
void
FUN_004029c0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004029ed;
  func_0x00401a30 (DAT_00618860, "Try \'%s --help\' for more information.\n",
		   DAT_00618968);
  do
    {
      func_0x00401c20 ((ulong) uParm1);
    LAB_004029ed:
      ;
      func_0x00401840 ("Usage: %s [OPTION]... [FILE]\n", DAT_00618968);
      uVar3 = DAT_00618840;
      func_0x00401a40
	("Output commands to set the LS_COLORS environment variable.\n\nDetermine format of output:\n  -b, --sh, --bourne-shell    output Bourne shell code to set LS_COLORS\n  -c, --csh, --c-shell        output C shell code to set LS_COLORS\n  -p, --print-database        output defaults\n",
	 DAT_00618840);
      func_0x00401a40 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401a40
	("      --version  output version information and exit\n", uVar3);
      func_0x00401a40
	("\nIf FILE is specified, read it to determine which colors to use for which\nfile types and extensions.  Otherwise, a precompiled database is used.\nFor details on the format of these files, run \'dircolors --print-database\'.\n",
	 uVar3);
      local_88 = &DAT_00413a33;
      local_80 = "test invocation";
      puVar6 = &DAT_00413a33;
      local_78 = 0x413abe;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401b60 ("dircolors", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401840 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401bc0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401ab0 (lVar2, &DAT_00413adf, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "dircolors";
		  goto LAB_00402b91;
		}
	    }
	  func_0x00401840 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "dircolors");
	LAB_00402bba:
	  ;
	  pcVar5 = "dircolors";
	  uVar3 = 0x413a77;
	}
      else
	{
	  func_0x00401840 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401bc0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401ab0 (lVar2, &DAT_00413adf, 3);
	      if (iVar1 != 0)
		{
		LAB_00402b91:
		  ;
		  func_0x00401840
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "dircolors");
		}
	    }
	  func_0x00401840 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "dircolors");
	  uVar3 = 0x41674f;
	  if (pcVar5 == "dircolors")
	    goto LAB_00402bba;
	}
      func_0x00401840
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

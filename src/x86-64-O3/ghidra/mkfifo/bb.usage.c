
#include "mkfifo.h"

long null_ARRAY_0061284_0_8_;
long null_ARRAY_0061284_8_8_;
long null_ARRAY_00612a4_0_8_;
long null_ARRAY_00612a4_16_8_;
long null_ARRAY_00612a4_24_8_;
long null_ARRAY_00612a4_32_8_;
long null_ARRAY_00612a4_40_8_;
long null_ARRAY_00612a4_48_8_;
long null_ARRAY_00612a4_8_8_;
long null_ARRAY_00612a8_0_4_;
long null_ARRAY_00612a8_16_8_;
long null_ARRAY_00612a8_24_4_;
long null_ARRAY_00612a8_32_8_;
long null_ARRAY_00612a8_40_4_;
long null_ARRAY_00612a8_4_4_;
long null_ARRAY_00612a8_44_4_;
long null_ARRAY_00612a8_48_4_;
long null_ARRAY_00612a8_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_0040f9c0;
long DAT_0040fa4b;
long DAT_0040fa5f;
long DAT_00410080;
long DAT_00410084;
long DAT_00410088;
long DAT_0041008b;
long DAT_0041008d;
long DAT_00410091;
long DAT_00410095;
long DAT_0041062b;
long DAT_00410ab5;
long DAT_00410bb9;
long DAT_00410bbf;
long DAT_00410bd1;
long DAT_00410bd2;
long DAT_00410bf0;
long DAT_00410bf4;
long DAT_00410c7e;
long DAT_00612410;
long DAT_00612420;
long DAT_00612430;
long DAT_006127e8;
long DAT_00612850;
long DAT_00612854;
long DAT_00612858;
long DAT_0061285c;
long DAT_00612880;
long DAT_00612890;
long DAT_006128a0;
long DAT_006128a8;
long DAT_006128c0;
long DAT_006128c8;
long DAT_00612910;
long DAT_00612918;
long DAT_00612920;
long DAT_00612ab8;
long DAT_00612ac0;
long DAT_00612ac8;
long DAT_00612ad8;
long _DYNAMIC;
long fde_00411620;
long int7;
long null_ARRAY_0040fe80;
long null_ARRAY_00410f20;
long null_ARRAY_00411170;
long null_ARRAY_00612840;
long null_ARRAY_006128e0;
long null_ARRAY_00612940;
long null_ARRAY_00612a40;
long null_ARRAY_00612a80;
long PTR_DAT_006127e0;
long PTR_null_ARRAY_00612838;
long register0x00000020;
long stack0x00000008;
void
FUN_00401cf0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401d1d;
  func_0x00401670 (DAT_006128a0, "Try \'%s --help\' for more information.\n",
		   DAT_00612920);
  do
    {
      func_0x00401830 ((ulong) uParm1);
    LAB_00401d1d:
      ;
      func_0x00401500 ("Usage: %s [OPTION]... NAME...\n", DAT_00612920);
      uVar3 = DAT_00612880;
      func_0x00401680 ("Create named pipes (FIFOs) with the given NAMEs.\n",
		       DAT_00612880);
      func_0x00401680
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401680
	("  -m, --mode=MODE    set file permission bits to MODE, not a=rw - umask\n",
	 uVar3);
      func_0x00401680
	("  -Z                   set the SELinux security context to default type\n      --context[=CTX]  like -Z, or if CTX is specified then set the SELinux\n                         or SMACK security context to CTX\n",
	 uVar3);
      func_0x00401680 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401680
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040f9c0;
      local_80 = "test invocation";
      puVar6 = &DAT_0040f9c0;
      local_78 = 0x40fa2a;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401780 ("mkfifo", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401500 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004017e0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004016f0 (lVar2, &DAT_0040fa4b, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "mkfifo";
		  goto LAB_00401ed9;
		}
	    }
	  func_0x00401500 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "mkfifo");
	LAB_00401f02:
	  ;
	  pcVar5 = "mkfifo";
	  uVar3 = 0x40f9e3;
	}
      else
	{
	  func_0x00401500 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004017e0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004016f0 (lVar2, &DAT_0040fa4b, 3);
	      if (iVar1 != 0)
		{
		LAB_00401ed9:
		  ;
		  func_0x00401500
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "mkfifo");
		}
	    }
	  func_0x00401500 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "mkfifo");
	  uVar3 = 0x410bef;
	  if (pcVar5 == "mkfifo")
	    goto LAB_00401f02;
	}
      func_0x00401500
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

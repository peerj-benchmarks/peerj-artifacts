typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined7;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00402180(void);
void thunk_FUN_00641048(void);
void thunk_FUN_00641150(void);
void FUN_004028f0(void);
ulong FUN_00402910(ulong uParm1,undefined8 *puParm2);
void entry(void);
void FUN_00403bd0(undefined8 *puParm1);
void FUN_00403c00(void);
void FUN_00403c80(void);
void FUN_00403d00(void);
void FUN_00403d40(long *plParm1,undefined8 uParm2);
void FUN_00403e80(uint uParm1);
void FUN_00404100(void);
long FUN_00404110(undefined8 uParm1,long *plParm2,long lParm3,long lParm4);
void FUN_00404280(long *plParm1,long lParm2,long lParm3);
long FUN_00404350(undefined8 uParm1,undefined8 uParm2,long *plParm3,long lParm4,long lParm5,code *pcParm6);
void FUN_00404540(undefined8 uParm1);
void FUN_00404570(undefined8 uParm1);
long FUN_004045b0(long *plParm1,int *piParm2);
long FUN_00404640(long *plParm1);
void FUN_00404660(long *plParm1);
void FUN_00404680(void);
ulong FUN_00404710(ulong *puParm1,ulong uParm2);
ulong FUN_00404720(ulong *puParm1,ulong *puParm2);
ulong FUN_00404730(ulong uParm1,ulong uParm2);
void FUN_00404740(long lParm1);
long * FUN_00404760(void);
void FUN_004047d0(undefined8 *puParm1);
undefined8 FUN_00404800(undefined8 *puParm1,long lParm2,long lParm3);
ulong FUN_00404940(undefined8 *puParm1,long lParm2,long lParm3);
void thunk_FUN_004028f0(void);
uint FUN_00404a80(void);
uint FUN_00404aa0(void);
ulong thunk_FUN_004079c0(byte *pbParm1,ulong uParm2);
ulong FUN_00404ad0(byte *pbParm1,ulong uParm2);
ulong FUN_00404d10(undefined8 uParm1,long lParm2,ulong uParm3);
undefined8 FUN_00404df0(byte *pbParm1,ulong uParm2);
void FUN_00405090(void);
ulong FUN_004050a0(long **pplParm1,char *pcParm2);
void FUN_00405300(long **pplParm1,long *plParm2,uint uParm3);
ulong FUN_00405660(code *pcParm1,long lParm2,char *pcParm3,uint uParm4,byte bParm5);
char * FUN_004058c0(long param_1,char *param_2,undefined8 *param_3,undefined param_4,undefined8 param_5,undefined8 param_6,uint param_7);
void FUN_00407490(void);
long FUN_004074b0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_004076a0(ulong uParm1,ulong uParm2);
undefined FUN_004076b0(long lParm1,long lParm2);;
undefined8 FUN_004076c0(long *plParm1,long **pplParm2,char cParm3);
long FUN_00407830(long *plParm1,long lParm2,long **pplParm3,char cParm4);
long FUN_00407950(long *plParm1,long lParm2);
ulong FUN_004079c0(byte *pbParm1,ulong uParm2);
long * FUN_00407a00(ulong uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_00407cf0(long **pplParm1);
ulong FUN_00407dd0(long *plParm1,ulong uParm2);
undefined8 FUN_00408040(long *plParm1,long lParm2,long *plParm3);
long FUN_00408330(long *plParm1,long lParm2);
long FUN_00408610(long *plParm1,long lParm2);
char * FUN_00408900(ulong uParm1,long lParm2,ulong uParm3,ulong uParm4,ulong uParm5);
undefined8 FUN_004096f0(char *pcParm1,uint *puParm2,long *plParm3);
ulong FUN_004098d0(ulong *puParm1,ulong uParm2);
ulong FUN_004098e0(ulong *puParm1,ulong *puParm2);
long * FUN_004098f0(long lParm1);
long FUN_00409950(undefined8 *puParm1,long lParm2);
char * FUN_004099f0(long lParm1,long lParm2);
ulong FUN_00409a90(byte *pbParm1,byte *pbParm2);
void FUN_0040a310(long lParm1);
undefined * FUN_0040a3b0(char *pcParm1,int iParm2);
void FUN_0040a480(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040b310(uint uParm1,uint uParm2,undefined8 uParm3);
undefined1 * FUN_0040b510(uint uParm1,undefined8 uParm2);
undefined1 * FUN_0040b6f0(undefined8 uParm1);
undefined1 * FUN_0040b8d0(uint uParm1,uint uParm2,undefined8 uParm3);
undefined1 * FUN_0040bb20(uint uParm1,undefined8 uParm2);
undefined1 * FUN_0040bcc0(undefined8 uParm1);
long FUN_0040be40(long lParm1,undefined8 uParm2);
long FUN_0040be80(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,undefined8 uParm6);
long FUN_0040c0e0(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5);
long FUN_0040c330(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
long FUN_0040c850(undefined8 param_1,long param_2,undefined8 param_3,undefined8 param_4,long param_5,long param_6,long param_7,long param_8,long param_9,long param_10,long param_11,long param_12,long param_13,long param_14);
void FUN_0040cd00(long lParm1);
long FUN_0040cd20(long lParm1,long lParm2);
void FUN_0040cd60(long lParm1,ulong *puParm2);
void FUN_0040cdc0(long lParm1);
void FUN_0040cdf0(ulong uParm1,ulong uParm2);
void FUN_0040ce20(undefined8 uParm1);
void FUN_0040ce60(void);
void FUN_0040ce90(undefined8 uParm1,uint uParm2);
ulong FUN_0040cee0(long lParm1,long lParm2);
ulong FUN_0040cf10(byte *pbParm1,byte **ppbParm2,uint uParm3,long *plParm4,long lParm5);
ulong FUN_0040d4f0(byte *pbParm1,byte **ppbParm2,uint uParm3,ulong *puParm4,long lParm5);
void FUN_0040db50(uint uParm1,int iParm2,undefined uParm3,long lParm4,undefined8 uParm5);
ulong FUN_0040dbd0(byte *pbParm1,byte **ppbParm2,uint uParm3,ulong *puParm4,long lParm5);
undefined8 FUN_0040e230(ulong uParm1,ulong uParm2);
void FUN_0040e280(undefined8 uParm1);
void FUN_0040e2c0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_0040e410(undefined8 uParm1);
ulong FUN_0040e4a0(long lParm1);
int * FUN_0040e530(int *piParm1);
char * FUN_0040e630(char *pcParm1);
undefined8 FUN_0040e740(int iParm1,long lParm2,ulong uParm3,ulong uParm4,byte bParm5,uint uParm6);
ulong FUN_0040ed80(uint *puParm1,uint *puParm2,uint *puParm3,byte bParm4,uint uParm5);
undefined8 FUN_0040fd60(int iParm1,long lParm2,ulong uParm3,ulong uParm4,byte bParm5,uint uParm6);
ulong FUN_00410350(byte *pbParm1,byte *pbParm2,byte *pbParm3,byte bParm4,uint uParm5);
ulong FUN_00411310(undefined8 uParm1,long lParm2,ulong uParm3);
undefined8 FUN_00411590(void);
void FUN_004115a0(void);
ulong FUN_004115b0(ulong *puParm1,ulong *puParm2);
ulong FUN_004115d0(long lParm1,ulong uParm2);
ulong FUN_004115e0(ulong *puParm1,ulong uParm2);
ulong FUN_004115f0(ulong *puParm1,ulong *puParm2);
ulong FUN_00411600(long *plParm1,long *plParm2);
ulong FUN_00411630(long lParm1,long lParm2,char cParm3);
undefined8 FUN_004117a0(long lParm1);
ulong FUN_00411900(long lParm1);
ulong FUN_004119a0(long lParm1,long lParm2,ulong uParm3,long lParm4);
long FUN_00411c10(long *plParm1,int iParm2);
long * FUN_00412aa0(long *plParm1,uint uParm2,long lParm3);
undefined8 FUN_004131c0(long *plParm1);
undefined8 * FUN_00413370(long *plParm1);
undefined8 FUN_00413e70(undefined8 uParm1,long lParm2,uint uParm3);
ulong FUN_00413ea0(long *plParm1,ulong *puParm2,int iParm3,long lParm4);
void FUN_00414000(long lParm1,int *piParm2);
ulong FUN_004142e0(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00414a40(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,int param_7);
void FUN_00415140(void);
undefined8 FUN_00415160(long lParm1,long lParm2);
void FUN_004151e0(long lParm1);
ulong FUN_00415200(uint *puParm1,byte *pbParm2,long lParm3);
ulong * FUN_00415270(ulong *puParm1,char cParm2,ulong uParm3);
void FUN_00415360(long lParm1);
undefined8 * FUN_00415400(void);
void FUN_00415af0(undefined8 *puParm1);
void FUN_00415b30(long lParm1,long lParm2);
undefined8 FUN_00415b70(char *pcParm1,long lParm2,ulong uParm3);
undefined8 FUN_00415ca0(ulong *puParm1,long lParm2);
void FUN_00415ed0(long *plParm1);
undefined8 FUN_004161b0(long *plParm1);
long FUN_00416ca0(long *plParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 *FUN_00416e90(undefined8 *puParm1,int *piParm2,long *plParm3,long *plParm4,undefined *puParm5);
undefined8 FUN_00416f60(long lParm1,long *plParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_004170b0(long *plParm1,long lParm2);
undefined8 FUN_004172a0(long *plParm1,long *plParm2,long *plParm3,long *plParm4,long *plParm5);
ulong FUN_00417460(long lParm1,long lParm2,ulong uParm3);
undefined8 FUN_00417530(byte **ppbParm1,byte *pbParm2,uint uParm3);
long FUN_00418060(long lParm1,long lParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_004183a0(long param_1,ulong *param_2,long *param_3,long *param_4,long *param_5,char *param_6,ulong param_7);
void FUN_00418bc0(long **pplParm1,long *plParm2,long *plParm3,long lParm4);
ulong FUN_00419040(long lParm1,ulong *puParm2,long lParm3,long lParm4,long lParm5);
undefined8 * FUN_00419340(undefined8 *puParm1,int *piParm2,undefined8 *puParm3);
undefined8 FUN_004193f0(long *plParm1,long *plParm2,ulong uParm3);
undefined8 FUN_00419a90(long *plParm1,ulong *puParm2,ulong uParm3);
long * FUN_00419bd0(long **pplParm1,long lParm2);
undefined8 FUN_00419d40(long *plParm1,int iParm2);
long FUN_00419f70(long *plParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_0041a240(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
undefined8 FUN_0041a530(long *plParm1,long *plParm2,long lParm3,byte bParm4);
undefined8 FUN_0041a7e0(long *plParm1,long lParm2,long lParm3);
void FUN_0041aa20(long *plParm1);
ulong FUN_0041ac30(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_0041b6e0(long param_1,long *param_2,long *param_3,undefined8 param_4,long param_5,undefined8 param_6,long param_7);
undefined8 *FUN_0041b9d0(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
undefined8 FUN_0041bde0(long *plParm1,ulong *puParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_0041c110(long *plParm1,long *plParm2,long lParm3,ulong uParm4);
ulong FUN_0041c250(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong * FUN_0041c620(undefined4 *puParm1,long *plParm2,long lParm3);
ulong FUN_0041ca20(long lParm1,long lParm2,long lParm3,undefined8 uParm4,uint uParm5);
ulong * FUN_0041ced0(undefined4 *puParm1,long *plParm2,long lParm3,ulong uParm4);
ulong FUN_0041d4c0(long *plParm1,long lParm2);
ulong FUN_0041e260(long lParm1,ulong *puParm2,ulong uParm3,long lParm4);
ulong FUN_0041ed90(long lParm1,long *plParm2);
ulong FUN_0041f310(long param_1,long *param_2,long param_3,long param_4,long param_5,long param_6,uint param_7);
undefined8 FUN_0041fe90(long lParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
ulong FUN_004200b0(long *plParm1,long *plParm2,long *plParm3);
long FUN_00420cf0(int *piParm1,long lParm2,long lParm3);
undefined8 FUN_00420ec0(long **pplParm1,long lParm2,long lParm3,long *plParm4,char cParm5);
ulong FUN_00421b00(long **param_1,long param_2,ulong param_3,long param_4,long param_5,ulong param_6,long *param_7,undefined8 *param_8,uint param_9);
long ** FUN_004237e0(long *plParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long ** FUN_00425c00(undefined8 uParm1,long *plParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long ** FUN_00425f60(long lParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
ulong FUN_004261e0(long **pplParm1,long lParm2,long *plParm3,long *plParm4);
ulong FUN_00427e90(long *plParm1,undefined8 uParm2,ulong uParm3);
ulong FUN_00428060(long lParm1,undefined8 uParm2,undefined8 uParm3,undefined8 *puParm4,ulong uParm5);
ulong FUN_00428100(long lParm1,long lParm2);
long FUN_00428150(long lParm1,long lParm2,long lParm3,ulong uParm4);
char * FUN_004285c0(char *pcParm1,char *pcParm2);
undefined8 * FUN_00428a60(long lParm1);
undefined8 FUN_00428bd0(undefined8 *puParm1,char *pcParm2);
undefined8 * FUN_00428db0(long lParm1);
undefined8 FUN_00428e50(long lParm1,undefined8 uParm2,undefined8 uParm3);
long FUN_00428f70(long lParm1,uint *puParm2);
void FUN_00429130(long lParm1);
void FUN_00429150(void);
ulong FUN_00429200(char *pcParm1);
ulong FUN_00429270(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00429340(undefined8 uParm1);
void FUN_004293b0(long lParm1);
undefined8 FUN_004293c0(long *plParm1,long *plParm2);
void FUN_00429450(undefined8 param_1,byte param_2,ulong param_3);
undefined8 FUN_004294a0(void);
ulong FUN_004294b0(ulong uParm1);
void FUN_00429550(undefined4 *puParm1,undefined4 uParm2);
ulong FUN_00429580(long lParm1);
ulong FUN_00429590(long lParm1,uint uParm2);
ulong FUN_004295d0(long lParm1);
char * FUN_00429610(void);
void FUN_00429960(void);
ulong FUN_004299b0(uint uParm1);
ulong FUN_004299f0(ulong param_1,undefined8 param_2,ulong param_3);
undefined8 FUN_00429b30(ulong uParm1);
undefined8 FUN_00429ba0(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
void FUN_00429c60(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00429c70(code *pcParm1,long *plParm2,undefined8 uParm3);
long FUN_00429d20(undefined8 *puParm1,code *pcParm2,long *plParm3);
long FUN_0042a670(undefined8 *puParm1);
ulong FUN_0042b130(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
ulong FUN_0042b250(char *pcParm1,char *pcParm2,ulong uParm3);
undefined4 * FUN_0042b380(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_0042b5e0(void);
uint * FUN_0042b850(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0042be00(int iParm1,ulong uParm2,undefined4 *puParm3,long lParm4,uint uParm5);
void FUN_0042cb50(uint param_1);
ulong FUN_0042cd30(void);
void FUN_0042cf50(undefined8 uParm1,uint uParm2);
undefined8 *FUN_0042d0c0(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
ulong FUN_00433a90(ulong uParm1,char cParm2);
undefined8 * FUN_00433af0(ulong uParm1);
void FUN_00433b70(ulong uParm1);
double FUN_00433c00(int *piParm1);
void FUN_00433c40(uint *param_1);
void FUN_00433cc0(undefined8 uParm1);
undefined8 FUN_00433cd0(uint *puParm1,ulong *puParm2);
undefined8 FUN_00433ef0(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_00434ba0(void);
ulong FUN_00434bd0(void);
void FUN_00434c10(void);
undefined8 _DT_FINI(void);
undefined FUN_00641000();
undefined FUN_00641008();
undefined FUN_00641010();
undefined FUN_00641018();
undefined FUN_00641020();
undefined FUN_00641028();
undefined FUN_00641030();
undefined FUN_00641038();
undefined FUN_00641040();
undefined FUN_00641048();
undefined FUN_00641050();
undefined FUN_00641058();
undefined FUN_00641060();
undefined FUN_00641068();
undefined FUN_00641070();
undefined FUN_00641078();
undefined FUN_00641080();
undefined FUN_00641088();
undefined FUN_00641090();
undefined FUN_00641098();
undefined FUN_006410a0();
undefined FUN_006410a8();
undefined FUN_006410b0();
undefined FUN_006410b8();
undefined FUN_006410c0();
undefined FUN_006410c8();
undefined FUN_006410d0();
undefined FUN_006410d8();
undefined FUN_006410e0();
undefined FUN_006410e8();
undefined FUN_006410f0();
undefined FUN_006410f8();
undefined FUN_00641100();
undefined FUN_00641108();
undefined FUN_00641110();
undefined FUN_00641118();
undefined FUN_00641120();
undefined FUN_00641128();
undefined FUN_00641130();
undefined FUN_00641138();
undefined FUN_00641140();
undefined FUN_00641148();
undefined FUN_00641150();
undefined FUN_00641158();
undefined FUN_00641160();
undefined FUN_00641168();
undefined FUN_00641170();
undefined FUN_00641178();
undefined FUN_00641180();
undefined FUN_00641188();
undefined FUN_00641190();
undefined FUN_00641198();
undefined FUN_006411a0();
undefined FUN_006411a8();
undefined FUN_006411b0();
undefined FUN_006411b8();
undefined FUN_006411c0();
undefined FUN_006411c8();
undefined FUN_006411d0();
undefined FUN_006411d8();
undefined FUN_006411e0();
undefined FUN_006411e8();
undefined FUN_006411f0();
undefined FUN_006411f8();
undefined FUN_00641200();
undefined FUN_00641208();
undefined FUN_00641210();
undefined FUN_00641218();
undefined FUN_00641220();
undefined FUN_00641228();
undefined FUN_00641230();
undefined FUN_00641238();
undefined FUN_00641240();
undefined FUN_00641248();
undefined FUN_00641250();
undefined FUN_00641258();
undefined FUN_00641260();
undefined FUN_00641268();
undefined FUN_00641270();
undefined FUN_00641278();
undefined FUN_00641280();
undefined FUN_00641288();
undefined FUN_00641290();
undefined FUN_00641298();
undefined FUN_006412a0();
undefined FUN_006412a8();
undefined FUN_006412b0();
undefined FUN_006412b8();
undefined FUN_006412c0();
undefined FUN_006412c8();
undefined FUN_006412d0();
undefined FUN_006412d8();
undefined FUN_006412e0();
undefined FUN_006412e8();
undefined FUN_006412f0();
undefined FUN_006412f8();
undefined FUN_00641300();
undefined FUN_00641308();
undefined FUN_00641310();
undefined FUN_00641318();
undefined FUN_00641320();
undefined FUN_00641328();
undefined FUN_00641330();
undefined FUN_00641338();
undefined FUN_00641340();
undefined FUN_00641348();
undefined FUN_00641350();
undefined FUN_00641358();
undefined FUN_00641360();
undefined FUN_00641368();
undefined FUN_00641370();
undefined FUN_00641378();
undefined FUN_00641380();
undefined FUN_00641388();
undefined FUN_00641390();
undefined FUN_00641398();
undefined FUN_006413a0();
undefined FUN_006413a8();


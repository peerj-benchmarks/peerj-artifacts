
#include "kill.h"

long null_ARRAY_0061342_0_4_;
long null_ARRAY_0061342_40_8_;
long null_ARRAY_0061342_4_4_;
long null_ARRAY_0061342_48_8_;
long null_ARRAY_0061346_0_8_;
long null_ARRAY_0061346_8_8_;
long null_ARRAY_0061380_0_8_;
long null_ARRAY_0061380_16_8_;
long null_ARRAY_0061380_24_8_;
long null_ARRAY_0061380_32_8_;
long null_ARRAY_0061380_40_8_;
long null_ARRAY_0061380_48_8_;
long null_ARRAY_0061380_8_8_;
long null_ARRAY_0061384_0_4_;
long null_ARRAY_0061384_16_8_;
long null_ARRAY_0061384_24_4_;
long null_ARRAY_0061384_32_8_;
long null_ARRAY_0061384_40_4_;
long null_ARRAY_0061384_4_4_;
long null_ARRAY_0061384_44_4_;
long null_ARRAY_0061384_48_4_;
long null_ARRAY_0061384_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_0040fbc0;
long DAT_0040fbc2;
long DAT_0040fc49;
long DAT_0040fc4d;
long DAT_0040fc95;
long DAT_004103b8;
long DAT_004103bc;
long DAT_004103c0;
long DAT_004103c3;
long DAT_004103c5;
long DAT_004103c9;
long DAT_004103cd;
long DAT_0041096b;
long DAT_00410df5;
long DAT_00410ef9;
long DAT_00410eff;
long DAT_00410f11;
long DAT_00410f12;
long DAT_00410f30;
long DAT_00410f40;
long DAT_00410f44;
long DAT_00613000;
long DAT_00613010;
long DAT_00613020;
long DAT_00613408;
long DAT_00613470;
long DAT_00613474;
long DAT_00613478;
long DAT_0061347c;
long DAT_00613480;
long DAT_00613484;
long DAT_00613640;
long DAT_00613650;
long DAT_00613660;
long DAT_00613668;
long DAT_00613680;
long DAT_00613688;
long DAT_006136d0;
long DAT_006136d8;
long DAT_006136e0;
long DAT_00613878;
long DAT_00613880;
long DAT_00613888;
long DAT_00613898;
long fde_00411960;
long int7;
long null_ARRAY_00410200;
long null_ARRAY_00411260;
long null_ARRAY_004114b0;
long null_ARRAY_00613420;
long null_ARRAY_00613460;
long null_ARRAY_006136a0;
long null_ARRAY_00613700;
long null_ARRAY_00613800;
long null_ARRAY_00613840;
long PTR_DAT_00613400;
long PTR_null_ARRAY_00613458;
long register0x00000020;
void
FUN_004020c0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004020ed;
  func_0x00401790 (DAT_00613660, "Try \'%s --help\' for more information.\n",
		   DAT_006136e0);
  do
    {
      func_0x00401930 ((ulong) uParm1);
    LAB_004020ed:
      ;
      func_0x004015d0
	("Usage: %s [-s SIGNAL | -SIGNAL] PID...\n  or:  %s -l [SIGNAL]...\n  or:  %s -t [SIGNAL]...\n",
	 DAT_006136e0, DAT_006136e0, DAT_006136e0);
      uVar3 = DAT_00613640;
      func_0x004017b0 ("Send signals to processes, or list signals.\n",
		       DAT_00613640);
      func_0x004017b0
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x004017b0
	("  -s, --signal=SIGNAL, -SIGNAL\n                   specify the name or number of the signal to be sent\n  -l, --list       list signal names, or convert signal names to/from numbers\n  -t, --table      print a table of signal information\n",
	 uVar3);
      func_0x004017b0 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x004017b0
	("      --version  output version information and exit\n", uVar3);
      func_0x004017b0
	("\nSIGNAL may be a signal name like \'HUP\', or a signal number like \'1\',\nor the exit status of a process terminated by a signal.\nPID is an integer; if negative it identifies a process group.\n",
	 uVar3);
      func_0x004015d0
	("\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n",
	 &DAT_0040fbc2);
      local_88 = &DAT_0040fbc0;
      local_80 = "test invocation";
      puVar5 = &DAT_0040fbc0;
      local_78 = 0x40fc28;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401880 (&DAT_0040fbc2, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x004015d0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004018f0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401800 (lVar2, &DAT_0040fc49, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_0040fbc2;
		  goto LAB_004022c1;
		}
	    }
	  func_0x004015d0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040fbc2);
	LAB_004022ea:
	  ;
	  puVar5 = &DAT_0040fbc2;
	  uVar3 = 0x40fbe1;
	}
      else
	{
	  func_0x004015d0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004018f0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401800 (lVar2, &DAT_0040fc49, 3);
	      if (iVar1 != 0)
		{
		LAB_004022c1:
		  ;
		  func_0x004015d0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_0040fbc2);
		}
	    }
	  func_0x004015d0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_0040fbc2);
	  uVar3 = 0x410f2f;
	  if (puVar5 == &DAT_0040fbc2)
	    goto LAB_004022ea;
	}
      func_0x004015d0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

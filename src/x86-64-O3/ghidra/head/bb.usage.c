
#include "head.h"

long null_ARRAY_0061540_0_4_;
long null_ARRAY_0061540_40_8_;
long null_ARRAY_0061540_4_4_;
long null_ARRAY_0061540_48_8_;
long null_ARRAY_0061544_0_8_;
long null_ARRAY_0061544_8_8_;
long null_ARRAY_0061564_0_8_;
long null_ARRAY_0061564_16_8_;
long null_ARRAY_0061564_24_8_;
long null_ARRAY_0061564_32_8_;
long null_ARRAY_0061564_40_8_;
long null_ARRAY_0061564_48_8_;
long null_ARRAY_0061564_8_8_;
long null_ARRAY_0061568_0_4_;
long null_ARRAY_0061568_16_8_;
long null_ARRAY_0061568_24_4_;
long null_ARRAY_0061568_32_8_;
long null_ARRAY_0061568_40_4_;
long null_ARRAY_0061568_4_4_;
long null_ARRAY_0061568_44_4_;
long null_ARRAY_0061568_48_4_;
long null_ARRAY_0061568_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_00411c04;
long DAT_00411c06;
long DAT_00411c8d;
long DAT_00412598;
long DAT_0041259c;
long DAT_004125a0;
long DAT_004125a3;
long DAT_004125a5;
long DAT_004125a9;
long DAT_004125ad;
long DAT_00412b2b;
long DAT_004131a8;
long DAT_004132b1;
long DAT_004132b7;
long DAT_004132c9;
long DAT_004132ca;
long DAT_004132e8;
long DAT_004132ec;
long DAT_00413376;
long DAT_00615000;
long DAT_00615010;
long DAT_00615020;
long DAT_006153e0;
long DAT_006153f0;
long DAT_00615450;
long DAT_00615454;
long DAT_00615458;
long DAT_0061545c;
long DAT_00615480;
long DAT_00615490;
long DAT_006154a0;
long DAT_006154a8;
long DAT_006154c0;
long DAT_006154c8;
long DAT_00615510;
long DAT_00615511;
long DAT_00615512;
long DAT_00615513;
long DAT_00615518;
long DAT_00615520;
long DAT_00615528;
long DAT_006156b8;
long DAT_006156c0;
long DAT_006156c8;
long DAT_006156d8;
long fde_00413d48;
long int7;
long null_ARRAY_004123d0;
long null_ARRAY_00412400;
long null_ARRAY_00413600;
long null_ARRAY_00413850;
long null_ARRAY_00615400;
long null_ARRAY_00615440;
long null_ARRAY_006154e0;
long null_ARRAY_00615540;
long null_ARRAY_00615640;
long null_ARRAY_00615680;
long PTR_DAT_006153e8;
long PTR_null_ARRAY_00615438;
long register0x00000020;
void
FUN_004035d0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  undefined *puVar5;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_004035fd;
  func_0x00401780 (DAT_006154a0, "Try \'%s --help\' for more information.\n",
		   DAT_00615528);
  do
    {
      func_0x00401930 ((ulong) uParm1);
    LAB_004035fd:
      ;
      func_0x004015d0 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00615528);
      func_0x004015d0
	("Print the first %d lines of each FILE to standard output.\nWith more than one FILE, precede each with a header giving the file name.\n",
	 10);
      uVar3 = DAT_00615480;
      func_0x00401790
	("\nWith no FILE, or when FILE is -, read standard input.\n",
	 DAT_00615480);
      func_0x00401790
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x004015d0
	("  -c, --bytes=[-]NUM       print the first NUM bytes of each file;\n                             with the leading \'-\', print all but the last\n                             NUM bytes of each file\n  -n, --lines=[-]NUM       print the first NUM lines instead of the first %d;\n                             with the leading \'-\', print all but the last\n                             NUM lines of each file\n",
	 10);
      func_0x00401790
	("  -q, --quiet, --silent    never print headers giving file names\n  -v, --verbose            always print headers giving file names\n",
	 uVar3);
      func_0x00401790
	("  -z, --zero-terminated    line delimiter is NUL, not newline\n",
	 uVar3);
      func_0x00401790 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401790
	("      --version  output version information and exit\n", uVar3);
      func_0x00401790
	("\nNUM may have a multiplier suffix:\nb 512, kB 1000, K 1024, MB 1000*1000, M 1024*1024,\nGB 1000*1000*1000, G 1024*1024*1024, and so on for T, P, E, Z, Y.\n",
	 uVar3);
      local_88 = &DAT_00411c04;
      local_80 = "test invocation";
      puVar5 = &DAT_00411c04;
      local_78 = 0x411c6c;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401880 (&DAT_00411c06, puVar5);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar5 = *ppuVar4;
	}
      while (puVar5 != (undefined *) 0x0);
      puVar5 = ppuVar4[1];
      if (puVar5 == (undefined *) 0x0)
	{
	  func_0x004015d0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004018e0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004017f0 (lVar2, &DAT_00411c8d, 3);
	      if (iVar1 != 0)
		{
		  puVar5 = &DAT_00411c06;
		  goto LAB_004037e9;
		}
	    }
	  func_0x004015d0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00411c06);
	LAB_00403812:
	  ;
	  puVar5 = &DAT_00411c06;
	  uVar3 = 0x411c25;
	}
      else
	{
	  func_0x004015d0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004018e0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004017f0 (lVar2, &DAT_00411c8d, 3);
	      if (iVar1 != 0)
		{
		LAB_004037e9:
		  ;
		  func_0x004015d0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     &DAT_00411c06);
		}
	    }
	  func_0x004015d0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   &DAT_00411c06);
	  uVar3 = 0x4132e7;
	  if (puVar5 == &DAT_00411c06)
	    goto LAB_00403812;
	}
      func_0x004015d0
	("or available locally via: info \'(coreutils) %s%s\'\n", puVar5,
	 uVar3);
    }
  while (true);
}

typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00401cb0(void);
void thunk_FUN_0062d038(void);
void thunk_FUN_0062d118(void);
ulong FUN_00402300(ulong uParm1,undefined8 *puParm2);
void entry(void);
void FUN_004029c0(void);
void FUN_00402a40(void);
void FUN_00402ac0(void);
undefined8 FUN_00402b00(ulong uParm1,undefined8 uParm2,char *pcParm3);
void FUN_004032b0(uint uParm1);
void FUN_004034d0(void);
long FUN_00403560(long lParm1,char *pcParm2,undefined8 *puParm3);
void FUN_00403660(long lParm1);
undefined * FUN_00403700(char *pcParm1,int iParm2);
void FUN_004037d0(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_00404660(uint uParm1,undefined8 uParm2);
undefined1 * FUN_00404840(undefined8 uParm1);
undefined1 * FUN_00404a20(uint uParm1,uint uParm2,undefined8 uParm3);
undefined1 * FUN_00404c70(undefined8 uParm1);
long FUN_00404df0(uint uParm1,undefined8 uParm2,ulong uParm3);
void FUN_00404e60(void);
ulong FUN_00404e80(uint uParm1);
long FUN_00404ec0(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,undefined8 uParm6);
long FUN_00405120(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5);
long FUN_00405370(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
long FUN_00405890(undefined8 param_1,long param_2,undefined8 param_3,undefined8 param_4,long param_5,long param_6,long param_7,long param_8,long param_9,long param_10,long param_11,long param_12,long param_13,long param_14);
void FUN_00405d40(long lParm1);
long FUN_00405d60(long lParm1,long lParm2);
void FUN_00405da0(void);
undefined8 FUN_00405dd0(ulong uParm1,ulong uParm2);
void FUN_00405e20(undefined8 uParm1);
void FUN_00405e60(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,uint param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
void FUN_00405fb0(void);
void FUN_00405fc0(long lParm1,int *piParm2);
ulong FUN_004062a0(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00406a00(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,int param_7);
void FUN_00407100(void);
void FUN_00407120(long lParm1);
ulong FUN_00407140(uint *puParm1,byte *pbParm2,long lParm3);
void FUN_004071b0(undefined8 uParm1);
void FUN_004071c0(long lParm1,long lParm2);
undefined8 FUN_00407200(char *pcParm1,long lParm2,ulong uParm3);
undefined8 FUN_00407330(ulong *puParm1,long lParm2);
void FUN_00407560(long *plParm1);
undefined8 FUN_00407840(long *plParm1);
long FUN_00408330(long *plParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 *FUN_00408520(undefined8 *puParm1,int *piParm2,long *plParm3,long *plParm4,undefined *puParm5);
undefined8 FUN_004085f0(long lParm1,long *plParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_00408740(long *plParm1,long lParm2);
undefined8 FUN_00408930(long *plParm1,long *plParm2,long *plParm3,long *plParm4,long *plParm5);
ulong FUN_00408af0(long lParm1,long lParm2,ulong uParm3);
undefined8 FUN_00408bc0(byte **ppbParm1,byte *pbParm2,uint uParm3);
long FUN_004096f0(long lParm1,long lParm2,long *plParm3,undefined8 uParm4);
undefined8 FUN_00409a30(long param_1,ulong *param_2,long *param_3,long *param_4,long *param_5,char *param_6,ulong param_7);
void FUN_0040a250(long **pplParm1,long *plParm2,long *plParm3,long lParm4);
ulong FUN_0040a6d0(long lParm1,ulong *puParm2,long lParm3,long lParm4,long lParm5);
undefined8 * FUN_0040a9d0(undefined8 *puParm1,int *piParm2,undefined8 *puParm3);
undefined8 FUN_0040aa80(long *plParm1,long *plParm2,ulong uParm3);
undefined8 FUN_0040b120(long *plParm1,ulong *puParm2,ulong uParm3);
long * FUN_0040b260(long **pplParm1,long lParm2);
undefined8 FUN_0040b3d0(long *plParm1,int iParm2);
long FUN_0040b600(long *plParm1,long lParm2,undefined8 uParm3);
undefined8 FUN_0040b8d0(long *plParm1,long lParm2,long lParm3,long lParm4,uint uParm5);
undefined8 FUN_0040bbc0(long *plParm1,long *plParm2,long lParm3,byte bParm4);
undefined8 FUN_0040be70(long *plParm1,long lParm2,long lParm3);
void FUN_0040c0b0(long *plParm1);
ulong FUN_0040c2c0(long lParm1,uint uParm2,long lParm3,long lParm4,long lParm5);
undefined8 FUN_0040cd70(long param_1,long *param_2,long *param_3,undefined8 param_4,long param_5,undefined8 param_6,long param_7);
undefined8 *FUN_0040d060(long lParm1,undefined8 uParm2,undefined8 uParm3,char *pcParm4,byte bParm5,int *piParm6);
undefined8 FUN_0040d470(long *plParm1,ulong *puParm2,long lParm3,long lParm4,uint uParm5);
ulong FUN_0040d7a0(long *plParm1,long *plParm2,long lParm3,ulong uParm4);
ulong FUN_0040d8e0(long *plParm1,long lParm2,long lParm3,long lParm4);
ulong * FUN_0040dcb0(undefined4 *puParm1,long *plParm2,long lParm3);
ulong FUN_0040e0b0(long lParm1,long lParm2,long lParm3,undefined8 uParm4,uint uParm5);
ulong * FUN_0040e560(undefined4 *puParm1,long *plParm2,long lParm3,ulong uParm4);
ulong FUN_0040eb50(long *plParm1,long lParm2);
ulong FUN_0040f8f0(long lParm1,ulong *puParm2,ulong uParm3,long lParm4);
ulong FUN_00410420(long lParm1,long *plParm2);
ulong FUN_004109a0(long param_1,long *param_2,long param_3,long param_4,long param_5,long param_6,uint param_7);
undefined8 FUN_00411520(long lParm1,long *plParm2,undefined8 *puParm3,undefined8 uParm4,long lParm5);
ulong FUN_00411740(long *plParm1,long *plParm2,long *plParm3);
long FUN_00412380(int *piParm1,long lParm2,long lParm3);
undefined8 FUN_00412550(long **pplParm1,long lParm2,long lParm3,long *plParm4,char cParm5);
ulong FUN_00413190(long **param_1,long param_2,ulong param_3,long param_4,long param_5,ulong param_6,long *param_7,undefined8 *param_8,uint param_9);
long ** FUN_00414e70(long *plParm1,long *plParm2,undefined8 *puParm3,ulong uParm4,long lParm5,int *piParm6);
long ** FUN_00417290(undefined8 uParm1,long *plParm2,long lParm3,undefined8 uParm4,long lParm5,int *piParm6);
long ** FUN_004175f0(long lParm1,long *plParm2,long lParm3,ulong uParm4,long lParm5,int *piParm6);
ulong FUN_00417870(long **pplParm1,long lParm2,long *plParm3,long *plParm4);
char * FUN_00419520(undefined8 uParm1,undefined8 uParm2,long lParm3);
long FUN_00419580(long *plParm1,undefined8 uParm2,long lParm3,long lParm4,long lParm5,ulong *puParm6);
undefined8 FUN_00419a60(long lParm1);
ulong FUN_00419b30(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_00419c00(undefined8 uParm1);
char * thunk_FUN_00419c7c(char *pcParm1);
char * FUN_00419c7c(char *pcParm1);
void FUN_00419cc0(long lParm1);
undefined8 FUN_00419cf0(void);
ulong FUN_00419d00(ulong uParm1);
char * FUN_00419da0(void);
ulong FUN_0041a0f0(undefined8 uParm1);
void FUN_0041a140(undefined8 uParm1);
void FUN_0041a150(undefined8 uParm1,uint *puParm2);
ulong FUN_0041a170(long lParm1,int iParm2,undefined8 uParm3,code *pcParm4);
void FUN_0041a2d0(undefined8 uParm1,undefined8 uParm2,undefined4 uParm3,int iParm4);
void FUN_0041a350(undefined8 uParm1);
void FUN_0041a360(undefined8 uParm1);
ulong FUN_0041a3f0(ulong param_1,undefined8 param_2,ulong param_3);
ulong FUN_0041a530(long lParm1);
undefined8 FUN_0041a5c0(void);
undefined8 FUN_0041a5d0(long lParm1,long lParm2);
ulong FUN_0041a650(long lParm1,ulong uParm2);
ulong FUN_0041a740(long lParm1,long lParm2);
undefined4 * FUN_0041a790(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_0041a9f0(void);
uint * FUN_0041ac60(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_0041b210(int iParm1,ulong uParm2,undefined4 *puParm3,long lParm4,uint uParm5);
void FUN_0041bf60(uint param_1);
ulong FUN_0041c140(void);
void FUN_0041c360(undefined8 uParm1,uint uParm2);
undefined8 *FUN_0041c4d0(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
undefined FUN_00422ea0(char *pcParm1);;
double FUN_00422ee0(int *piParm1);
void FUN_00422f20(uint *param_1);
long * FUN_00422fa0(void);
ulong FUN_00422fe0(undefined8 *puParm1,ulong uParm2);
ulong FUN_00423190(undefined8 *puParm1);
void FUN_004231d0(long lParm1);
long * FUN_00423220(ulong uParm1,ulong uParm2);
void FUN_004234b0(long *plParm1,ulong uParm2,ulong uParm3);
undefined8 FUN_00423620(long *plParm1);
void FUN_00423670(long *plParm1,long *plParm2);
void FUN_004238e0(ulong *puParm1);
void FUN_00423b20(undefined8 uParm1,undefined8 uParm2);
void FUN_00423b40(void);
undefined8 FUN_00423c00(uint *puParm1,ulong *puParm2);
undefined8 FUN_00423e20(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_00424ad0(void);
ulong FUN_00424b00(void);
long FUN_00424b40(undefined8 uParm1,undefined8 uParm2);
void FUN_00424be0(void);
undefined8 _DT_FINI(void);
undefined FUN_0062d000();
undefined FUN_0062d008();
undefined FUN_0062d010();
undefined FUN_0062d018();
undefined FUN_0062d020();
undefined FUN_0062d028();
undefined FUN_0062d030();
undefined FUN_0062d038();
undefined FUN_0062d040();
undefined FUN_0062d048();
undefined FUN_0062d050();
undefined FUN_0062d058();
undefined FUN_0062d060();
undefined FUN_0062d068();
undefined FUN_0062d070();
undefined FUN_0062d078();
undefined FUN_0062d080();
undefined FUN_0062d088();
undefined FUN_0062d090();
undefined FUN_0062d098();
undefined FUN_0062d0a0();
undefined FUN_0062d0a8();
undefined FUN_0062d0b0();
undefined FUN_0062d0b8();
undefined FUN_0062d0c0();
undefined FUN_0062d0c8();
undefined FUN_0062d0d0();
undefined FUN_0062d0d8();
undefined FUN_0062d0e0();
undefined FUN_0062d0e8();
undefined FUN_0062d0f0();
undefined FUN_0062d0f8();
undefined FUN_0062d100();
undefined FUN_0062d108();
undefined FUN_0062d110();
undefined FUN_0062d118();
undefined FUN_0062d120();
undefined FUN_0062d128();
undefined FUN_0062d130();
undefined FUN_0062d138();
undefined FUN_0062d140();
undefined FUN_0062d148();
undefined FUN_0062d150();
undefined FUN_0062d158();
undefined FUN_0062d160();
undefined FUN_0062d168();
undefined FUN_0062d170();
undefined FUN_0062d178();
undefined FUN_0062d180();
undefined FUN_0062d188();
undefined FUN_0062d190();
undefined FUN_0062d198();
undefined FUN_0062d1a0();
undefined FUN_0062d1a8();
undefined FUN_0062d1b0();
undefined FUN_0062d1b8();
undefined FUN_0062d1c0();
undefined FUN_0062d1c8();
undefined FUN_0062d1d0();
undefined FUN_0062d1d8();
undefined FUN_0062d1e0();
undefined FUN_0062d1e8();
undefined FUN_0062d1f0();
undefined FUN_0062d1f8();
undefined FUN_0062d200();
undefined FUN_0062d208();
undefined FUN_0062d210();
undefined FUN_0062d218();
undefined FUN_0062d220();
undefined FUN_0062d228();
undefined FUN_0062d230();
undefined FUN_0062d238();
undefined FUN_0062d240();
undefined FUN_0062d248();
undefined FUN_0062d250();
undefined FUN_0062d258();
undefined FUN_0062d260();
undefined FUN_0062d268();
undefined FUN_0062d270();
undefined FUN_0062d278();
undefined FUN_0062d280();
undefined FUN_0062d288();
undefined FUN_0062d290();
undefined FUN_0062d298();
undefined FUN_0062d2a0();
undefined FUN_0062d2a8();
undefined FUN_0062d2b0();
undefined FUN_0062d2b8();
undefined FUN_0062d2c0();
undefined FUN_0062d2c8();
undefined FUN_0062d2d0();
undefined FUN_0062d2d8();
undefined FUN_0062d2e0();
undefined FUN_0062d2e8();
undefined FUN_0062d2f0();
undefined FUN_0062d2f8();
undefined FUN_0062d300();
undefined FUN_0062d308();
undefined FUN_0062d310();
undefined FUN_0062d318();


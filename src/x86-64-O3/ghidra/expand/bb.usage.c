
#include "expand.h"

long null_ARRAY_0061342_0_4_;
long null_ARRAY_0061342_40_8_;
long null_ARRAY_0061342_4_4_;
long null_ARRAY_0061342_48_8_;
long null_ARRAY_0061346_0_8_;
long null_ARRAY_0061346_8_8_;
long null_ARRAY_0061368_0_8_;
long null_ARRAY_0061368_16_8_;
long null_ARRAY_0061368_24_8_;
long null_ARRAY_0061368_32_8_;
long null_ARRAY_0061368_40_8_;
long null_ARRAY_0061368_48_8_;
long null_ARRAY_0061368_8_8_;
long null_ARRAY_006136c_0_4_;
long null_ARRAY_006136c_16_8_;
long null_ARRAY_006136c_24_4_;
long null_ARRAY_006136c_32_8_;
long null_ARRAY_006136c_40_4_;
long null_ARRAY_006136c_4_4_;
long null_ARRAY_006136c_44_4_;
long null_ARRAY_006136c_48_4_;
long null_ARRAY_006136c_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_00410280;
long DAT_0041030b;
long DAT_00410aa0;
long DAT_00410aa4;
long DAT_00410aa8;
long DAT_00410aab;
long DAT_00410aad;
long DAT_00410ab1;
long DAT_00410ab5;
long DAT_0041106b;
long DAT_004114f5;
long DAT_004115f9;
long DAT_004115ff;
long DAT_00411611;
long DAT_00411612;
long DAT_00411630;
long DAT_00411634;
long DAT_004116be;
long DAT_00613000;
long DAT_00613010;
long DAT_00613020;
long DAT_00613418;
long DAT_00613470;
long DAT_00613474;
long DAT_00613478;
long DAT_0061347c;
long DAT_00613480;
long DAT_00613488;
long DAT_00613490;
long DAT_006134a0;
long DAT_006134a8;
long DAT_006134c0;
long DAT_006134c8;
long DAT_00613510;
long DAT_00613514;
long DAT_00613518;
long DAT_00613520;
long DAT_00613528;
long DAT_00613530;
long DAT_00613538;
long DAT_00613540;
long DAT_00613548;
long DAT_00613550;
long DAT_00613558;
long DAT_00613560;
long DAT_00613568;
long DAT_00613570;
long DAT_006136f8;
long DAT_00613700;
long DAT_00613708;
long DAT_00613710;
long DAT_00613720;
long fde_00412098;
long int7;
long null_ARRAY_00410640;
long null_ARRAY_004106e0;
long null_ARRAY_00411960;
long null_ARRAY_00411bb0;
long null_ARRAY_00613400;
long null_ARRAY_00613420;
long null_ARRAY_00613460;
long null_ARRAY_006134e0;
long null_ARRAY_00613580;
long null_ARRAY_00613680;
long null_ARRAY_006136c0;
long PTR_DAT_00613410;
long PTR_null_ARRAY_00613458;
long register0x00000020;
void
FUN_00401f00 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401f2d;
  func_0x004017f0 (DAT_006134a0, "Try \'%s --help\' for more information.\n",
		   DAT_00613570);
  do
    {
      func_0x004019b0 ((ulong) uParm1);
    LAB_00401f2d:
      ;
      func_0x00401650 ("Usage: %s [OPTION]... [FILE]...\n", DAT_00613570);
      uVar3 = DAT_00613480;
      func_0x00401800
	("Convert tabs in each FILE to spaces, writing to standard output.\n",
	 DAT_00613480);
      func_0x00401800
	("\nWith no FILE, or when FILE is -, read standard input.\n", uVar3);
      func_0x00401800
	("\nMandatory arguments to long options are mandatory for short options too.\n",
	 uVar3);
      func_0x00401800
	("  -i, --initial    do not convert tabs after non blanks\n  -t, --tabs=N     have tabs N characters apart, not 8\n",
	 uVar3);
      FUN_00402b00 ();
      func_0x00401800 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401800
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_00410280;
      local_80 = "test invocation";
      puVar6 = &DAT_00410280;
      local_78 = 0x4102ea;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401900 ("expand", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401650 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401960 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401860 (lVar2, &DAT_0041030b, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "expand";
		  goto LAB_004020f1;
		}
	    }
	  func_0x00401650 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "expand");
	LAB_0040211a:
	  ;
	  pcVar5 = "expand";
	  uVar3 = 0x4102a3;
	}
      else
	{
	  func_0x00401650 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401960 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x00401860 (lVar2, &DAT_0041030b, 3);
	      if (iVar1 != 0)
		{
		LAB_004020f1:
		  ;
		  func_0x00401650
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "expand");
		}
	    }
	  func_0x00401650 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "expand");
	  uVar3 = 0x41162f;
	  if (pcVar5 == "expand")
	    goto LAB_0040211a;
	}
      func_0x00401650
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

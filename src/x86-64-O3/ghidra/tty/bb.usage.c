
#include "tty.h"

long null_ARRAY_00611ca_0_4_;
long null_ARRAY_00611ca_40_8_;
long null_ARRAY_00611ca_4_4_;
long null_ARRAY_00611ca_48_8_;
long null_ARRAY_00611ce_0_8_;
long null_ARRAY_00611ce_8_8_;
long null_ARRAY_00611ec_0_8_;
long null_ARRAY_00611ec_16_8_;
long null_ARRAY_00611ec_24_8_;
long null_ARRAY_00611ec_32_8_;
long null_ARRAY_00611ec_40_8_;
long null_ARRAY_00611ec_48_8_;
long null_ARRAY_00611ec_8_8_;
long null_ARRAY_00611f0_0_4_;
long null_ARRAY_00611f0_16_8_;
long null_ARRAY_00611f0_24_4_;
long null_ARRAY_00611f0_32_8_;
long null_ARRAY_00611f0_40_4_;
long null_ARRAY_00611f0_4_4_;
long null_ARRAY_00611f0_44_4_;
long null_ARRAY_00611f0_48_4_;
long null_ARRAY_00611f0_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long lVar13;
long DAT_00000008;
long DAT_00000010;
long DAT_0040f240;
long DAT_0040f2db;
long DAT_0040f638;
long DAT_0040f63c;
long DAT_0040f640;
long DAT_0040f643;
long DAT_0040f645;
long DAT_0040f649;
long DAT_0040f64d;
long DAT_0040fbeb;
long DAT_00410075;
long DAT_00410179;
long DAT_0041017f;
long DAT_00410191;
long DAT_00410192;
long DAT_004101b0;
long DAT_004101b4;
long DAT_0041023e;
long DAT_006118b8;
long DAT_006118c8;
long DAT_006118d8;
long DAT_00611c88;
long DAT_00611cf0;
long DAT_00611cf4;
long DAT_00611cf8;
long DAT_00611cfc;
long DAT_00611d00;
long DAT_00611d10;
long DAT_00611d20;
long DAT_00611d28;
long DAT_00611d40;
long DAT_00611d48;
long DAT_00611d90;
long DAT_00611d98;
long DAT_00611da0;
long DAT_00611da8;
long DAT_00611f38;
long DAT_00611f40;
long DAT_00611f48;
long DAT_00611f58;
long fde_00410bc0;
long int7;
long null_ARRAY_0040f540;
long null_ARRAY_004104e0;
long null_ARRAY_00410730;
long null_ARRAY_00611ca0;
long null_ARRAY_00611ce0;
long null_ARRAY_00611d60;
long null_ARRAY_00611dc0;
long null_ARRAY_00611ec0;
long null_ARRAY_00611f00;
long PTR_DAT_00611c80;
long PTR_null_ARRAY_00611cd8;
long register0x00000020;
void
FUN_00401ba0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  long lVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00401bcd;
  func_0x00401650 (DAT_00611d20, "Try \'%s --help\' for more information.\n",
		   DAT_00611da8);
  do
    {
      func_0x004017e0 ((ulong) uParm1);
    LAB_00401bcd:
      ;
      func_0x004014c0 ("Usage: %s [OPTION]...\n", DAT_00611da8);
      uVar3 = DAT_00611d00;
      func_0x00401660
	("Print the file name of the terminal connected to standard input.\n\n  -s, --silent, --quiet   print nothing, only return an exit status\n",
	 DAT_00611d00);
      func_0x00401660 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401660
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_0040f240;
      local_80 = "test invocation";
      puVar6 = &DAT_0040f240;
      local_78 = 0x40f2ba;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401730 (0x40f2e5, puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      lVar5 = (long) ppuVar4[1];
      if (lVar5 == 0)
	{
	  func_0x004014c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar5 = func_0x004017a0 (5, 0);
	  if (lVar5 != 0)
	    {
	      iVar1 = func_0x004016b0 (lVar5, &DAT_0040f2db, 3);
	      if (iVar1 != 0)
		{
		  lVar5 = 0x40f2e5;
		  goto LAB_00401d61;
		}
	    }
	  func_0x004014c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   0x40f2e5);
	LAB_00401d8a:
	  ;
	  lVar5 = 0x40f2e5;
	  uVar3 = 0x40f273;
	}
      else
	{
	  func_0x004014c0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004017a0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004016b0 (lVar2, &DAT_0040f2db, 3);
	      if (iVar1 != 0)
		{
		LAB_00401d61:
		  ;
		  func_0x004014c0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     0x40f2e5);
		}
	    }
	  func_0x004014c0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   0x40f2e5);
	  uVar3 = 0x4101af;
	  if (lVar5 == 0x40f2e5)
	    goto LAB_00401d8a;
	}
      func_0x004014c0
	("or available locally via: info \'(coreutils) %s%s\'\n", lVar5,
	 uVar3);
    }
  while (true);
}


#include "readlink.h"

long null_ARRAY_0061548_0_8_;
long null_ARRAY_0061548_8_8_;
long null_ARRAY_0061568_0_8_;
long null_ARRAY_0061568_16_8_;
long null_ARRAY_0061568_24_8_;
long null_ARRAY_0061568_32_8_;
long null_ARRAY_0061568_40_8_;
long null_ARRAY_0061568_48_8_;
long null_ARRAY_0061568_8_8_;
long null_ARRAY_006156c_0_4_;
long null_ARRAY_006156c_16_8_;
long null_ARRAY_006156c_24_4_;
long null_ARRAY_006156c_32_8_;
long null_ARRAY_006156c_40_4_;
long null_ARRAY_006156c_4_4_;
long null_ARRAY_006156c_44_4_;
long null_ARRAY_006156c_48_4_;
long null_ARRAY_006156c_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long DAT_00000008;
long DAT_00000010;
long DAT_00411900;
long DAT_0041198d;
long DAT_004121f0;
long DAT_004121f6;
long DAT_004121f8;
long DAT_004121fc;
long DAT_00412200;
long DAT_00412203;
long DAT_00412205;
long DAT_00412209;
long DAT_0041220d;
long DAT_004127ab;
long DAT_00412c35;
long DAT_00412c46;
long DAT_00412c47;
long DAT_00412d41;
long DAT_00412d47;
long DAT_00412d59;
long DAT_00412d5a;
long DAT_00412d78;
long DAT_00412d7c;
long DAT_00412e06;
long DAT_00615000;
long DAT_00615010;
long DAT_00615020;
long DAT_00615428;
long DAT_00615490;
long DAT_00615494;
long DAT_00615498;
long DAT_0061549c;
long DAT_006154c0;
long DAT_006154d0;
long DAT_006154e0;
long DAT_006154e8;
long DAT_00615500;
long DAT_00615508;
long DAT_00615550;
long DAT_00615551;
long DAT_00615558;
long DAT_00615560;
long DAT_00615568;
long DAT_006156f8;
long DAT_00615700;
long DAT_00615708;
long DAT_00615710;
long DAT_00615718;
long DAT_00615728;
long fde_00413858;
long int7;
long null_ARRAY_00412000;
long null_ARRAY_00412180;
long null_ARRAY_004130a0;
long null_ARRAY_004132e0;
long null_ARRAY_00615480;
long null_ARRAY_00615520;
long null_ARRAY_00615580;
long null_ARRAY_00615680;
long null_ARRAY_006156c0;
long PTR_DAT_00615420;
long PTR_null_ARRAY_00615478;
long register0x00000020;
void
FUN_004021e0 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_0040220d;
  func_0x00401950 (DAT_006154e0, "Try \'%s --help\' for more information.\n",
		   DAT_00615568);
  do
    {
      func_0x00401b00 ((ulong) uParm1);
    LAB_0040220d:
      ;
      func_0x00401770 ("Usage: %s [OPTION]... FILE...\n", DAT_00615568);
      uVar3 = DAT_006154c0;
      func_0x00401960
	("Print value of a symbolic link or canonical file name\n\n",
	 DAT_006154c0);
      func_0x00401960
	("  -f, --canonicalize            canonicalize by following every symlink in\n                                every component of the given name recursively;\n                                all but the last component must exist\n  -e, --canonicalize-existing   canonicalize by following every symlink in\n                                every component of the given name recursively,\n                                all components must exist\n",
	 uVar3);
      func_0x00401960
	("  -m, --canonicalize-missing    canonicalize by following every symlink in\n                                every component of the given name recursively,\n                                without requirements on components existence\n  -n, --no-newline              do not output the trailing delimiter\n  -q, --quiet\n  -s, --silent                  suppress most error messages (on by default)\n  -v, --verbose                 report error messages\n  -z, --zero                    end each output line with NUL, not newline\n",
	 uVar3);
      func_0x00401960 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401960
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_00411900;
      local_80 = "test invocation";
      puVar6 = &DAT_00411900;
      local_78 = 0x41196c;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401a40 ("readlink", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401aa0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004019c0 (lVar2, &DAT_0041198d, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "readlink";
		  goto LAB_004023b9;
		}
	    }
	  func_0x00401770 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "readlink");
	LAB_004023e2:
	  ;
	  pcVar5 = "readlink";
	  uVar3 = 0x411925;
	}
      else
	{
	  func_0x00401770 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x00401aa0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004019c0 (lVar2, &DAT_0041198d, 3);
	      if (iVar1 != 0)
		{
		LAB_004023b9:
		  ;
		  func_0x00401770
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "readlink");
		}
	    }
	  func_0x00401770 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "readlink");
	  uVar3 = 0x412d77;
	  if (pcVar5 == "readlink")
	    goto LAB_004023e2;
	}
      func_0x00401770
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}


#include "factor.h"

long null_ARRAY_0061e42_0_4_;
long null_ARRAY_0061e42_40_8_;
long null_ARRAY_0061e42_4_4_;
long null_ARRAY_0061e42_48_8_;
long null_ARRAY_0061e46_0_8_;
long null_ARRAY_0061e46_8_8_;
long null_ARRAY_0061e51_0_8_;
long null_ARRAY_0061e51_8_8_;
long null_ARRAY_0061e64_0_8_;
long null_ARRAY_0061e64_16_8_;
long null_ARRAY_0061e64_24_8_;
long null_ARRAY_0061e64_32_8_;
long null_ARRAY_0061e64_40_8_;
long null_ARRAY_0061e64_48_8_;
long null_ARRAY_0061e64_8_8_;
long null_ARRAY_0061e68_0_4_;
long null_ARRAY_0061e68_16_8_;
long null_ARRAY_0061e68_24_4_;
long null_ARRAY_0061e68_32_8_;
long null_ARRAY_0061e68_40_4_;
long null_ARRAY_0061e68_4_4_;
long null_ARRAY_0061e68_44_4_;
long null_ARRAY_0061e68_48_4_;
long null_ARRAY_0061e68_8_4_;
long local_2f_1_1_;
long local_5b_1_1_;
long local_9c_4_4_;
long local_a1_0_4_;
long local_a1_0_8_;
long local_a1_1_9_;
long local_a1_4_6_;
long local_a1_8_2_;
long local_a3_4_4_;
long local_a4_0_2_;
long local_a4_0_4_;
long local_a4_4_4_;
long local_a5_0_8_;
long local_a5_1_9_;
long local_a5_4_6_;
long local_a5_8_2_;
long local_a6_0_4_;
long local_c_0_1_;
long DAT_00000008;
long DAT_00000010;
long DAT_00416f23;
long DAT_00416fae;
long DAT_00416fb2;
long DAT_00416fcd;
long DAT_00416ffd;
long DAT_0041a4d0;
long DAT_0041a543;
long DAT_0041a547;
long DAT_0041a54a;
long DAT_0041a54c;
long DAT_0041a550;
long DAT_0041a554;
long DAT_0041ab15;
long DAT_0041af95;
long DAT_0041b0b1;
long DAT_0041b0b2;
long DAT_0041b0d0;
long DAT_0041b0d4;
long DAT_0041b199;
long DAT_0061e000;
long DAT_0061e010;
long DAT_0061e020;
long DAT_0061e400;
long DAT_0061e410;
long DAT_0061e470;
long DAT_0061e474;
long DAT_0061e478;
long DAT_0061e47c;
long DAT_0061e480;
long DAT_0061e488;
long DAT_0061e490;
long DAT_0061e4a0;
long DAT_0061e4a8;
long DAT_0061e4c0;
long DAT_0061e4c8;
long DAT_0061e520;
long DAT_0061e528;
long DAT_0061e530;
long DAT_0061e538;
long DAT_0061e6b8;
long DAT_0061eec8;
long DAT_0061eed0;
long DAT_0061eee0;
long fde_0041bc48;
long int7;
long null_ARRAY_00417340;
long null_ARRAY_00417384;
long null_ARRAY_004173c0;
long null_ARRAY_00417440;
long null_ARRAY_00419e80;
long null_ARRAY_0041a140;
long null_ARRAY_0041a400;
long null_ARRAY_0041b160;
long null_ARRAY_0041b440;
long null_ARRAY_0041b6a0;
long null_ARRAY_0061e420;
long null_ARRAY_0061e460;
long null_ARRAY_0061e4e0;
long null_ARRAY_0061e510;
long null_ARRAY_0061e540;
long null_ARRAY_0061e640;
long null_ARRAY_0061e680;
long null_ARRAY_0061e6c0;
long PTR_DAT_0061e408;
long PTR_null_ARRAY_0061e458;
long register0x00000020;
long stack0x00000008;
void
FUN_00405a30 (uint uParm1)
{
  int iVar1;
  long lVar2;
  undefined8 uVar3;
  undefined **ppuVar4;
  char *pcVar5;
  undefined *puVar6;
  undefined *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;

  if (uParm1 == 0)
    goto LAB_00405a5d;
  func_0x00401870 (DAT_0061e4a0, "Try \'%s --help\' for more information.\n",
		   DAT_0061e538);
  do
    {
      func_0x00401a30 ((ulong) uParm1);
    LAB_00405a5d:
      ;
      func_0x004016b0 ("Usage: %s [NUMBER]...\n  or:  %s OPTION\n",
		       DAT_0061e538, DAT_0061e538);
      uVar3 = DAT_0061e480;
      func_0x00401880
	("Print the prime factors of each specified integer NUMBER.  If none\nare specified on the command line, read them from standard input.\n\n",
	 DAT_0061e480);
      func_0x00401880 ("      --help     display this help and exit\n",
		       uVar3);
      func_0x00401880
	("      --version  output version information and exit\n", uVar3);
      local_88 = &DAT_00416f23;
      local_80 = "test invocation";
      puVar6 = &DAT_00416f23;
      local_78 = 0x416f8d;
      local_70 = "Multi-call invocation";
      local_68 = "sha224sum";
      local_60 = "sha2 utilities";
      local_58 = "sha256sum";
      local_50 = "sha2 utilities";
      local_48 = "sha384sum";
      local_40 = "sha2 utilities";
      local_38 = "sha512sum";
      local_30 = "sha2 utilities";
      local_28 = 0;
      local_20 = 0;
      ppuVar4 = &local_88;
      do
	{
	  iVar1 = func_0x00401980 ("factor", puVar6);
	  if (iVar1 == 0)
	    break;
	  ppuVar4 = ppuVar4 + 2;
	  puVar6 = *ppuVar4;
	}
      while (puVar6 != (undefined *) 0x0);
      pcVar5 = ppuVar4[1];
      if (pcVar5 == (char *) 0x0)
	{
	  func_0x004016b0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004019e0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004018d0 (lVar2, &DAT_00416fae, 3);
	      if (iVar1 != 0)
		{
		  pcVar5 = "factor";
		  goto LAB_00405bf9;
		}
	    }
	  func_0x004016b0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "factor");
	LAB_00405c22:
	  ;
	  pcVar5 = "factor";
	  uVar3 = 0x416f46;
	}
      else
	{
	  func_0x004016b0 ("\n%s online help: <%s>\n", "GNU coreutils",
			   "https://www.gnu.org/software/coreutils/");
	  lVar2 = func_0x004019e0 (5, 0);
	  if (lVar2 != 0)
	    {
	      iVar1 = func_0x004018d0 (lVar2, &DAT_00416fae, 3);
	      if (iVar1 != 0)
		{
		LAB_00405bf9:
		  ;
		  func_0x004016b0
		    ("Report %s translation bugs to <https://translationproject.org/team/>\n",
		     "factor");
		}
	    }
	  func_0x004016b0 ("Full documentation at: <%s%s>\n",
			   "https://www.gnu.org/software/coreutils/",
			   "factor");
	  uVar3 = 0x41b0cf;
	  if (pcVar5 == "factor")
	    goto LAB_00405c22;
	}
      func_0x004016b0
	("or available locally via: info \'(coreutils) %s%s\'\n", pcVar5,
	 uVar3);
    }
  while (true);
}

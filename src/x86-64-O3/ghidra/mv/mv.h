typedef unsigned char   undefined;

typedef unsigned char    byte;
typedef unsigned char    dwfenc;
typedef unsigned int    dword;
typedef unsigned long    qword;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined3;
typedef unsigned int    undefined4;
typedef unsigned long    undefined5;
typedef unsigned long    undefined6;
typedef unsigned long    undefined8;
typedef unsigned short    word;
#define unkbyte9   unsigned long long
#define unkbyte10   unsigned long long
#define unkbyte11   unsigned long long
#define unkbyte12   unsigned long long
#define unkbyte13   unsigned long long
#define unkbyte14   unsigned long long
#define unkbyte15   unsigned long long
#define unkbyte16   unsigned long long

#define unkuint9   unsigned long long
#define unkuint10   unsigned long long
#define unkuint11   unsigned long long
#define unkuint12   unsigned long long
#define unkuint13   unsigned long long
#define unkuint14   unsigned long long
#define unkuint15   unsigned long long
#define unkuint16   unsigned long long

#define unkint9   long long
#define unkint10   long long
#define unkint11   long long
#define unkint12   long long
#define unkint13   long long
#define unkint14   long long
#define unkint15   long long
#define unkint16   long long

#define unkfloat1   float
#define unkfloat2   float
#define unkfloat3   float
#define unkfloat5   double
#define unkfloat6   double
#define unkfloat7   double
#define unkfloat9   long double
#define unkfloat11   long double
#define unkfloat12   long double
#define unkfloat13   long double
#define unkfloat14   long double
#define unkfloat15   long double
#define unkfloat16   long double

#define BADSPACEBASE   void
#define code   void

#define ulong long
#define true 1
#define bool int
#define false 0
#define float10 int
#define ushort int
#define undefined7 long
#define undefined2 long
#define uint5 long
#define uint6 long

// Add definition for uint
typedef unsigned int    uint;

#define sbyte char

extern imperfection_wrapper(void);
typedef struct eh_frame_hdr eh_frame_hdr, *Peh_frame_hdr;

struct eh_frame_hdr {
    byte eh_frame_hdr_version; // Exception Handler Frame Header Version
    dwfenc eh_frame_pointer_encoding; // Exception Handler Frame Pointer Encoding
    dwfenc eh_frame_desc_entry_count_encoding; // Encoding of # of Exception Handler FDEs
    dwfenc eh_frame_table_encoding; // Exception Handler Table Encoding
};

typedef struct fde_table_entry fde_table_entry, *Pfde_table_entry;

struct fde_table_entry {
    dword initial_loc; // Initial Location
    dword data_loc; // Data location
};

typedef enum Elf64_DynTag {
    DT_AUDIT=1879047932,
    DT_AUXILIARY=2147483645,
    DT_BIND_NOW=24,
    DT_CHECKSUM=1879047672,
    DT_CONFIG=1879047930,
    DT_DEBUG=21,
    DT_DEPAUDIT=1879047931,
    DT_ENCODING=32,
    DT_FEATURE_1=1879047676,
    DT_FILTER=2147483647,
    DT_FINI=13,
    DT_FINI_ARRAY=26,
    DT_FINI_ARRAYSZ=28,
    DT_FLAGS=30,
    DT_FLAGS_1=1879048187,
    DT_GNU_CONFLICT=1879047928,
    DT_GNU_CONFLICTSZ=1879047670,
    DT_GNU_HASH=1879047925,
    DT_GNU_LIBLIST=1879047929,
    DT_GNU_LIBLISTSZ=1879047671,
    DT_GNU_PRELINKED=1879047669,
    DT_HASH=4,
    DT_INIT=12,
    DT_INIT_ARRAY=25,
    DT_INIT_ARRAYSZ=27,
    DT_JMPREL=23,
    DT_MOVEENT=1879047674,
    DT_MOVESZ=1879047675,
    DT_MOVETAB=1879047934,
    DT_NEEDED=1,
    DT_NULL=0,
    DT_PLTGOT=3,
    DT_PLTPAD=1879047933,
    DT_PLTPADSZ=1879047673,
    DT_PLTREL=20,
    DT_PLTRELSZ=2,
    DT_POSFLAG_1=1879047677,
    DT_PREINIT_ARRAYSZ=33,
    DT_REL=17,
    DT_RELA=7,
    DT_RELACOUNT=1879048185,
    DT_RELAENT=9,
    DT_RELASZ=8,
    DT_RELCOUNT=1879048186,
    DT_RELENT=19,
    DT_RELSZ=18,
    DT_RPATH=15,
    DT_RUNPATH=29,
    DT_SONAME=14,
    DT_STRSZ=10,
    DT_STRTAB=5,
    DT_SYMBOLIC=16,
    DT_SYMENT=11,
    DT_SYMINENT=1879047679,
    DT_SYMINFO=1879047935,
    DT_SYMINSZ=1879047678,
    DT_SYMTAB=6,
    DT_TEXTREL=22,
    DT_TLSDESC_GOT=1879047927,
    DT_TLSDESC_PLT=1879047926,
    DT_VERDEF=1879048188,
    DT_VERDEFNUM=1879048189,
    DT_VERNEED=1879048190,
    DT_VERNEEDNUM=1879048191,
    DT_VERSYM=1879048176
} Elf64_DynTag;

typedef enum Elf_ProgramHeaderType {
    PT_DYNAMIC=2,
    PT_GNU_EH_FRAME=1685382480,
    PT_GNU_RELRO=1685382482,
    PT_GNU_STACK=1685382481,
    PT_INTERP=3,
    PT_LOAD=1,
    PT_NOTE=4,
    PT_NULL=0,
    PT_PHDR=6,
    PT_SHLIB=5,
    PT_TLS=7
} Elf_ProgramHeaderType;

typedef struct Elf64_Dyn Elf64_Dyn, *PElf64_Dyn;

struct Elf64_Dyn {
    enum Elf64_DynTag d_tag;
    qword d_val;
};

typedef struct Elf64_Shdr Elf64_Shdr, *PElf64_Shdr;

typedef enum Elf_SectionHeaderType {
    SHT_CHECKSUM=1879048184,
    SHT_DYNAMIC=6,
    SHT_DYNSYM=11,
    SHT_FINI_ARRAY=15,
    SHT_GNU_ATTRIBUTES=1879048181,
    SHT_GNU_HASH=1879048182,
    SHT_GNU_LIBLIST=1879048183,
    SHT_GNU_verdef=1879048189,
    SHT_GNU_verneed=1879048190,
    SHT_GNU_versym=1879048191,
    SHT_GROUP=17,
    SHT_HASH=5,
    SHT_INIT_ARRAY=14,
    SHT_NOBITS=8,
    SHT_NOTE=7,
    SHT_NULL=0,
    SHT_PREINIT_ARRAY=16,
    SHT_PROGBITS=1,
    SHT_REL=9,
    SHT_RELA=4,
    SHT_SHLIB=10,
    SHT_STRTAB=3,
    SHT_SUNW_COMDAT=1879048187,
    SHT_SUNW_move=1879048186,
    SHT_SUNW_syminfo=1879048188,
    SHT_SYMTAB=2,
    SHT_SYMTAB_SHNDX=18
} Elf_SectionHeaderType;

struct Elf64_Shdr {
    dword sh_name;
    enum Elf_SectionHeaderType sh_type;
    qword sh_flags;
    qword sh_addr;
    qword sh_offset;
    qword sh_size;
    dword sh_link;
    dword sh_info;
    qword sh_addralign;
    qword sh_entsize;
};

typedef struct Elf64_Phdr Elf64_Phdr, *PElf64_Phdr;

struct Elf64_Phdr {
    enum Elf_ProgramHeaderType p_type;
    dword p_flags;
    qword p_offset;
    qword p_vaddr;
    qword p_paddr;
    qword p_filesz;
    qword p_memsz;
    qword p_align;
};

typedef struct Elf64_Rela Elf64_Rela, *PElf64_Rela;

struct Elf64_Rela {
    qword r_offset; // location to apply the relocation action
    qword r_info; // the symbol table index and the type of relocation
    qword r_addend; // a constant addend used to compute the relocatable field value
};

typedef struct Elf64_Sym Elf64_Sym, *PElf64_Sym;

struct Elf64_Sym {
    dword st_name;
    byte st_info;
    byte st_other;
    word st_shndx;
    qword st_value;
    qword st_size;
};

typedef struct Elf64_Ehdr Elf64_Ehdr, *PElf64_Ehdr;

struct Elf64_Ehdr {
    byte e_ident_magic_num;
    char e_ident_magic_str[3];
    byte e_ident_class;
    byte e_ident_data;
    byte e_ident_version;
    byte e_ident_pad[9];
    word e_type;
    word e_machine;
    dword e_version;
    qword e_entry;
    qword e_phoff;
    qword e_shoff;
    dword e_flags;
    word e_ehsize;
    word e_phentsize;
    word e_phnum;
    word e_shentsize;
    word e_shnum;
    word e_shstrndx;
};




undefined8 _DT_INIT(void);
void FUN_00402510(void);
void thunk_FUN_0062a070(void);
void thunk_FUN_0062a1b8(void);
void FUN_00402d50(void);
void thunk_FUN_0062a408(void);
ulong FUN_00402d70(void);
ulong FUN_00402da0(uint uParm1,undefined8 *puParm2);
void entry(void);
void FUN_00403380(undefined8 *puParm1);
void FUN_004033b0(void);
void FUN_00403430(void);
void FUN_004034b0(void);
ulong FUN_004034f0(long lParm1,undefined8 uParm2,undefined8 uParm3,long lParm4);
void FUN_004037b0(uint uParm1);
undefined8 FUN_00403a00(long lParm1,long lParm2,char *pcParm3,char cParm4);
undefined8 FUN_00403bd0(long *plParm1,char *pcParm2);
void FUN_00404a70(undefined8 uParm1,undefined8 uParm2,long lParm3);
void FUN_00404ae0(long lParm1,undefined8 uParm2,uint *puParm3);
undefined8 FUN_00404be0(ulong uParm1,undefined8 uParm2,char cParm3,long lParm4);
undefined8 FUN_00404cc0(uint param_1,uint param_2,long param_3,ulong param_4,ulong param_5,byte param_6,undefined8 param_7,undefined8 param_8,ulong param_9,long *param_10,byte *param_11);
ulong FUN_00405110(long lParm1,undefined8 uParm2,uint uParm3,long lParm4,char cParm5,long lParm6);
ulong FUN_00405310(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,long lParm5);
undefined8 FUN_00405430(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,long lParm4);
void FUN_004054c0(long lParm1);
void FUN_004054f0(long lParm1);
ulong FUN_00405520(long lParm1);
ulong FUN_00405550(void);
ulong FUN_00405590(undefined8 *param_1,undefined8 *param_2,byte param_3,long **param_4,undefined8 *param_5,long **param_6,uint param_7,byte *param_8,byte *param_9,undefined *param_10);
void FUN_004096c0(undefined8 uParm1,undefined8 uParm2,ulong uParm3,uint *puParm4,undefined8 uParm5,undefined8 uParm6);
ulong FUN_00409830(ulong *puParm1,ulong uParm2);
ulong FUN_00409840(long *plParm1,long *plParm2);
void FUN_00409860(long lParm1);
void FUN_00409880(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_004098d0(undefined8 uParm1,undefined8 uParm2);
undefined8 FUN_00409910(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_00409990(void);
void FUN_004099d0(undefined4 uParm1,undefined4 *puParm2);
ulong FUN_00409a00(uint *puParm1);
void FUN_00409cd0(undefined8 uParm1,uint *puParm2);
void FUN_00409cf0(undefined8 uParm1,undefined8 *puParm2);
ulong FUN_00409d10(undefined4 uParm1,undefined8 uParm2,ulong uParm3,long lParm4,undefined4 uParm5,byte bParm6);
ulong FUN_00409e80(undefined8 uParm1,ulong uParm2,long lParm3,byte bParm4);
ulong FUN_00409fd0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
ulong FUN_0040a050(undefined8 uParm1);
long FUN_0040a090(undefined8 uParm1,ulong uParm2);
void FUN_0040a1b0(long lParm1,long lParm2);
void FUN_0040a2a0(char *pcParm1);
long FUN_0040a300(long lParm1,int iParm2,char cParm3);
void FUN_0040a830(undefined8 uParm1,undefined8 uParm2);
ulong FUN_0040a840(undefined8 uParm1,char *pcParm2);
ulong FUN_0040a8d0(ulong uParm1,ulong uParm2,ulong uParm3);
char * FUN_0040a950(char *pcParm1,uint uParm2);
void FUN_0040af70(undefined8 uParm1);
void FUN_0040af80(void);
void FUN_0040b040(void);
long FUN_0040b0d0(void);
void FUN_0040b180(void);
long FUN_0040b1a0(char *pcParm1);
char * thunk_FUN_0040b29c(char *pcParm1);
char * FUN_0040b29c(char *pcParm1);
void FUN_0040b2e0(long lParm1);
undefined FUN_0040b310(char *pcParm1);;
void FUN_0040b350(void);
void FUN_0040b360(undefined8 param_1,byte param_2,ulong param_3);
void FUN_0040b3b0(long lParm1,undefined8 uParm2,undefined8 *puParm3);
ulong FUN_0040b440(long lParm1,undefined8 uParm2,undefined8 *puParm3);
char * FUN_0040b480(long lParm1);
void FUN_0040b530(ulong uParm1,undefined *puParm2);
void FUN_0040b700(void);
long FUN_0040b720(long lParm1,char *pcParm2,undefined8 *puParm3);
long FUN_0040b820(uint uParm1,long lParm2,long lParm3);
ulong FUN_0040b8a0(ulong uParm1,ulong uParm2);
undefined FUN_0040b8b0(long lParm1,long lParm2);;
undefined8 FUN_0040b8c0(long *plParm1,long **pplParm2,char cParm3);
long FUN_0040ba30(long *plParm1,long lParm2,long **pplParm3,char cParm4);
long FUN_0040bb50(long *plParm1,long lParm2);
long * FUN_0040bbc0(ulong uParm1,undefined1 *puParm2,code *pcParm3,code *pcParm4,long lParm5);
void FUN_0040beb0(long **pplParm1);
ulong FUN_0040bf90(long *plParm1,ulong uParm2);
long FUN_0040c200(long *plParm1,long lParm2);
long FUN_0040c4e0(long *plParm1,long lParm2);
ulong FUN_0040c7d0(undefined8 *puParm1,ulong uParm2);
undefined8 FUN_0040c800(undefined8 *puParm1,undefined8 *puParm2);
ulong FUN_0040c830(undefined8 *puParm1,undefined8 *puParm2);
void FUN_0040c870(undefined8 *puParm1);
void FUN_0040c890(long lParm1);
undefined8 FUN_0040c930(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,uint uParm4,uint uParm5);
void FUN_0040c970(undefined8 uParm1,uint uParm2,undefined4 uParm3);
undefined * FUN_0040c9b0(char *pcParm1,int iParm2);
void FUN_0040ca80(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,ulong param_6,long param_7,char *param_8,char *param_9);
undefined1 * FUN_0040d910(uint uParm1,uint uParm2,undefined8 uParm3);
undefined1 * FUN_0040db10(uint uParm1,undefined8 uParm2);
undefined1 * FUN_0040dcf0(undefined8 uParm1);
undefined1 * FUN_0040ded0(uint uParm1,uint uParm2,undefined8 uParm3);
undefined1 * FUN_0040e120(uint uParm1,undefined8 uParm2);
undefined1 * FUN_0040e2c0(undefined8 uParm1);
ulong FUN_0040e440(uint uParm1,long lParm2,uint uParm3,long lParm4,uint uParm5);
undefined8 * FUN_0040e810(undefined8 *puParm1);
long FUN_0040e860(uint uParm1,undefined8 uParm2,ulong uParm3);
ulong FUN_0040e8d0(undefined8 uParm1,undefined8 uParm2);
void FUN_0040ea00(undefined8 *puParm1,undefined8 *puParm2);
long FUN_0040ea10(long lParm1,uint uParm2);
undefined8 FUN_0040ed40(undefined8 uParm1,ulong uParm2);
ulong FUN_0040edb0(long lParm1,int iParm2,undefined8 uParm3,code *pcParm4,ulong uParm5);
ulong FUN_0040ef30(uint uParm1);
ulong FUN_0040ef70(ulong *puParm1,ulong uParm2);
ulong FUN_0040ef80(ulong *puParm1,ulong *puParm2);
ulong FUN_0040ef90(undefined8 uParm1,undefined8 *puParm2,long lParm3,ulong uParm4);
ulong FUN_0040fa50(ulong uParm1,long lParm2,undefined8 *puParm3);
ulong FUN_0040ff80(long lParm1,undefined8 *puParm2);
ulong FUN_00410320(long lParm1,undefined8 *puParm2);
long FUN_004109e0(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,undefined8 uParm6);
long FUN_00410c40(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5);
long FUN_00410e90(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,uint *puParm5);
long FUN_004113b0(undefined8 param_1,long param_2,undefined8 param_3,undefined8 param_4,long param_5,long param_6,long param_7,long param_8,long param_9,long param_10,long param_11,long param_12,long param_13,long param_14);
ulong FUN_00411860(void);
void FUN_004118a0(long lParm1);
long FUN_004118c0(long lParm1,long lParm2);
void FUN_00411900(undefined8 uParm1);
void FUN_00411940(void);
void FUN_00411970(undefined8 uParm1,uint uParm2);
long FUN_004119c0(void);
ulong FUN_004119f0(void);
undefined8 FUN_00411a30(ulong uParm1,ulong uParm2);
void FUN_00411a80(undefined8 uParm1);
ulong FUN_00411ac0(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,uint param_1,ulong param_2,undefined8 param_3,undefined8 param_4,undefined8 param_5,undefined8 param_6);
ulong FUN_00411c10(ulong uParm1,long lParm2);
ulong FUN_00411c90(ulong param_1,undefined8 param_2,ulong param_3);
undefined8 FUN_00411dd0(ulong uParm1);
ulong FUN_00411e40(long lParm1);
undefined8 FUN_00411ed0(void);
void FUN_00411ee0(void);
undefined8 FUN_00411ef0(ulong uParm1,long lParm2,long lParm3,ulong uParm4);
ulong FUN_00411fb0(ulong *puParm1,ulong *puParm2);
ulong FUN_00411fd0(long lParm1,ulong uParm2);
ulong FUN_00411fe0(ulong *puParm1,ulong uParm2);
ulong FUN_00411ff0(ulong *puParm1,ulong *puParm2);
ulong FUN_00412000(long *plParm1,long *plParm2);
ulong FUN_00412030(long lParm1,long lParm2,char cParm3);
undefined8 FUN_004121a0(long lParm1);
ulong FUN_00412300(long lParm1);
ulong FUN_004123a0(long lParm1,long lParm2,ulong uParm3,long lParm4);
long FUN_00412610(long *plParm1,int iParm2);
long * FUN_004134a0(long *plParm1,uint uParm2,long lParm3);
undefined8 FUN_00413bc0(long *plParm1);
undefined8 * FUN_00413d70(long *plParm1);
undefined8 FUN_00414870(undefined8 uParm1,long lParm2,uint uParm3);
long FUN_004148a0(long lParm1,ulong uParm2);
void FUN_00414d20(long lParm1,int *piParm2);
ulong FUN_00415000(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,int param_6,int *param_7,int param_8,undefined8 param_9);
ulong FUN_00415760(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,ulong param_6,int param_7);
void FUN_00415e60(void);
ulong FUN_00415e80(long lParm1,uint uParm2,uint uParm3);
ulong FUN_00415f70(ulong uParm1,char *pcParm2,uint uParm3,long lParm4,uint uParm5);
undefined8 FUN_00416200(long lParm1,long lParm2);
void FUN_00416280(void);
ulong FUN_004162a0(uint *puParm1,byte *pbParm2,long lParm3);
ulong FUN_00416310(long lParm1,ulong uParm2);
undefined8 FUN_00416400(long lParm1,ulong uParm2);
undefined8 FUN_00416460(long lParm1,uint uParm2,long lParm3);
ulong FUN_00416510(uint param_1,undefined8 param_2,uint param_3,ulong param_4);
undefined8 FUN_00416630(long lParm1,undefined8 uParm2,undefined8 uParm3);
void FUN_004166a0(long lParm1,long lParm2);
ulong FUN_004166e0(long lParm1,long lParm2);
void FUN_00416b30(void);
ulong FUN_00416b40(long lParm1);
ulong FUN_00416be0(long lParm1,long lParm2);
undefined8 FUN_00416c30(undefined8 uParm1,ulong uParm2,long lParm3);
undefined8 FUN_00416ca0(long lParm1);
undefined8 FUN_00416d70(uint uParm1,long lParm2,ulong uParm3);
ulong FUN_00416e70(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3);
undefined8 FUN_00416f40(undefined8 uParm1,undefined8 uParm2,undefined4 uParm3,undefined4 *puParm4);
ulong FUN_00416f70(uint *puParm1,undefined8 uParm2,uint uParm3);
void FUN_00416fb0(undefined8 uParm1,undefined8 uParm2);
void FUN_00416fd0(void);
void FUN_00416fe0(long *plParm1,long lParm2,long lParm3);
long FUN_004170b0(undefined8 uParm1,undefined8 uParm2,long *plParm3,long lParm4,long lParm5,code *pcParm6);
undefined *FUN_004172a0(uint uParm1,undefined8 uParm2,undefined *puParm3,ulong uParm4,undefined1 *puParm5,code *pcParm6);
ulong FUN_00417460(ulong uParm1,char cParm2);
ulong FUN_004174c0(undefined8 uParm1);
void FUN_00417530(long lParm1);
undefined8 FUN_00417540(long *plParm1,long *plParm2);
undefined8 FUN_004175d0(void);
void FUN_004175e0(undefined8 *puParm1);
ulong FUN_00417620(ulong uParm1);
ulong FUN_004176c0(char *pcParm1,ulong uParm2);
void FUN_004176f0(undefined4 *puParm1,undefined4 uParm2);
ulong FUN_00417720(long lParm1);
ulong FUN_00417730(long lParm1,uint uParm2);
ulong FUN_00417770(long lParm1);
char * FUN_004177b0(void);
void FUN_00417b00(void);
long * FUN_00417b50(void);
ulong FUN_00417b90(undefined8 *puParm1,ulong uParm2);
ulong FUN_00417d40(undefined8 *puParm1);
void FUN_00417d80(long lParm1);
long * FUN_00417dd0(ulong uParm1,ulong uParm2);
void FUN_00418060(long *plParm1,ulong uParm2,ulong uParm3);
undefined8 FUN_004181d0(long *plParm1);
void FUN_00418220(long *plParm1,long *plParm2);
void FUN_00418490(ulong *puParm1);
void FUN_004186d0(undefined8 uParm1);
undefined8 FUN_004186e0(long lParm1,uint uParm2,uint uParm3);
void FUN_00418800(undefined8 uParm1,undefined8 uParm2);
void FUN_00418820(undefined8 uParm1);
void FUN_004188b0(void);
undefined8 FUN_00418970(uint uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4);
undefined4 * FUN_004189e0(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5);
ulong FUN_00418c40(void);
uint * FUN_00418eb0(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5);
char * FUN_00419460(int iParm1,ulong uParm2,undefined4 *puParm3,long lParm4,uint uParm5);
void FUN_0041a1b0(uint param_1);
ulong FUN_0041a390(void);
void FUN_0041a5b0(undefined8 uParm1,uint uParm2);
undefined8 *FUN_0041a720(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4);
long FUN_004210f0(undefined8 uParm1,undefined8 uParm2);
double FUN_00421190(int *piParm1);
void FUN_004211d0(uint *param_1);
undefined8 FUN_00421250(uint *puParm1,ulong *puParm2);
undefined8 FUN_00421470(byte *pbParm1,ulong *puParm2,byte **ppbParm3);
ulong FUN_00422120(void);
ulong FUN_00422150(void);
void FUN_00422190(void);
undefined8 _DT_FINI(void);
undefined FUN_0062a000();
undefined FUN_0062a008();
undefined FUN_0062a010();
undefined FUN_0062a018();
undefined FUN_0062a020();
undefined FUN_0062a028();
undefined FUN_0062a030();
undefined FUN_0062a038();
undefined FUN_0062a040();
undefined FUN_0062a048();
undefined FUN_0062a050();
undefined FUN_0062a058();
undefined FUN_0062a060();
undefined FUN_0062a068();
undefined FUN_0062a070();
undefined FUN_0062a078();
undefined FUN_0062a080();
undefined FUN_0062a088();
undefined FUN_0062a090();
undefined FUN_0062a098();
undefined FUN_0062a0a0();
undefined FUN_0062a0a8();
undefined FUN_0062a0b0();
undefined FUN_0062a0b8();
undefined FUN_0062a0c0();
undefined FUN_0062a0c8();
undefined FUN_0062a0d0();
undefined FUN_0062a0d8();
undefined FUN_0062a0e0();
undefined FUN_0062a0e8();
undefined FUN_0062a0f0();
undefined FUN_0062a0f8();
undefined FUN_0062a100();
undefined FUN_0062a108();
undefined FUN_0062a110();
undefined FUN_0062a118();
undefined FUN_0062a120();
undefined FUN_0062a128();
undefined FUN_0062a130();
undefined FUN_0062a138();
undefined FUN_0062a140();
undefined FUN_0062a148();
undefined FUN_0062a150();
undefined FUN_0062a158();
undefined FUN_0062a160();
undefined FUN_0062a168();
undefined FUN_0062a170();
undefined FUN_0062a178();
undefined FUN_0062a180();
undefined FUN_0062a188();
undefined FUN_0062a190();
undefined FUN_0062a198();
undefined FUN_0062a1a0();
undefined FUN_0062a1a8();
undefined FUN_0062a1b0();
undefined FUN_0062a1b8();
undefined FUN_0062a1c0();
undefined FUN_0062a1c8();
undefined FUN_0062a1d0();
undefined FUN_0062a1d8();
undefined FUN_0062a1e0();
undefined FUN_0062a1e8();
undefined FUN_0062a1f0();
undefined FUN_0062a1f8();
undefined FUN_0062a200();
undefined FUN_0062a208();
undefined FUN_0062a210();
undefined FUN_0062a218();
undefined FUN_0062a220();
undefined FUN_0062a228();
undefined FUN_0062a230();
undefined FUN_0062a238();
undefined FUN_0062a240();
undefined FUN_0062a248();
undefined FUN_0062a250();
undefined FUN_0062a258();
undefined FUN_0062a260();
undefined FUN_0062a268();
undefined FUN_0062a270();
undefined FUN_0062a278();
undefined FUN_0062a280();
undefined FUN_0062a288();
undefined FUN_0062a290();
undefined FUN_0062a298();
undefined FUN_0062a2a0();
undefined FUN_0062a2a8();
undefined FUN_0062a2b0();
undefined FUN_0062a2b8();
undefined FUN_0062a2c0();
undefined FUN_0062a2c8();
undefined FUN_0062a2d0();
undefined FUN_0062a2d8();
undefined FUN_0062a2e0();
undefined FUN_0062a2e8();
undefined FUN_0062a2f0();
undefined FUN_0062a2f8();
undefined FUN_0062a300();
undefined FUN_0062a308();
undefined FUN_0062a310();
undefined FUN_0062a318();
undefined FUN_0062a320();
undefined FUN_0062a328();
undefined FUN_0062a330();
undefined FUN_0062a338();
undefined FUN_0062a340();
undefined FUN_0062a348();
undefined FUN_0062a350();
undefined FUN_0062a358();
undefined FUN_0062a360();
undefined FUN_0062a368();
undefined FUN_0062a370();
undefined FUN_0062a378();
undefined FUN_0062a380();
undefined FUN_0062a388();
undefined FUN_0062a390();
undefined FUN_0062a398();
undefined FUN_0062a3a0();
undefined FUN_0062a3a8();
undefined FUN_0062a3b0();
undefined FUN_0062a3b8();
undefined FUN_0062a3c0();
undefined FUN_0062a3c8();
undefined FUN_0062a3d0();
undefined FUN_0062a3d8();
undefined FUN_0062a3e0();
undefined FUN_0062a3e8();
undefined FUN_0062a3f0();
undefined FUN_0062a3f8();
undefined FUN_0062a400();
undefined FUN_0062a408();

